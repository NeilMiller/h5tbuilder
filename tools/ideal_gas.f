program ideal_gas
  
  integer, parameter :: imax = 256
  integer, parameter :: jmax = 256
  double precision :: minlogRho, maxlogRho, minlogT, maxlogT, &
       dlogRho, dlogT, Rho, T, logRho, logT, lnRho, lnT, &
       f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho2dt, d3f_drhodt2, d4f_drho2dt2, nden, N_A, mu, alpha, k, h, m
  integer :: i,j, fid
  character (len=256) :: filename
  
  minlogRho = -2.
  maxlogRho = 2.
  minlogT = 3.
  maxlogT = 6.

  dlogRho = (maxlogRho - minlogRho) / (imax - 1)
  dlogT = (maxlogT - minlogT) / (jmax - 1)

  h = 6.6e-27 
  k = 1.38e-16 !! erg K^-1
  mu = 18. !! Water
  N_A = 6.e23 !! Number of particles per mole
  m = 18. / N_A !! g / particle
  alpha = (h / (2 * 3.14 * m * k)**0.5)**3.

  write(*,*) "mu = ", mu
  write(*,*) "m = ", m

  Rho = 1.
  T = 300.
  write(*,*) h / (2. * 3.14 * m * k * T)**0.5
  
  fid = 20
  filename = "idealwater.data"
  open(fid, file=filename)
  


  
  do i=1,imax
     do j=1,jmax
        logRho = minlogRho + (i-1) * dlogRho
        logT = minlogT + (j-1) * dlogT
        
        Rho = 10.**logRho
        T = 10.**logT
        lnRho = log(Rho)
        lnT = log(T)
        
        !! Get the equation of state here

        f_val = -(N_A / mu) * k * T * ( - log(rho) - log(N_A / mu) - log(alpha) + 1.5 * log(T) + 1.)
        df_drho =  (N_A / mu) * k * T / rho
        df_dt = -(N_A / mu) * k * ( - log(rho) - log(N_A / mu) - log(alpha) + 1.5 * log(T) + 2.5)
        d2f_drho2 = -(N_A / mu) * k * T / rho**2
        d2f_dt2 =  -(N_A / mu) * 1.5 * k  / T
        d2f_drhodt =  (N_A / mu) * k / rho
        d3f_drho2dt = -(N_A / mu) * k / rho**2
        d3f_drhodt2 = 0.
        d4f_drho2dt2 = 0.
        write(fid, *) lnRho, lnT, f_val, df_drho, df_dt, &
             d2f_drho2, d2f_dt2, d2f_drhodt, &
             d3f_drho2dt, d3f_drhodt2, d4f_drho2dt2
     enddo
  enddo

  close(fid)

end program ideal_gas
