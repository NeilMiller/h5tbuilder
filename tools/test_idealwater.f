program test
  use ideal_water
  
  type (idealparams), pointer :: tbl, tblcpy
  integer :: eos_ID, ierr
  double precision :: Rho, T
  double precision, pointer, dimension(:) :: Pvect, Evect, Svect
  
  
  allocate(tbl)
  allocate(Pvect(6), Evect(6), Svect(6))
  call set_handle(tbl, eos_ID, ierr)
  write(*,*) "eos_ID: ", eos_ID
  write(*,*) "ierr : ", ierr
  
  call get_tbl(eos_ID, tblcpy, ierr)
  
  Rho = 0.1
  T = 1000.
  call get_val(tblcpy, Rho, T, Pvect, Evect, Svect, ierr)
  write(*,*) "Rho = ", Rho
  write(*,*) "T = ", T
  write(*,*) "P = ", Pvect(i_val)
  write(*,*) "E = ", Evect(i_val)
  write(*,*) "S = ", Svect(i_val)
  
  call free_handle(eos_ID, ierr)
  
  
  deallocate(Pvect)
  deallocate(Evect)
  deallocate(Svect)
  deallocate(tbl)
  
end program test
