program simple
  
  !  PARTS:
  !  1. Compare the results of the h5table over a variety of points vs the actual results of the ANEOS code
  use aneos_access
  use h5table
  use table_2D_mod !! Luke's code

  implicit none  

  logical, parameter :: verbose = .false.
  integer, parameter :: imax=1000
  integer, parameter :: jmax=1000
  
  integer :: i, j
  real(8) :: logtmin, logtmax, logrhomin, logrhomax, &
       logtstep, logrhostep, logPmin, logPmax, logPstep
  real(8), pointer, dimension(:) :: logrhovect
  real(8) :: rhovect(imax), lnrhovect(imax)
  real(8) :: logPvect(imax), Pvect(imax), lnPvect(imax)
  real(8) :: logtvect(jmax), tvect(jmax), lntvect(jmax)
  real(8), pointer, dimension(:,:) :: logPmat, logEmat,logSmat, &
       logPdiff,logEdiff,logSdiff, &
       h5logP, h5logE, h5logS, &
       free_energy, alpha, &
       logRhomat, logRhodiff, h5logRho
  real(8), pointer, dimension(:,:,:) :: lnPvec_mat, lnEvec_mat, lnSvec_mat, &
       Pvec_mat, Evec_mat, Svec_mat

  type (h5tbl), pointer :: mytbl
  character (len=256) :: h5filename, h5name
  real(8), pointer, dimension(:) :: h5Pvect, h5Evect, h5Svect, &
       h5lnPvect, h5lnEvect, h5lnSvect
  integer :: h5Xsz, h5Ysz, indXLow, indXHi, indYLow, indYHi
  integer :: ierr, ios
  
  real(8) :: Fvect(9), Pan, Ean, San, Rho, T, P, logRho, logEan, logSan
  real(8) :: minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta
  real(8) :: minlogPAN, maxlogPAN

  !! Variables for Luke's Table2D
  type (table_2D), pointer :: lrtbl
  integer :: nx(2), nderiv(2)
  real(8) :: x0(2), dx(2)
  logical :: log_spc(2)
  real(8), pointer, dimension(:,:,:,:) :: tab
  
  character (len=256) :: filename
  logical, parameter :: do_testII = .true.
  logical, parameter :: do_testIII = .false.
  logical, parameter :: compare_aneos = .true.
  
  !! Part I.
  !!  Initiate the h5 table and the ANEOS table
  
  h5filename = "data/waterfe.bin"
  h5name = "water"
  ierr = 0
  h5Xsz = 81
  h5Ysz = 31

  write(*,*) "Please check that this is the right filename: ", h5filename
  
  allocate(mytbl)
  allocate(logrhovect(imax))
  allocate(logPmat(imax,jmax), logEmat(imax,jmax), logSmat(imax,jmax), logRhomat(imax,jmax))
  allocate(logPdiff(imax,jmax), logEdiff(imax,jmax), logSdiff(imax,jmax), logRhodiff(imax,jmax))
  allocate(h5logP(imax,jmax), h5logE(imax,jmax), h5logS(imax,jmax), h5logRho(imax,jmax))
  allocate(free_energy(imax,jmax), alpha(imax,jmax))

  allocate(h5Pvect(num_derivs), h5Evect(num_derivs), h5Svect(num_derivs))
  allocate(h5lnPvect(num_derivs), h5lnEvect(num_derivs), h5lnSvect(num_derivs))

  allocate(lnPvec_mat(imax,jmax,num_derivs), &
       lnEvec_mat(imax,jmax,num_derivs), &
       lnSvec_mat(imax,jmax,num_derivs))
  allocate(Pvec_mat(imax,jmax,num_derivs), &
       Evec_mat(imax,jmax,num_derivs), &
       Svec_mat(imax,jmax,num_derivs))
  if(verbose) then
     write(*,*) "Setting up h5 table from file: ", h5filename
  endif
  call setup_h5tbl(mytbl, h5Xsz, h5Ysz, h5filename, h5name, ierr)
  if(verbose) then
     if(ierr .eq. 0) then
        write(*,*) "h5 table setup SUCCESS"
     else
        write(*,*) "h5 table setup FAIL"
     end if
  endif
!  minlogRhoAlpha = 2.
!  minlogRhoBeta = -10.
  minlogRhoAlpha = 0.  !! no cut on the left side
  minlogRhoBeta = -10.

!  maxlogRhoAlpha = 8.
!  maxlogRhoBeta = -26.

!! Standard water cut
  maxlogRhoAlpha = 7.5
  maxlogRhoBeta = -24.25

!  maxlogRhoAlpha = 0.
!  maxlogRhoBeta = 10.
  call slice_h5table(mytbl, minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta)

  !! h5 Table initiated!!
  
  !! Initiate the aneos Water table
  if(compare_aneos) then
     call ANEOSWater_Init
  endif

  !! Part II.
  !!  Sample the equation of state over a density - temperature grid

  indXLow = 2
  indXHi = h5Xsz-1
  indYLow = 2
  indYHi = h5Ysz-1
  logtmin = mytbl%lnT_v(indYLow) / log(1d1)
  logtmax = mytbl%lnT_v(indYHi) / log(1d1)
  logrhomin =  mytbl%lnRho_v(indXLow) / log(1d1)
  logrhomax = mytbl%lnRho_v(indXHi) / log(1d1)
  
  logrhostep = (logrhomax - logrhomin) / (imax - 1)
  logtstep = (logtmax - logtmin) / (jmax - 1)
  

  !! Rho is in g/cc
  do i=1,imax
     logrhovect(i) = logrhomin + logrhostep * (i-1)
     rhovect(i) = 10d0**logrhovect(i)
     lnrhovect(i) = log(rhovect(i))
  enddo

  !! T is in K
  do j=1,jmax
     logtvect(j) = logtmin + logtstep * (j-1)
     tvect(j) = 10**logtvect(j)
     lntvect(j) = log(tvect(j))
  enddo
  
  minlogPAN = 100d0
  maxlogPAN = -100d0
  
  if(do_testII) then

  do i=1,imax
     do j=1,jmax
        Rho = rhovect(i)
        T = tvect(j)
        
!        write(*,*) "(Rho,T)", Rho, T
        if(compare_aneos) then
           call ANEOS_get_PES(Rho, T, Pan, Ean, San)  !! This is more pure
           call ANEOS_get_free(Rho,T, Fvect)          !! This result involves numerical derivatives
        endif


        call h5_get_val(mytbl, Rho, T, h5Pvect, h5Evect, h5Svect, ierr)
        !! convert this into lnderivs

        if(ierr .eq. 0) then           
!           write(*,*) " H5 output (ierr=0) (P,E,S) = ", h5Pvect(i_val), h5Evect(i_val), h5Svect(i_val)
           call convDerivslnDerivs(Rho, T, h5Pvect, h5Evect, h5Svect, h5lnPvect, h5lnEvect, h5lnSvect)
           
        else
           h5Pvect(:) = 1d-100
           h5Evect(:) = 1d-100
           h5Svect(:) = 1d-100
           
           h5lnPvect(:) = 0d0
           h5lnEvect(:) = 0d0
           h5lnSvect(:) = 0d0
!           write(*,*) "Calling aneos: ", Rho, T
!           write(*,*) " ANEOS output (P,E,S)          = ", Pan, Ean, San           
!           write(*,*) "h5 ierr = ", ierr
        endif

        Pvec_mat(i,j,1:num_derivs) = h5Pvect(1:num_derivs)
        Evec_mat(i,j,1:num_derivs) = h5Evect(1:num_derivs)
        Svec_mat(i,j,1:num_derivs) = h5Svect(1:num_derivs)
        
        lnPvec_mat(i,j,1:num_derivs) = h5lnPvect(1:num_derivs)
        lnEvec_mat(i,j,1:num_derivs) = h5lnEvect(1:num_derivs)
        lnSvec_mat(i,j,1:num_derivs) = h5lnSvect(1:num_derivs)
        
        h5logP(i,j) = log10(h5Pvect(i_val))
        h5logE(i,j) = log10(h5Evect(i_val))
        h5logS(i,j) = log10(h5Svect(i_val))
        
        
        if(isnan(h5Pvect(i_val))) then
           if (verbose) write(*,*) "h5 P NAN"
        else if(h5Pvect(i_val) .le. 0d0) then
           if (verbose) write(*,*) "h5 P NEGATIVE"
           h5logP(i,j) = -100.
        endif
        
        if(isnan(h5Evect(i_val))) then
           if (verbose) write(*,*) "h5 E NAN"
        else if(h5Evect(i_val) .le. 0d0) then
           if (verbose) write(*,*) "h5 E NEGATIVE"
           h5logE(i,j) = -100.
        endif
        
        if(isnan(h5Svect(i_val))) then
           if (verbose) write(*,*) "h5 S NAN"
        else if(h5Svect(i_val) .le. 0d0) then
           if (verbose) write(*,*) "h5 S NEGATIVE : Rho = ", Rho, "; T = ", T
           h5logS(i,j) = -100
        endif
        
!        write(*,*) log10(h5Pvect(i_val)), log10(h5Evect(i_val)), log10(h5Svect(i_val))
        if(compare_aneos) then
           logPmat(i,j) = log10(Pan)
           logEmat(i,j) = log10(Ean)
           if(San .le. 0) then 
              San = 1d-100
           endif
           logSmat(i,j) = log10(San)
           
!        write(*,*) h5logP(i,j), h5logE(i,j), h5logS(i,j)
        
           alpha(i,j) = - (1. / Rho**2) * h5Pvect(i_dt) / h5Svect(i_drho) - 1.
        
           logPdiff(i,j) = logPmat(i,j) - h5logP(i,j)
           logEdiff(i,j) = logEmat(i,j) - h5logE(i,j)
           logSdiff(i,j) = logSmat(i,j) - h5logS(i,j)
        
           if(log10(Pan) .lt. minlogPAN) then
              minlogPAN = log10(Pan)
           endif
           
           if(log10(Pan) .gt. maxlogPAN) then
              maxlogPAN = log10(Pan)
           endif
        endif
     enddo
  enddo

  if(compare_aneos) then
     if(verbose) write(*,*) "logP range: ", minlogPAN, maxlogPAN
  endif

  !! Write out the differences to a file
  
  filename = "h5tdata/h5t_logRhovect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logRhovect
  close(unit=19)
  
  filename = "h5tdata/h5t_logTvect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logtvect
  close(unit=19)
  
  if(compare_aneos) then
     filename = "h5tdata/h5t_logPdiff.data"
     call write_matrix(logPdiff, filename)
     
     filename = "h5tdata/h5t_logEdiff.data"
     call write_matrix(logEdiff, filename)
     
     filename = "h5tdata/h5t_logSdiff.data"
     call write_matrix(logSdiff, filename)
     
     filename = "h5tdata/an_logP.data"
     call write_matrix(logPmat, filename)
  
     filename = "h5tdata/an_logE.data"
     call write_matrix(logEmat, filename)
     
     filename = "h5tdata/an_logS.data"
     call write_matrix(logSmat, filename)
  endif

  filename = "h5tdata/h5t_logP.data"
  call write_matrix(h5logP, filename)
  
  filename = "h5tdata/h5t_logE.data"
  call write_matrix(h5logE, filename)
  
  filename = "h5tdata/h5t_logS.data"
  call write_matrix(h5logS, filename)
  
  if(compare_aneos) then
     filename = "h5tdata/h5t_alpha.data"
     call write_matrix(alpha, filename)
  endif

  !!  Here are the derivatives in lnderivs Space
  filename = "h5tdata/lnderivs/h5t_logRhovect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logRhovect
  close(unit=19)
  
  filename = "h5tdata/lnderivs/h5t_logTvect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logtvect
  close(unit=19)
  
  filename = "h5tdata/lnderivs/lnP.data"
  call write_matrix(lnPvec_mat(1:imax,1:jmax,i_val), filename)

  filename = "h5tdata/lnderivs/dlnPdlnRho.data"
  call write_matrix(lnPvec_mat(1:imax,1:jmax,i_dlnRho), filename)

  filename = "h5tdata/lnderivs/dlnPdlnT.data"
  call write_matrix(lnPvec_mat(1:imax,1:jmax,i_dlnT), filename)

  filename = "h5tdata/lnderivs/d2lnPdlnRho2.data"
  call write_matrix(lnPvec_mat(1:imax,1:jmax,i_dlnRho2), filename)
  
  filename = "h5tdata/lnderivs/d2lnPdlnT2.data"
  call write_matrix(lnPvec_mat(1:imax,1:jmax,i_dlnT2), filename)
  
  filename = "h5tdata/lnderivs/d2lnPdlnRhodlnT.data"
  call write_matrix(lnPvec_mat(1:imax,1:jmax,i_dlnRhodlnT), filename)


  filename = "h5tdata/lnderivs/lnE.data"
  call write_matrix(lnEvec_mat(1:imax,1:jmax,i_val), filename)

  filename = "h5tdata/lnderivs/dlnEdlnRho.data"
  call write_matrix(lnEvec_mat(1:imax,1:jmax,i_dlnRho), filename)

  filename = "h5tdata/lnderivs/dlnEdlnT.data"
  call write_matrix(lnEvec_mat(1:imax,1:jmax,i_dlnT), filename)

  filename = "h5tdata/lnderivs/d2lnEdlnRho2.data"
  call write_matrix(lnEvec_mat(1:imax,1:jmax,i_dlnRho2), filename)
  
  filename = "h5tdata/lnderivs/d2lnEdlnT2.data"
  call write_matrix(lnEvec_mat(1:imax,1:jmax,i_dlnT2), filename)
  
  filename = "h5tdata/lnderivs/d2lnEdlnRhodlnT.data"
  call write_matrix(lnEvec_mat(1:imax,1:jmax,i_dlnRhodlnT), filename)



  filename = "h5tdata/lnderivs/lnS.data"
  call write_matrix(lnSvec_mat(1:imax,1:jmax,i_val), filename)

  filename = "h5tdata/lnderivs/dlnSdlnRho.data"
  call write_matrix(lnSvec_mat(1:imax,1:jmax,i_dlnRho), filename)

  filename = "h5tdata/lnderivs/dlnSdlnT.data"
  call write_matrix(lnSvec_mat(1:imax,1:jmax,i_dlnT), filename)

  filename = "h5tdata/lnderivs/d2lnSdlnRho2.data"
  call write_matrix(lnSvec_mat(1:imax,1:jmax,i_dlnRho2), filename)
  
  filename = "h5tdata/lnderivs/d2lnSdlnT2.data"
  call write_matrix(lnSvec_mat(1:imax,1:jmax,i_dlnT2), filename)
  
  filename = "h5tdata/lnderivs/d2lnSdlnRhodlnT.data"
  call write_matrix(lnSvec_mat(1:imax,1:jmax,i_dlnRhodlnT), filename)

  !! linear derivatives
 
  !!  Here are the derivatives in derivs Space
  filename = "h5tdata/derivs/h5t_logRhovect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logRhovect
  close(unit=19)
  
  filename = "h5tdata/derivs/h5t_logTvect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logtvect
  close(unit=19)
  
  filename = "h5tdata/derivs/P.data"
  call write_matrix(Pvec_mat(1:imax,1:jmax,i_val), filename)

  filename = "h5tdata/derivs/dPdRho.data"
  call write_matrix(Pvec_mat(1:imax,1:jmax,i_dRho), filename)

  filename = "h5tdata/derivs/dPdT.data"
  call write_matrix(Pvec_mat(1:imax,1:jmax,i_dT), filename)

  filename = "h5tdata/derivs/d2PdRho2.data"
  call write_matrix(Pvec_mat(1:imax,1:jmax,i_dRho2), filename)
  
  filename = "h5tdata/derivs/d2PdT2.data"
  call write_matrix(Pvec_mat(1:imax,1:jmax,i_dT2), filename)
  
  filename = "h5tdata/derivs/d2PdRhodT.data"
  call write_matrix(Pvec_mat(1:imax,1:jmax,i_dRhodT), filename)


  filename = "h5tdata/derivs/E.data"
  call write_matrix(Evec_mat(1:imax,1:jmax,i_val), filename)

  filename = "h5tdata/derivs/dEdRho.data"
  call write_matrix(Evec_mat(1:imax,1:jmax,i_dRho), filename)

  filename = "h5tdata/derivs/dEdT.data"
  call write_matrix(Evec_mat(1:imax,1:jmax,i_dT), filename)

  filename = "h5tdata/derivs/d2EdRho2.data"
  call write_matrix(Evec_mat(1:imax,1:jmax,i_dRho2), filename)
  
  filename = "h5tdata/derivs/d2EdT2.data"
  call write_matrix(Evec_mat(1:imax,1:jmax,i_dT2), filename)
  
  filename = "h5tdata/derivs/d2EdRhodT.data"
  call write_matrix(Evec_mat(1:imax,1:jmax,i_dRhodT), filename)



  filename = "h5tdata/derivs/S.data"
  call write_matrix(Svec_mat(1:imax,1:jmax,i_val), filename)

  filename = "h5tdata/derivs/dSdRho.data"
  call write_matrix(Svec_mat(1:imax,1:jmax,i_dRho), filename)

  filename = "h5tdata/derivs/dSdT.data"
  call write_matrix(Svec_mat(1:imax,1:jmax,i_dT), filename)

  filename = "h5tdata/derivs/d2SdRho2.data"
  call write_matrix(Svec_mat(1:imax,1:jmax,i_dRho2), filename)
  
  filename = "h5tdata/derivs/d2SdT2.data"
  call write_matrix(Svec_mat(1:imax,1:jmax,i_dT2), filename)
  
  filename = "h5tdata/derivs/d2SdRhodT.data"
  call write_matrix(Svec_mat(1:imax,1:jmax,i_dRhodT), filename)

  endif

  if(do_testIII) then
  
  !! Part III.  
  !!   Sample P - T grid
  
!  logPmin = 11.
!  logPmax = 15.
  logPmin = 4.
  logPmax = 18.
  logPstep = (logPmax - logPmin) / (imax - 1.)
  
  do i=1,imax
     logPvect(i) = logPmin + (i-1) * logPstep
     Pvect(i) = 10d0**logPvect(i)
     lnPvect(i) = log(Pvect(i))
  enddo

  h5logRho(:,:) = 0d0
  h5logS(:,:) = 0d0
  h5logE(:,:) = 0d0

  do j=1,jmax

     do i=1,imax
        !! Recall that we already have looked up the ANEOS value {P,E,S}(Rho)|T
        P = Pvect(i)
        T = Tvect(j)
        write(*,*) i, j, P, T

        call h5_get_Rho(mytbl, P, T, Rho, h5Pvect, h5Evect, h5Svect, ierr)
        if(ierr .eq. 0) then
           
        else
           Rho = 1d-100
           h5Pvect(:) = 1d-100
           h5Evect(:) = 1d-100
           h5Svect(:) = 1d-100
        endif
        
        h5logRho(i,j) = log10(Rho)

        if(h5Evect(i_val) .le. 0.d0) then
           h5logE(i,j) = - 100
           write(*,*) "E negative at i,j, (P,Rho,T)", i, j, P, Rho, T
        else
           h5logE(i,j) = log10(h5Evect(i_val))           
        endif 

        if(h5Svect(i_val) .le. 0d0) then
           h5logE(i,j) = -100
           write(*,*) "S negative at i,j, (P,Rho,T)", i, j, P, Rho, T
        else
           h5logS(i,j) = log10(h5Svect(i_val))        
        endif
        
        
!        call linterpolate(logPmat(:,j),logrhovect,imax,logPvect(i),logRho,ierr)
!        if(ierr .ne. 0) then
!           write(*,*) "Failed to find density for interpolation"
!        else
!           call linterpolate(logrhovect,logEmat(:,j),imax,logRho,logEan,ierr)
!           call linterpolate(logrhovect,logSmat(:,j),imax,logRho,logSan,ierr)
!        endif
        
!        logRhomat(i,j) = logRho
!        logEmat(i,j) = logEan
!        logSmat(i,j) = logSan
        
!        logRhodiff(i,j) = logRhomat(i,j) - h5logRho(i,j)
!        logEmat(i,j) = logEmat(i,j) - h5logE(i,j)
!        logSmat(i,j) = logSmat(i,j) - h5logS(i,j)
        
     enddo
     
  enddo


  filename = "h5tdata/h5tPT_logPvect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logPvect
  close(unit=19)
  
  filename = "h5tdata/h5tPT_logTvect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logtvect
  close(unit=19)
  
!  filename = "h5tdata/h5tPT_logRhodiff.data"
!  call write_matrix(logRhodiff, filename)
  
!  filename = "h5tdata/h5tPT_logEdiff.data"
!  call write_matrix(logEdiff, filename)
  
!  filename = "h5tdata/h5tPT_logSdiff.data"
!  call write_matrix(logSdiff, filename)
  
!  filename = "h5tdata/anPT_logRho.data"
!  call write_matrix(logRhomat, filename)
  
!  filename = "h5tdata/anPT_logE.data"
!  call write_matrix(logEmat, filename)
  
!  filename = "h5tdata/anPT_logS.data"
!  call write_matrix(logSmat, filename)
  
  filename = "h5tdata/h5tPT_logRho.data"
  call write_matrix(h5logRho, filename)
  
  filename = "h5tdata/h5tPT_logE.data"
  call write_matrix(h5logE, filename)
  
  filename = "h5tdata/h5tPT_logS.data"
  call write_matrix(h5logS, filename)
  
  endif

  !! Part IV. 
  !!  Deallocate and clean up memory
  !! Clean up the h5 table

  call free_h5tbl(mytbl)
  if(compare_aneos) then
     call ANEOS_Shutdown
  endif
  deallocate(mytbl)
  deallocate(logrhovect)
  deallocate(h5Pvect)
  deallocate(h5Evect)
  deallocate(h5Svect)
  deallocate(h5lnPvect)
  deallocate(h5lnEvect)
  deallocate(h5lnSvect)
  deallocate(logPmat)
  deallocate(logRhomat)
  deallocate(logSmat)
  deallocate(logEmat)
  deallocate(logPdiff)
  deallocate(logRhodiff)
  deallocate(logEdiff)
  deallocate(logSdiff)
  deallocate(h5logP)
  deallocate(h5logRho)
  deallocate(h5logE)
  deallocate(h5logS)
  deallocate(free_energy)
  deallocate(alpha)
  
  deallocate(lnPvec_mat, lnEvec_mat, lnSvec_mat)
  deallocate(Pvec_mat, Evec_mat, Svec_mat)

contains
  subroutine donothing(temp)
    real(8), intent(inout) :: temp
  end subroutine donothing
  
  
  subroutine write_matrix(matrix, filename)
    real(8), intent(in) :: matrix(imax,jmax)
    character (len=256) :: filename
    
    open(unit=19,file=trim(filename),status='replace', &
         iostat=ios,action='write',form='formatted')       
    write(19,'(e20.6)') matrix
    close(unit=19)
  end subroutine write_matrix

  !! lin-terpolate - get it?
  subroutine linterpolate(Xvect, Yvect, N, Xval, Yval, ierr)
    real(8), pointer, dimension(:), intent(in) :: Xvect, Yvect
    integer, intent(in) :: N
    real(8), intent(in) :: Xval
    real(8), intent(out) :: Yval
    integer, intent(out) :: ierr

    integer :: ind1, ind2
    real(8) :: w1, w2

    ierr = 0

    call binary_search(Xvect, Xval, N, ind1, ind2, ierr)

    write(*,*) "linterpolate"
    write(*,*) "Xvect[ind1], Xvect[ind2], Xval, ind1, ind2, ierr"
    write(*,*) Xvect(ind1), Xvect(ind2), Xval, ind1, ind2, ierr
    

    if(ierr .ne. 0) then
       return
    endif

    w1 = (Xvect(ind2) - Xval) / (Xvect(ind2) - Xvect(ind1))
    w2 = 1.d0 - w1

    Yval = w1 * Yvect(ind1) + w2 * Yvect(ind2)

    
  end subroutine linterpolate

  subroutine conv_vector(Fvect, lnFvect)
    use mixeos_def
    real(8), intent(in) :: Fvect(6)
    real(8), intent(out) :: lnFvect(6)
    
    lnFvect(i_val) = log(Fvect(i_val))
    lnFvect(i_dX) = (Rho / Fvect(i_val)) * Fvect(i_dX)
    lnFvect(i_dY) = (T / Fvect(i_val)) * Fvect(i_dY)
    lnFvect(i_dX2) = lnFvect(i_dX) - lnFvect(i_dX)**2 + (Rho**2 / Fvect(i_val)) * Fvect(i_dX2)
    lnFvect(i_dY2) = lnFvect(i_dY) - lnFvect(i_dY)**2 + (T**2 / Fvect(i_val)) * Fvect(i_dY2)
    lnFvect(i_dXdY) = - lnFvect(i_dX) * lnFvect(i_dY) + (Rho * T / Fvect(i_val)) * Fvect(i_dXdY)
  end subroutine conv_vector
  

  subroutine convDerivslnDerivs(Rho, T, Pvect, Evect, Svect, lnPvect, lnEvect, lnSvect)
    use mixeos_def

    real(8), intent(in) :: Rho, T
    real(8), intent(in) :: Pvect(6), Evect(6), Svect(6)
    real(8), intent(out) :: lnPvect(6), lnEvect(6), lnSvect(6)

    call conv_vector(Pvect, lnPvect)
    call conv_vector(Evect, lnEvect)
    call conv_vector(Svect, lnSvect)
    
  end subroutine convDerivslnDerivs


  !! Vector must be sorted
  subroutine binary_search(Xvect, Xval, N, ind1, ind2, ierr)
    real(8), pointer, dimension(:), intent(in) :: Xvect
    real(8), intent(in) :: Xval
    integer, intent(in) :: N
    integer, intent(out) :: ind1, ind2
    integer, intent(out) :: ierr
    real(8) :: dx1, dx2, dx3
    integer :: ind3
    
    ind1 = 1
    ind2 = N
    dx1 = Xvect(ind1) - Xval
    dx2 = Xvect(ind2) - Xval
    if(dx1 * dx2 .gt. 0) then
       ierr = -1
       return
    endif

    do while ((ind2 - ind1) .gt. 1)
       ind3 = (ind1 + ind2) / 2
       dx3 = Xvect(ind3) - Xval
       
       if(dx1 * dx3 .le. 0) then
          ind2 = ind3
          dx2 = dx3
       else
          ind1 = ind3
          dx1 = dx3
       endif
    end do
  end subroutine binary_search
end program simple
   
