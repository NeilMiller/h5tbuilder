!! Compare Luke's interpolation routines with the h5 tabular interpolation routines.  
!! Do this for an extremely simple case such as a polynomial equation of state.
!! Then do it for the ideal gas law and ANEOS (with numerical derivatives)

program test
  use h5table       !! My code
  use h5table_params
  use table_2D_mod  !! Luke's code
  use mixeos_def

  implicit none

  integer, parameter :: Xsize = 21
  integer, parameter :: Ysize = 21
  integer :: i,j
  real(8) :: coeff01, coeff02
  real(8) :: Rho, T
  
  !! Variables to initiate h5table
  type (h5tbl), pointer :: mytbl
  real(8), pointer, dimension(:) :: RhoVect, TVect
  real(8), pointer, dimension(:,:,:) :: Fij
  real(8), pointer, dimension(:,:) :: matrix
  character (len=256) :: name
  integer :: ierr

  !! Variables to initialte luke's table2D

  type (table_2D), pointer :: lrtbl
  integer :: nx(2), nderiv(2)
  real(8) :: x0(2), dx(2)
  logical :: log_spc(2)
  real(8), pointer, dimension(:,:,:,:) :: tab

  !! Variables for test matrix
  
  integer, parameter :: test_Xsize = 101
  integer, parameter :: test_Ysize = 101
  real(8), pointer, dimension(:) :: test_RhoVect, test_TVect, &
       test_logRhoVect, test_logTVect
  real(8), pointer, dimension(:,:,:) :: Fij_luke, Fij_orig, Fij_h5 
  real(8) :: test_minlogRho, test_maxlogRho, test_dlogRho, &
             test_minlogT, test_maxlogT, test_dlogT
  real(8) :: X_test(2)
  integer :: X_der(2)
  real(8), pointer, dimension(:) :: Fvect
  character (len=256) :: filename
  integer :: DM1, DM2(2)
  real(8) :: Fij_luke_val, Fij_luke_dT


  logical :: write_data = .true.
  logical :: test_single = .false.
  logical :: test_multi = .true.

  name = "test"
  
  nx(1) = Xsize
  nx(2) = Ysize
  nderiv(1) = 2
  nderiv(2) = 2
  x0(:) = 0.1
  dx(:) = 0.1
  log_spc(:) = .true.

  allocate(mytbl)
  allocate(lrtbl)
  allocate(RhoVect(Xsize), Tvect(Ysize))
  allocate(Fij(9,Xsize,Ysize))
  allocate(tab(Xsize,Ysize,3,3))
  allocate(Fvect(num_derivs3))

  
  do i=1,Xsize
     RhoVect(i) = x0(1) * 1d1**((i-1)*dx(1))
  enddo

  do j=1,Ysize
     TVect(j) = x0(2) * 1d1**((j-1)*dx(2))
  enddo
  
  !! Initialize table identically for Luke's code as for my code

  coeff01 = 1d1
  coeff02 = 1d1
  do i=1,Xsize
     do j=1,Ysize
        Fij(:,i,j) = 0d0
        Fij(TBL_VAL,i,j) = coeff01 * TVect(j) + coeff02 * TVect(j)**2d0
        Fij(TBL_DY,i,j) = coeff01 + 2d0 * coeff02 * TVect(j)
        Fij(TBL_DY2,i,j) = 2d0 * coeff02

        tab(i,j,:,:) = 0d0
        tab(i,j,1,1) = coeff01 * TVect(j) + coeff02 * TVect(j)**2d0
        tab(i,j,1,2) = coeff01 + 2d0 * coeff02 * TVect(j)
        tab(i,j,1,3) = 2d0 * coeff02 
     enddo
  enddo
  
  
  call lrtbl%init(nx, nderiv, x0, dx, log_spc=log_spc, tab=tab)
  call setup_h5tbl_mem(mytbl, Xsize, Ysize, RhoVect, TVect, Fij, name, ierr)

  !! In this test, I will fix Rho, T and have both routines be very verbose
  if(test_single) then
     Rho = 1.321
     T = 1.2131

     call h5_get_free(mytbl, Rho, T, Fvect, ierr)
     write(*,*) "Fvect(i_dT) = ", Fvect(i_dT)

     X_test(1) = Rho
     X_test(2) = T
     X_der(1) = 0
     X_der(2) = 0
     Fij_luke_val = lrtbl%interp(X_test, X_der)
     X_der(1) = 0
     X_der(2) = 1
     Fij_luke_dT = lrtbl%interp(X_test, X_der)
     write(*,*) "Fij_like_dT = ", Fij_luke_dT
  endif
  
  if(test_multi) then

  !! Now test these tables 
  test_minlogRho = -0.5d0
  test_maxlogRho = 0.5d0
  test_dlogRho = (test_maxlogRho - test_minlogRho) / (test_Xsize - 1)

  test_minlogT = -0.5d0
  test_maxlogT = 0.5d0
  test_dlogT = (test_maxlogT - test_minlogT) / (test_Ysize - 1)

  allocate(test_RhoVect(test_Xsize), test_logRhoVect(test_Xsize))
  allocate(test_TVect(test_Ysize), test_logTVect(test_Ysize))
  
  allocate(Fij_luke(num_derivs3,test_Xsize,test_Ysize))
  allocate(Fij_orig(num_derivs3,test_Xsize,test_Ysize))
  allocate(Fij_h5(num_derivs3,test_Xsize,test_Ysize))


  do i=1,test_Xsize
     test_RhoVect(i) = 1d1**(test_minlogRho + test_dlogRho*(i-1))
     test_logRhoVect(i) = log10(test_RhoVect(i))
  enddo

  do j=1,test_Ysize
     test_TVect(j) = 1d1**(test_minlogT + test_dlogT*(j-1))
     test_logTVect(j) = log10(test_TVect(j))
  enddo

  do i=1,test_Xsize
     do j=1,test_Ysize

        
        Rho = test_RhoVect(i)
        T = test_TVect(j)
        X_test(1) = Rho
        X_test(2) = T
        write(*,*) i, j, Rho, T

        !! Call original interpolation
        Fij_orig(:,i,j) = 0d0
        Fij_orig(i_val,i,j) = coeff01 * test_TVect(j) + coeff02 * test_TVect(j)**2d0
        Fij_orig(i_dT,i,j) = coeff01 + 2d0 * coeff02 * test_TVect(j)
        Fij_orig(i_dT2,i,j) = 2d0 * coeff02
        
        
        !! Call luke interpolation
        Fij_luke(:,i,j) = 0d0
        X_der(1) = 0
        X_der(2) = 0
        Fij_luke(i_val,i,j) = lrtbl%interp(X_test, X_der)
        X_der(1) = 0
        X_der(2) = 1
        Fij_luke(i_dT,i,j) = lrtbl%interp(X_test, X_der)
        write(*,*) "Fij_luke(i_dT,i,j) - Fij_orig = ", &
             Fij_luke(i_dT,i,j) - Fij_orig(i_dT,i,j)

        !! Call h5 interpolation
        call h5_get_free(mytbl, Rho, T, Fvect, ierr)
        Fij_h5(:,i,j) = Fvect
        write(*,*) "Fij_h5(i_dT,i,j) - Fij_dT = ", &
             Fij_h5(i_dT,i,j) - Fij_orig(i_dT,i,j)
     enddo
  enddo

  if(write_data) then
     filename = "test_luke.data/logRhoVect.data"
     call write_vect(test_logRhoVect,filename)
     
     filename = "test_luke.data/logTVect.data"
     call write_vect(test_logTVect,filename)
     
     allocate(matrix(test_Xsize, test_Ysize))

     !! Write out to data file
     filename = "test_luke.data/val_LO.data"
     matrix = Fij_luke(i_val,:,:) - Fij_orig(i_val,:,:)
     call write_matrix(matrix,filename)
     
     filename = "test_luke.data/val_HO.data"
     matrix = Fij_h5(i_val,:,:) - Fij_orig(i_val,:,:)
     call write_matrix(matrix,filename)
     
     filename = "test_luke.data/dfdt_LO.data"
     matrix = Fij_luke(i_dT,:,:) - Fij_orig(i_dT,:,:)
     call write_matrix(matrix,filename)
     
     filename = "test_luke.data/dfdt_HO.data"
     matrix = Fij_h5(i_dT,:,:) - Fij_orig(i_dT,:,:)
     call write_matrix(matrix,filename)
     
     deallocate(matrix)
  endif

  deallocate(test_RhoVect, test_TVect)
  deallocate(test_logRhoVect, test_logTVect)
  deallocate(Fij_luke, Fij_orig, Fij_h5)
 
  endif

  

  deallocate(mytbl)
  deallocate(lrtbl)
  deallocate(RhoVect, TVect)
  deallocate(Fij)
  deallocate(tab)
  deallocate(Fvect)

  
contains
  subroutine write_vect(vector, filename)
    real(8), intent(in), pointer, dimension(:) :: vector
    character (len=256) :: filename
    integer :: ios
    
    open(unit=19,file=trim(filename), status='replace', &
         iostat=ios, action='write', form='formatted')
    write(19,'(e20.6)') vector
    close(unit=19)
  end subroutine write_vect

  subroutine write_matrix(matrix, filename)
    real(8), intent(in), pointer, dimension(:,:) :: matrix
    character (len=256) :: filename
    integer :: ios
    
    open(unit=19,file=trim(filename),status='replace', &
         iostat=ios,action='write',form='formatted')       
    write(19,'(e20.6)') matrix
    close(unit=19)
  end subroutine write_matrix

end program test

