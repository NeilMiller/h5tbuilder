program simple
  use aneos_access
  implicit none
  
  integer :: i, j, ios
  integer :: imax, jmax
  double precision :: logRhoMin, logRhoMax, dlogRho, logRho
  double precision :: logTMin, logTMax, dlogT, logT
  double precision :: Rho, T, Fvect(num_free_derivs)

  double precision, pointer, dimension(:) :: logRhoVec, logTVec  
  double precision, pointer, dimension(:,:,:) :: Fvec_mat

  character (len=256) :: filename

  imax = 1024
  jmax = 1024

  allocate(logRhoVec(imax), logTVec(jmax))
  allocate(Fvec_mat(imax, jmax, num_free_derivs))

  logRhoMin = -2.
  logRhoMax = 2.
  logTMin = 3.
  logTMax = 5.
  
  dlogRho = (logRhoMax - logRhoMin) / (imax - 1)
  dlogT = (logTMax - logTMin) / (jmax - 1)

  
  do i=1, imax
     logRhoVec(i) = logRhoMin + (i-1) * dlogRho
  enddo
  do j=1,jmax
     logTVec(j) = logTMin + (j-1) * dlogT
  enddo

  
  call ANEOSWater_Init


  do i=1, imax
     do j=1, jmax
        logRho = logRhoVec(i)
        Rho = 1d1**logRhoVec(i)
        
        logT = logTVec(j)
        T = 1d1**logTVec(j)

        call ANEOS_get_free(Rho, T, Fvect)

        Fvec_mat(i,j,1:num_free_derivs) = Fvect
     enddo
  enddo

  call ANEOS_Shutdown

  
  filename = "h5tdata/subgrid/logRho.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logRhoVec
  close(unit=19)

  filename = "h5tdata/subgrid/logT.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') logTVec
  close(unit=19)

  filename = "h5tdata/subgrid/fr.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_val), filename)
  
  filename = "h5tdata/subgrid/dfdrho.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dRho), filename)
  
  filename = "h5tdata/subgrid/dfdt.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dT), filename)

  filename = "h5tdata/subgrid/d2fdrho2.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dRho2), filename)
  
  filename = "h5tdata/subgrid/d2fdt2.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dT2), filename)
  
  filename = "h5tdata/subgrid/d2fdrhodt.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dRhodT), filename)

  filename = "h5tdata/subgrid/d3fdrho2dt.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dRho2dT), filename)
  
  filename = "h5tdata/subgrid/d3fdrhodt2.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dRhodT2), filename)

  filename = "h5tdata/subgrid/d4fdrho2dt2.data"
  call write_matrix(Fvec_mat(1:imax,1:jmax, a_dRho2dT2), filename)
  
  deallocate(logRhoVec)
  deallocate(logTVec)
  deallocate(Fvec_mat)

  
contains
  
  subroutine write_matrix(matrix, filename)
    double precision, intent(in) :: matrix(imax,jmax)
    character (len=256) :: filename
    
    open(unit=19,file=trim(filename),status='replace', &
         iostat=ios,action='write',form='formatted')       
    write(19,'(e20.6)') matrix
    close(unit=19)
  end subroutine write_matrix


end program simple

