program simple
  
  !  PARTS:
  !  1. Compare the results of the h5table over a variety of points vs the actual results of the ANEOS code
  use aneos_access
  use h5table

  implicit none  

  
  type (h5tbl), pointer :: mytbl
  character (len=256) :: h5filename, h5name
  double precision, pointer, dimension(:) :: h5Pvect, h5Evect, h5Svect
  integer :: h5Xsz, h5Ysz
  integer :: ierr, ios
  
  double precision :: Fvect(9), Pan, Ean, San, Rho, T, P, logRho, logEan, logSan
  double precision :: minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta
  
  double precision :: minlogPAN, maxlogPAN

  character (len=256) :: filename
  integer, parameter :: VERBOSE = 0
  character (len=256) :: filename_in, filename_out

  

  !! Part I.
  !!  Initiate the h5 table and the ANEOS table
  
  h5filename = "data/waterfe.bin"
  h5name = "water"
  ierr = 0
  h5Xsz = 2048
  h5Ysz = 2048
  
  allocate(mytbl)
  allocate(h5Pvect(6), h5Evect(6), h5Svect(6))

  write(*,*) "Setting up h5 table from file: ", h5filename
  call setup_h5tbl(mytbl, h5Xsz, h5Ysz, h5filename, h5name, ierr)
  if(ierr .eq. 0) then
     write(*,*) "h5 table setup SUCCESS"
  else
     write(*,*) "h5 table setup FAIL"
  end if

!  minlogRhoAlpha = 2.
!  minlogRhoBeta = -10.

  minlogRhoAlpha = 0. ! no cut
  minlogRhoBeta = -10.

  maxlogRhoAlpha = 8.
  maxlogRhoBeta = -26.
  
  call slice_h5table(mytbl, minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta)

  !! h5 Table initiated!!
  
  !! Initiate the aneos Water table
  call ANEOSWater_Init


  !! Part II.
  !!  Sample the equation of state over a density - temperature grid

  Rho = 1.2
  T = 1d5
        
  call ANEOS_get_PES(Rho, T, Pan, Ean, San)  !! This is more pure
  call ANEOS_get_free(Rho,T, Fvect)          !! This result involves numerical derivatives
  
  call h5_get_val(mytbl, Rho, T, h5Pvect, h5Evect, h5Svect, ierr)
  if(ierr .eq. 0) then           
!           write(*,*) " H5 output (ierr=0) (P,E,S) = ", h5Pvect(i_val), h5Evect(i_val), h5Svect(i_val)
  else
     h5Pvect(:) = 1d-100
     h5Evect(:) = 1d-100
     h5Svect(:) = 1d-100
  endif
        
  if(isnan(h5Pvect(i_val))) then
     write(*,*) "h5 P NAN"
  else if(h5Pvect(i_val) .le. 0d0) then
     write(*,*) "h5 P NEGATIVE"
  endif
  
  if(isnan(h5Evect(i_val))) then
     write(*,*) "h5 E NAN"
  else if(h5Evect(i_val) .le. 0d0) then
     write(*,*) "h5 E NEGATIVE"
  endif
  
  if(isnan(h5Svect(i_val))) then
     write(*,*) "h5 S NAN"
  else if(h5Svect(i_val) .le. 0d0) then
     write(*,*) "h5 S NEGATIVE : Rho = ", Rho, "; T = ", T
  endif
        

  write(*,*) "Rho: ", Rho
  write(*,*) "T: ", T
  write(*,*) "ANEOS result: ", Pan, Ean, San
  write(*,*) "h5 result: ", h5Pvect(i_val), h5Evect(i_val), h5Svect(i_val)
  
  P = h5Pvect(i_val)
  call h5_get_Rho(mytbl, P, T, Rho, h5Pvect, h5Evect, h5Svect, ierr)
  write(*,*) "P, T", P, T
  write(*,*) "h5vect: ", h5Pvect(i_val), h5Evect(i_val), h5Svect(i_val)

  filename_in = "planet_data/log_lowM_highE.dat"
  filename_out = "planet_data/RhoTset01.dat"
  call parse_file(filename_in, filename_out)
  filename_in = "planet_data/log_lowM_midE.dat"
  filename_out = "planet_data/RhoTset02.dat"
  call parse_file(filename_in, filename_out)
  filename_in = "planet_data/log_lowM_lowE.dat"
  filename_out = "planet_data/RhoTset03.dat"
  call parse_file(filename_in, filename_out)
        !! Part III, (P, T) lookup
  filename_in = "planet_data/logPT_hd80606.dat"
  filename_out = "planet_data/RhoTset04.dat"
  call parse_file(filename_in, filename_out)
  
  
!        P = Pvect(i)
!        T = Tvect(j)

!        call h5_get_Rho(mytbl, P, T, Rho, h5Pvect, h5Evect, h5Svect, ierr)
!        if(ierr .eq. 0) then
           
!        else
!           Rho = 1d-100
!           h5Pvect(:) = 1d-100
!           h5Evect(:) = 1d-100
!           h5Svect(:) = 1d-100
!        endif
        
        
!        call linterpolate(logPmat(:,j),logrhovect,imax,logPvect(i),logRho,ierr)
!        if(ierr .ne. 0) then
!           write(*,*) "Failed to find density for interpolation"
!        else
!           call linterpolate(logrhovect,logEmat(:,j),imax,logRho,logEan,ierr)
!           call linterpolate(logrhovect,logSmat(:,j),imax,logRho,logSan,ierr)
!        endif
        
        

  !! Part IV. 
  !!  Deallocate and clean up memory
  !! Clean up the h5 table

  call free_h5tbl(mytbl)
  call ANEOS_Shutdown
  deallocate(mytbl)
  deallocate(h5Pvect)
  deallocate(h5Evect)
  deallocate(h5Svect)
  
contains

  !! lin-terpolate - get it?
  subroutine linterpolate(Xvect, Yvect, N, Xval, Yval, ierr)
    double precision, pointer, dimension(:), intent(in) :: Xvect, Yvect
    integer, intent(in) :: N
    double precision, intent(in) :: Xval
    double precision, intent(out) :: Yval
    integer, intent(out) :: ierr

    integer :: ind1, ind2
    double precision :: w1, w2

    ierr = 0

    call binary_search(Xvect, Xval, N, ind1, ind2, ierr)

    write(*,*) "linterpolate"
    write(*,*) "Xvect[ind1], Xvect[ind2], Xval, ind1, ind2, ierr"
    write(*,*) Xvect(ind1), Xvect(ind2), Xval, ind1, ind2, ierr
    

    if(ierr .ne. 0) then
       return
    endif

    w1 = (Xvect(ind2) - Xval) / (Xvect(ind2) - Xvect(ind1))
    w2 = 1.d0 - w1

    Yval = w1 * Yvect(ind1) + w2 * Yvect(ind2)

    
  end subroutine linterpolate

  !! Vector must be sorted
  subroutine binary_search(Xvect, Xval, N, ind1, ind2, ierr)
    double precision, pointer, dimension(:), intent(in) :: Xvect
    double precision, intent(in) :: Xval
    integer, intent(in) :: N
    integer, intent(out) :: ind1, ind2
    integer, intent(out) :: ierr
    double precision :: dx1, dx2, dx3
    integer :: ind3
    
    ind1 = 1
    ind2 = N
    dx1 = Xvect(ind1) - Xval
    dx2 = Xvect(ind2) - Xval
    if(dx1 * dx2 .gt. 0) then
       ierr = -1
       return
    endif

    do while ((ind2 - ind1) .gt. 1)
       ind3 = (ind1 + ind2) / 2
       dx3 = Xvect(ind3) - Xval
       
       if(dx1 * dx3 .le. 0) then
          ind2 = ind3
          dx2 = dx3
       else
          ind1 = ind3
          dx1 = dx3
       endif
    end do
  end subroutine binary_search


  !! The input file is in the format
  !! P, T
  !! Use the h5_get_Rho to get (Rho, T)
  !! output to filename_out : log10Rho, log10T

  subroutine parse_file(filename_in, filename_out)
    character (len=256) :: filename_in, filename_out
    integer :: fid_in, fid_out, io
    double precision :: P=1d20, Td5, log10P, log10T

    fid_in =  19
    fid_out = 20

    open(unit=fid_in,file=trim(filename_in),action='read',status="old")
    open(unit=fid_out,file=trim(filename_out), action='write',status="replace")! form='unformatted')
    
    P = 1d20

    do while(P .gt. 1d6) 
       ierr = 0

       write(*,*) "Read line"
100    format(F16.7, G16.7)
       read(fid_in,*,iostat=io) log10P, log10T
       P = 1d1**log10P
       T = 1d1**log10T
       write(*,*) "iostat = ", io
       write(*,*) "Read P = ", P, "T = ", T
       if(P .gt. 1d6) then
          call h5_get_Rho(mytbl, P, T, Rho, h5Pvect, h5Evect, h5Svect, ierr)
          if(ierr .eq. 0) then
             write(fid_out,*) log10(Rho), log10(T)
          else
             write(*,*) "lookup problem for file: ", filename_in
          endif
       endif
    end do
    close(unit=fid_in)
    close(unit=fid_out)
  end subroutine parse_file
end program simple
   
