program simple
  
  !! 1. Create a free energy EOS table first with numerical derivatives
  !! 2. Compare this table with the analytic table
  
  use h5table
  use ideal_water, only : get_IG_EOS, convDF_thermo
  
  implicit none  
  
  integer, parameter :: imax=128
  integer, parameter :: jmax=128
  integer, parameter :: h5Xsz = 128
  integer, parameter :: h5Ysz = 128

  integer :: i, j, fid
  double precision :: logtmin, logtmax, logrhomin, logrhomax, &
       logtstep, logrhostep
  double precision :: logrhovect(imax), rhovect(imax), lnrhovect(imax)
  double precision :: logtvect(jmax), tvect(jmax), lntvect(jmax)

  double precision :: nd_logRho(h5Xsz), nd_logT(h5Ysz)

  !! Compare results
  double precision :: logPmat(imax,jmax),logEmat(imax,jmax),logSmat(imax,jmax), &
       logPdiff(imax,jmax),logEdiff(imax,jmax),logSdiff(imax,jmax), &
       h5logP(imax,jmax), h5logE(imax,jmax), h5logS(imax,jmax), &
       free_energy(imax,jmax)
  double precision :: df_drho_diff(h5Xsz,h5Ysz), df_dt_diff(h5Xsz,h5Ysz), &
       d2f_drho2_diff(h5Xsz,h5Ysz), d2f_dt2_diff(h5Xsz,h5Ysz), d2f_drhodt_diff(h5Xsz,h5Ysz), &
       d3f_drho2dt_diff(h5Xsz,h5Ysz), d3f_drhodt2_diff(h5Xsz,h5Ysz)
  

  double precision :: f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
             d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
  
  
  type (h5tbl), pointer :: mytbl
  character (len=256) :: h5filename, h5name
  double precision :: lnRho, lnT, Rho, T, logT, logRho
  double precision, pointer, dimension(:) :: Pvect, Evect, Svect
  integer :: ierr, ios
  
  double precision :: free_cluster(3,3) !! take 9 points at every point to calculate numerical derivatives      
  
  double precision :: yD0, xD0, sigDX, sigDY, epsilon, mult, rad

  double precision :: delta = 1d-4
  double precision :: dX, dY, twodX, twodY, tmpvar
  double precision :: Xvect(3), Yvect(3)
  
  integer :: idx, idy
  double precision :: dfdx_num, dfdy_num, d2fdx2_num, d2fdy2_num, d2fdxdy_num, d3fdxdy2_num, d3fdx2dy_num, d4fdx2dy2_num
  
  !! Some variables for the 1D vector
  integer, parameter :: LEN = 1000
  
  character (len=256) :: filename
  integer, parameter :: VERBOSE = 0

  logical, parameter :: DO_2DCOMPARE = .true.
  logical, parameter :: DO_1DTEST = .false.
  
  allocate(mytbl)
  allocate(Pvect(6), Evect(6), Svect(6))
  
  !! Initiate the h5table
  
  h5filename = "waterfe.data"
  h5name = "water"
  ierr = 0
  fid= 20
  open(fid, file=h5filename)

  logtmin = 3.
  logtmax = 6.
  logrhomin = -3.
  logrhomax = 1.
  logrhostep = (logrhomax - logrhomin) / (h5Xsz - 1)
  logtstep = (logtmax - logtmin) / (h5Ysz - 1)
  
  do i=1, h5Xsz 
     nd_logRho(i) = logrhomin + (i-1) * logrhostep
  enddo

  do j=1, h5Ysz
     nd_logt(j) = logtmin + (j-1) * logtstep
  enddo

  !! 1. Build a table from scratch here - later set a flag so this isn't always run
  do i=1, h5Xsz
     do j=1, h5Ysz
        logRho = logRhomin + (i-1) * logrhostep
        logT = logTmin + (j-1) * logtstep
        Rho = 10**logRho
        T = 10**logT
        lnRho = log(Rho)
        lnT = log(T)

        !! Stencil
        dX = delta * Rho
        dY = delta * T
        
        !! Some superstition put in by numerical recepies
        tmpvar = Rho + dX
        call donothing(tmpvar)
        dX = tmpvar - Rho
        
        tmpvar = T + dY
        call donothing(tmpvar)
        dY = tmpvar - T

        !! setup stencil coordinates
        Xvect(1) = Rho - dX
        Xvect(2) = Rho
        Xvect(3) = Rho + dX
        Yvect(1) = T - dY
        Yvect(2) = T 
        Yvect(3) = T + dY        

        !! compute the ideal gas free energy at all 3x3 stencil cordinates
        do idx=1,3
           do idy=1,3
              lnRho = log(Xvect(idx))
              lnT = log(Yvect(idy))
              call get_IG_EOS(lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
                   d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
              free_cluster(idx,idy) = f_val
           enddo
        enddo
        
        !! get numeric derivatives, compare to analytic derivatives
        dfdx_num = (free_cluster(3,2) - free_cluster(1,2)) / (2.*dX)
        dfdy_num = (free_cluster(2,3) - free_cluster(2,1)) / (2.*dY)
        d2fdx2_num = (free_cluster(3,2) - 2*free_cluster(2,2) + free_cluster(1,2)) / (dX * dX)
        d2fdy2_num = (free_cluster(2,3) - 2*free_cluster(2,2) + free_cluster(2,1)) / (dY * dY)
        d2fdxdy_num = ((free_cluster(3,3) - free_cluster(3,1) - free_cluster(1,3) + free_cluster(1,1)) / (4. * dX * dY))
        d3fdxdy2_num = ((free_cluster(3,3) - 2.*free_cluster(3,2) + free_cluster(3,1)) &
             - (free_cluster(1,3) - 2. * free_cluster(1,2) + free_cluster(1,1))) / (dY*dY*2.*dX)
        d3fdx2dy_num = ((free_cluster(3,3) - 2. * free_cluster(2,3) + free_cluster(1,3)) &
             - (free_cluster(3,1) - 2. * free_cluster(2,1) + free_cluster(1,1))) / (dX*dX*2*dY)
        d4fdx2dy2_num = ((free_cluster(3,3) - 2.*free_cluster(3,2) + free_cluster(3,1)) &
             - 2 * (free_cluster(2,3) - 2. * free_cluster(2,2) + free_cluster(2,1)) &
             + (free_cluster(1,3) - 2. * free_cluster(1,2) + free_cluster(1,1))) / (dX*dX*dY*dY)
        
        write(fid,*) lnRho, lnT, f_val, dfdx_num, dfdy_num, d2fdx2_num, d2fdy2_num, &
             d2fdxdy_num, d3fdx2dy_num, d3fdxdy2_num, d4fdx2dy2_num
        
        Rho = Xvect(2)
        T = Yvect(2)
        lnRho = log(Rho)
        lnT = log(T)
        call get_IG_EOS(lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
                   d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
        
        
        !!! Build up matrices of relative differences between numerical derivative and 
        !!! actual analytic derivative
        df_drho_diff(i,j) = (dfdx_num - df_drho) / df_drho
        df_dt_diff(i,j) = (dfdy_num - df_dt) / df_dt
        d2f_drho2_diff(i,j) = (d2fdx2_num - d2f_drho2) / d2f_drho2
        d2f_dt2_diff(i,j) = (d2fdy2_num - d2f_dt2) / d2f_dt2
        d2f_drhodt_diff(i,j) = (d2fdxdy_num - d2f_drhodt) / d2f_drhodt
        d3f_drho2dt_diff(i,j) = (d3fdx2dy_num - d3f_drho2dt) 
        d3f_drhodt2_diff(i,j) = (d3fdxdy2_num - d3f_drhodt2) 
     enddo
  enddo
  close(fid)
  
  filename = "h5tdata/nd_logRhovect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') nd_logRho
  close(unit=19)
  
  filename = "h5tdata/nd_logTvect.data"
  open(unit=19,file=trim(filename),status='replace', &
       iostat=ios,action='write',form='formatted')
  write(19,'(e14.6)') nd_logT
  close(unit=19)
  
  filename = "h5tdata/nd_dfdrho_rdiff.data"
  call write_matrix(df_drho_diff, filename)
  
  filename = "h5tdata/nd_dfdt_rdiff.data"
  call write_matrix(df_dt_diff, filename)

  filename = "h5tdata/nd_d2fdrho2_rdiff.data"
  call write_matrix(d2f_drho2_diff, filename)

  filename = "h5tdata/nd_d2fdt2_rdiff.data"
  call write_matrix(d2f_dt2_diff, filename)

  filename = "h5tdata/nd_d2fdrhodt_rdiff.data"
  call write_matrix(d2f_drhodt_diff, filename)

  filename = "h5tdata/nd_d3fdrho2dt_rdiff.data"
  call write_matrix(d3f_drho2dt_diff, filename)

  filename = "h5tdata/nd_d3fdrhodt2_rdiff.data"
  call write_matrix(d3f_drhodt2_diff, filename)

  

  if(DO_2DCOMPARE) then

     call setup_h5tbl(mytbl, h5Xsz, h5Ysz, h5filename, h5name, ierr)
     
     !! h5 Table initiated!!
     
     logtmin = 4.
     logtmax = 4.5
     logrhomin = 0
     logrhomax = 0.5
     
     logrhostep = (logrhomax - logrhomin) / (imax - 1)
     logtstep = (logtmax - logtmin) / (jmax - 1)
     
     do i=1,imax
        logrhovect(i) = logrhomin + logrhostep * (i-1)
        rhovect(i) = 10**logrhovect(i)
        lnrhovect(i) = log(rhovect(i))
     enddo
     
     do j=1,jmax
        logtvect(j) = logtmin + logtstep * (j-1)
        tvect(j) = 10**logtvect(j)
        lntvect(j) = log(tvect(j))
     enddo
     
     do i=1,imax
        do j=1, jmax
           lnRho = lnrhovect(i)
           Rho = rhovect(i)
           
           lnT = lntvect(j)
           T = tvect(j)

           !! Get equation of state from h5 table
           call get_val(mytbl, Rho, T, Pvect, Evect, Svect, ierr)
           logPmat(i,j) = log10(Pvect(i_val))
           logEmat(i,j) = log10(Evect(i_val))
           logSmat(i,j) = log10(Svect(i_val))
           
           !! Get equation of state from analytic expression
           call get_IG_EOS(lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
                d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
           call convDF_thermo(Rho, T, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
                d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
                Pvect, Evect, Svect)
           
           
           h5logP(i,j) = log10(Pvect(i_val))
           h5logE(i,j) = log10(Evect(i_val))
           h5logS(i,j) = log10(Svect(i_val))
           
           
           logPdiff(i,j) = logPmat(i,j) - h5logP(i,j)
           logEdiff(i,j) = logEmat(i,j) - h5logE(i,j)
           logSdiff(i,j) = logSmat(i,j) - h5logS(i,j)
           
           
        enddo
     enddo
     
     !! Write out the differences to a file
     
     
     
     filename = "h5tdata/h5t_logRhovect.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')
     write(19,'(e14.6)') logRhovect
     close(unit=19)
     
     filename = "h5tdata/h5t_logTvect.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')
     write(19,'(e14.6)') logtvect
     close(unit=19)
     
     filename = "h5tdata/h5t_logPdiff.data"
     call write_matrix(logPdiff, filename)
     
     filename = "h5tdata/h5t_logEdiff.data"
     call write_matrix(logEdiff, filename)
     
     filename = "h5tdata/h5t_logSdiff.data"
     call write_matrix(logSdiff, filename)
     
     filename = "h5tdata/an_logP.data"
     call write_matrix(logPmat, filename)
     
     filename = "h5tdata/an_logE.data"
     call write_matrix(logEmat, filename)
     
     filename = "h5tdata/an_logS.data"
     call write_matrix(logSmat, filename)
     
     filename = "h5tdata/h5t_logP.data"
     call write_matrix(h5logP, filename)
     
     filename = "h5tdata/h5t_logE.data"
     call write_matrix(h5logE, filename)
     
     filename = "h5tdata/h5t_logS.data"
     call write_matrix(h5logS, filename)
  endif
     
!!! Lets plot a 1D line of free energy(T) as a result of the h5tbl 
  
  

  if(DO_1DTEST .and. DO_2DCOMPARE) then
     !! Pick an index somewhere in the middle
     Rho = mytbl%Rho_v(h5Xsz / 2) !! I'm interested in what happens along a grid point
     write(*,*) "Picking Rho = ", Rho, " for a verticle slice"
     
     
     logtmin = 4.
     logtmax = 4.1
     logtstep = (logtmax - logtmin) / (LEN - 1)
     
     do i=1,LEN
        
        
!        call get_val(mytbl, rrhoi, ttti, Pvect, Evect, Svect, ierr)

     
     enddo

  endif
     
  if(DO_2DCOMPARE) then
     call free_h5tbl(mytbl)
  endif

  !! Clean up the h5 table     
  if(associated(mytbl)) deallocate(mytbl)
  if(associated(Pvect)) deallocate(Pvect)
  if(associated(Evect)) deallocate(Evect)
  if(associated(Svect)) deallocate(Svect)
  
contains
  subroutine donothing(temp)
    double precision, intent(inout) :: temp
  end subroutine donothing

  subroutine write_matrix(matrix, filename)
    double precision, intent(in), dimension(:,:) :: matrix
    character (len=256) :: filename
    
    open(unit=19,file=trim(filename),status='replace', &
         iostat=ios,action='write',form='formatted')       
    write(19,'(e14.6)') matrix
    close(unit=19)
  end subroutine write_matrix
end program simple
   
!      include 'aneosequ.f'
