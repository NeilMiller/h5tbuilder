program simple
  
  !! Compare the pure water ideal gas law to the aneos pure equation of state
  
  use convolve
  use h5table
  use ideal_water, only : get_IG_EOS, convDF_thermo
  
  implicit none  

  integer, parameter :: maxmat=21
  integer, parameter :: imax=128
  integer, parameter :: jmax=128
  integer :: imat,nntype(maxmat),klst,kinp,kpai
  double precision :: tti,ttti,rrhoi,ppi,uui,ssi,ccvi
  double precision :: ddpdti,ddpdri,ffkrosi,ccsi,ffve,ffva
  
  integer :: i, j, outfid, outfid2
  double precision :: logtmin, logtmax, logrhomin, logrhomax, &
       logtstep, logrhostep
  double precision :: logrhovect(imax), rhovect(imax), lnrhovect(imax)
  double precision :: logtvect(jmax), tvect(jmax), lntvect(jmax)
  double precision :: logPmat(imax,jmax),logEmat(imax,jmax),logSmat(imax,jmax), &
       logPdiff(imax,jmax),logEdiff(imax,jmax),logSdiff(imax,jmax), &
       h5logP(imax,jmax), h5logE(imax,jmax), h5logS(imax,jmax), &
       free_energy(imax,jmax)
  double precision :: f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
             d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
  
  
  type (h5tbl), pointer :: mytbl
  character (len=256) :: h5filename, h5name
  double precision :: lnRho, lnT
  double precision, pointer, dimension(:) :: Pvect, Evect, Svect
  integer :: h5Xsz, h5Ysz
  integer :: ierr, ios, nmat
  
  double precision :: free_cluster(3,3) !! take 9 points at every point to calculate numerical derivatives      
  
  external aneos,aneos2
  common /fileos/ klst, kinp
  double precision, parameter :: evtemp = 11604.8d0
  
  double precision :: yD0, xD0, sigDX, sigDY, epsilon, mult, rad

  double precision :: delta = 1d-4
  double precision :: dX, dY, twodX, twodY, tmpvar
  double precision :: Xvect(3), Yvect(3)
  
  integer :: idx, idy
  double precision :: dfdx_num, dfdy_num, d2fdx2_num, d2fdy2_num, d2fdxdy_num, d3fdxdy2_num, d3fdx2dy_num, d4fdx2dy2_num
  
  !! Some variables for the 1D vector
  integer, parameter :: LEN = 1000
  
  character (len=256) :: filename
  integer, parameter :: VERBOSE = 0
  
  !! Initiate the h5table
  
  h5filename = "waterfe.data"
  h5name = "water"
  ierr = 0
  h5Xsz = 512
  h5Ysz = 512

  allocate(mytbl)
  allocate(Pvect(6), Evect(6), Svect(6))
  call setup_h5tbl(mytbl, h5Xsz, h5Ysz, h5filename, h5name, ierr)
  
  !! h5 Table initiated!!
  
  
  nntype(:) = 0
  logtmin = 5.
  logtmax = 3
  logrhomin = 1
  logrhomax = -3.
  
  logrhostep = (logrhomax - logrhomin) / (imax - 1)
  logtstep = (logtmax - logtmin) / (jmax - 1)
  
  do i=1,imax
     logrhovect(i) = logrhomin + logrhostep * (i-1)
     rhovect(i) = 10**logrhovect(i)
     lnrhovect(i) = log(rhovect(i))
  enddo
  
  do j=1,jmax
     logtvect(j) = logtmin + logtstep * (j-1)
     tvect(j) = 10**logtvect(j)
     lntvect(j) = log(tvect(j))
  enddo
  
  !--INITIALIZATION PARAMETERS
  
  outfid = 20
  outfid = 23
  kinp=21
  klst=22
  
  ! Material code
  ! 1 = water
  ! 2 = ice
  ! 3 = Serpentine
  ! 4 = DUNITE with Molecules
  ! 5 = Iron
!      imat=2
  do imat=1,1
     
     if(imat.eq.1) then
!        open(outfid,file='water.data') 
        open(kinp,file='water.input')
        open(klst, file='water.output')
!        open(outfid2,file='waterfe.data')
     elseif(imat.eq.2) then
!        open(outfid,file='ice.data') 
        open(kinp,file='ice.input')
        open(klst, file='ice.output')
!        open(outfid2,file='icefe.data')
     elseif(imat.eq.3) then
!        open(outfid,file='serpentine.data') 
        open(kinp,file='serpentine.input')
        open(klst, file='serpentine.output')
!        open(outfid2,file='serpentinefe.data')
     elseif(imat.eq.4) then
!        open(outfid,file='dunite.data') 
        open(kinp,file='aneosdunite.input')
        open(klst, file='dunite.output')
!        open(outfid2,file='dunitefe.data')
     elseif(imat.eq.5) then
!        open(outfid,file='iron.data') 
        open(kinp,file='aneosiron.input')
        open(klst, file='iron.output')            
!        open(outfid2,file='ironfe.data')
     endif
     
     ! Open file and prepare to write
     
!
!--SET TO imat > 1  IF MORE MATERIAL TYPES ARE REQUIRED
!
     nmat=1
     nntype(1)=-imat
     
     write(*,*) imat, nmat, nntype
     !
     !--INITIALIZATION SUBROUTINE
     !
     call aneos2(1,nmat,0,nntype)
     
         
     do i=1,imax
        
        rrhoi = rhovect(i)
        do j=1,jmax            
           kpai = 2
           
           ttti = tvect(j)   !! This number is in Kelvin
           
           !! Test values
           !               rrhoi = 1.
           !               ttti = 10000.
           
!!! Convert ttti (K) -> tti (eV)
           tti = ttti / evtemp                 
           
           if(VERBOSE .eq. 1) then
              write(*,*) "Rho = ", rrhoi, "[g/cc]; T = ", ttti, " [ K ]"
           endif
           !               write(*,*) log10(rhovect(i)), log10(tvect(j))
           
           !--MAIN SUBROUTINE TO CALL
           !
           
           
           dX = rrhoi * delta
           dY = tti * delta
           
           if(VERBOSE .eq. 1) then
              write(*,*) dX
           endif
           tmpvar = rrhoi + dX
           call donothing(tmpvar)
           dX = tmpvar - rrhoi
           if(VERBOSE .eq. 1) then
              write(*,*) dX
           endif
           
           tmpvar = tti + dY
           call donothing(tmpvar)
           dY = tmpvar - tti
           
           Xvect(1) = rrhoi - dX
           Xvect(2) = rrhoi
           Xvect(3) = rrhoi + dX
           Yvect(1) = tti - dY
           Yvect(2) = tti 
           Yvect(3) = tti + dY
           
           !               dX = (Xvect(3) - Xvect(2))
           !               dY = (Yvect(3) - Yvect(2))
           
           do idx=1,3
              do idy=1,3
                 call aneos(Yvect(idY), Xvect(idX), ppi, uui, ssi, ccvi, ddpdti, &
                      ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
                 
                 free_cluster(idx,idy) = uui - Yvect(idY) * ssi
                 if(VERBOSE .eq. 1) then
                    write(*,*) idx, idy, free_cluster(idx, idy), Xvect(idx), Yvect(idy), uui, -Yvect(idy) * ssi
                 endif
              enddo
           enddo
           
           dfdx_num = (free_cluster(3,2) - free_cluster(1,2)) / (2.*dX)
           dfdy_num = (free_cluster(2,3) - free_cluster(2,1)) / (2.*dY)
           d2fdx2_num = (free_cluster(3,2) - 2*free_cluster(2,2) + free_cluster(1,2)) / (dX * dX)
           d2fdy2_num = (free_cluster(2,3) - 2*free_cluster(2,2) + free_cluster(2,1)) / (dY * dY)
           d2fdxdy_num = ((free_cluster(3,3) - free_cluster(3,1) - free_cluster(1,3) + free_cluster(1,1)) / (4. * dX * dY))
           d3fdxdy2_num = ((free_cluster(3,3) - 2.*free_cluster(3,2) + free_cluster(3,1)) &
                - (free_cluster(1,3) - 2. * free_cluster(1,2) + free_cluster(1,1))) / (dY*dY*2.*dX)
           d3fdx2dy_num = ((free_cluster(3,3) - 2. * free_cluster(2,3) + free_cluster(1,3)) &
                - (free_cluster(3,1) - 2. * free_cluster(2,1) + free_cluster(1,1))) / (dX*dX*2*dY)
           d4fdx2dy2_num = ((free_cluster(3,3) - 2.*free_cluster(3,2) + free_cluster(3,1)) &
                - 2 * (free_cluster(2,3) - 2. * free_cluster(2,2) + free_cluster(2,1)) &
                + (free_cluster(1,3) - 2. * free_cluster(1,2) + free_cluster(1,1))) / (dX*dX*dY*dY)
           
           call aneos (tti,rrhoi,ppi,uui,ssi,ccvi,ddpdti, &
                ddpdri,ffkrosi,ccsi,kpai,imat,ffve,ffva)
           
           free_energy(i,j) = uui - tti * ssi
           
           if(VERBOSE .eq. 1) then
              
              write(*,*) "Rho: ", rrhoi, dX
              write(*,*) "tti: ", tti, dY
              write(*,*) "free energy: ", free_energy(i,j)
              write(*,*) "dfdx: ", dfdx_num, ppi / rrhoi**2.
              write(*,*) "dfdy: ", dfdy_num, -ssi, dfdy_num / evtemp
              write(*,*) "d2fdx2: ", d2fdx2_num
              write(*,*) "d2fdy2: ", d2fdy2_num, -ccvi / tti, d2fdy2_num / evtemp**2
              write(*,*) "d2fdxdy: ", d2fdxdy_num, ddpdti / rrhoi**2, d2fdxdy_num / evtemp
              write(*,*) "d3fdxdy2: ", d3fdxdy2_num, d3fdxdy2_num / evtemp**2
              write(*,*) "d3fdx2dy: ", d3fdx2dy_num, d3fdx2dy_num / evtemp
              write(*,*) "d4fdx2dy2: ", d4fdx2dy2_num, d4fdx2dy2_num / evtemp**2
              write(*,*)
              write(*,*) "ppi: ", ppi
              write(*,*) "uui: ", uui, free_energy(i,j) -  tti * dfdy_num
              write(*,*) "ssi: ", ssi
              write(*,*) "ccvi: ", ccvi, -tti * d2fdy2_num
              write(*,*) "ddpdti: ", ddpdti, rrhoi**2 * d2fdxdy_num
              write(*,*) "ddpdri: ", ddpdri, 2 * rrhoi * dfdx_num + rrhoi**2 * d2fdx2_num
              write(*,*) "ffkrosi: ", ffkrosi
              write(*,*) "ccsi: ", ccsi
              write(*,*) "kpai: ", kpai
              write(*,*) "ffve: ", ffve
              write(*,*) "ffva: ", ffva
           endif
           
           logPmat(i,j) = log10(ppi)
           logEmat(i,j) = log10(uui)
           logSmat(i,j) = log10(ssi / evtemp) !!! This is not right
           if(isnan(logPmat(i,j))) then
              logPmat(i,j) = -100
              write(*,*) rrhoi, ttti
           endif
           if(isnan(logSmat(i,j))) then 
              logSmat(i,j) = 10
           endif
           if(isnan(logEmat(i,j))) then
              logEmat(i,j) = -100
           endif
           
           lnRho = log(rrhoi)
           lnT = log(ttti)
           
           call get_IG_EOS(lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
             d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
           call convDF_thermo(rrhoi, ttti, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
                d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
                Pvect, Evect, Svect)
           
           
           h5logP(i,j) = log10(Pvect(i_val))
           h5logE(i,j) = log10(Evect(i_val))
           h5logS(i,j) = log10(Svect(i_val))
           
           
           logPdiff(i,j) = logPmat(i,j) - h5logP(i,j)
           logEdiff(i,j) = logEmat(i,j) - h5logE(i,j)
           logSdiff(i,j) = logSmat(i,j) - h5logS(i,j)
           
           
        enddo
     enddo
     
     !! Write out the differences to a file
     
     
     
     filename = "h5tdata/h5t_logRhovect.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')
     write(19,'(e14.6)') logRhovect
     close(unit=19)
     
     filename = "h5tdata/h5t_logTvect.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')
     write(19,'(e14.6)') logtvect
     close(unit=19)
     
     filename = "h5tdata/h5t_logPdiff.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') logPdiff
     close(unit=19)
     
     filename = "h5tdata/h5t_logEdiff.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') logEdiff
     close(unit=19)
     
     filename = "h5tdata/h5t_logSdiff.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') logSdiff
     close(unit=19)
     
     filename = "h5tdata/an_logP.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') logPmat
     close(unit=19)
     
     filename = "h5tdata/an_logE.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') logEmat
     close(unit=19)
     
     filename = "h5tdata/an_logS.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') logSmat
     close(unit=19)
     
     filename = "h5tdata/h5t_logP.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') h5logP
     close(unit=19)
     
     filename = "h5tdata/h5t_logE.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') h5logE
     close(unit=19)
     
     filename = "h5tdata/h5t_logS.data"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     write(19,'(e14.6)') h5logS
     close(unit=19)


!!! Lets plot a 1D line of free energy(T) as a result of the h5tbl 
     

     !! Pick an index somewhere in the middle
     rrhoi = mytbl%Rho_v(h5Xsz / 2) !! I'm interested in what happens along a grid point
     write(*,*) "Picking Rho = ", rrhoi, " for a verticle slice"
     

     logtmin = 4.
     logtmax = 4.1
     logtstep = (logtmax - logtmin) / (LEN - 1)
     
     do i=1,LEN
        
        ttti = 10**(logtmin + (i-1) * logtstep)
        tti = ttti / evtemp
        
        call aneos (tti,rrhoi,ppi,uui,ssi,ccvi,ddpdti, &
             ddpdri,ffkrosi,ccsi,kpai,imat,ffve,ffva)
        
        free_energy(i,j) = uui - tti * ssi
        
        call get_val(mytbl, rrhoi, ttti, Pvect, Evect, Svect, ierr)
        
        write(*,*) ttti, Evect(1), uui
        
     enddo
     
!     close(outfid)
!     close(outfid2)
     close(kinp)
     close(klst) 
     
     !! Clean up the h5 table
     
     call free_h5tbl(mytbl)
     deallocate(mytbl)
     deallocate(Pvect)
     deallocate(Evect)
     deallocate(Svect)
     
  enddo
  
contains
  subroutine donothing(temp)
    double precision, intent(inout) :: temp
  end subroutine donothing
end program simple
   
!      include 'aneosequ.f'
