program simple
  use aneos_access
  implicit none
  
  double precision :: Rho, T, P, E, S
  double precision :: time1, time2
  integer :: i

  call ANEOSWater_Init

  Rho = 3.
  T = 1d5
  call CPU_TIME(time1)
  do i=1,100000
     call ANEOS_get_PES(Rho, T, P, E, S)
  enddo
  call CPU_TIME(time2)

  write(*,*) "ANEOS Water"
  write(*,*) "Rho = ", Rho
  write(*,*) "T = ", T
  write(*,*) "P = ", P
  write(*,*) "E = ", E
  write(*,*) "S = ", S
  
  write(*,*) "time: " , time2 - time1

  call ANEOS_Shutdown

end program simple

