program simple
  !! Plot out 1D slices of the P,E,S profile & the original 

  use aneos_access
  use h5table

  implicit none  

  integer, parameter :: imax=200
  integer, parameter :: jmax=5
  
  integer :: i, j
  double precision :: logtmin, logtmax, logrhomin, logrhomax, &
       logtstep, logrhostep
  double precision :: logrhovect(imax), rhovect(imax)
  double precision :: logtvect(jmax), tvect(jmax)
  
  type (h5tbl), pointer :: mytbl
  character (len=256) :: h5filename, h5name
  double precision, pointer, dimension(:) :: h5Pvect, h5Evect, h5Svect
  integer :: h5Xsz, h5Ysz
  integer :: ierr, ios
  
  double precision :: Fvect(9), Pan, Ean, San, Rho, T
  double precision :: minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta

  character (len=256) :: filename, filenum
  integer, parameter :: VERBOSE = 0
  
!  logical, parameter :: write_to_files = .true.
  
  !! Initiate the h5table
  
  h5filename = "data/waterfe.bin"
  h5name = "water"
  ierr = 0
  h5Xsz = 2048
  h5Ysz = 2048
  
  allocate(mytbl)

  allocate(h5Pvect(6), h5Evect(6), h5Svect(6))
  write(*,*) "Setting up h5 table from file: ", h5filename
  call setup_h5tbl(mytbl, h5Xsz, h5Ysz, h5filename, h5name, ierr)
  minlogRhoAlpha = 2.
  minlogRhoBeta = -10.
  maxlogRhoAlpha = 8.
  maxlogRhoBeta = -26.
  
  call slice_h5table(mytbl, minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta)
  if(ierr .eq. 0) then
     write(*,*) "h5 table setup SUCCESS"
  else
     write(*,*) "h5 table setup FAIL"
  end if

  !! h5 Table initiated!!  
  !! Initiate the aneos Water table
  call ANEOSWater_Init


  logtmin = 3.1
  logtmax = 5.
  logrhomin = -2
  logrhomax = 1.9
  
  logrhostep = (logrhomax - logrhomin) / (imax - 1)
  logtstep = (logtmax - logtmin) / (jmax - 1)
  

  !! Rho is in g/cc
  do i=1,imax
     logrhovect(i) = logrhomin + logrhostep * (i-1)
     rhovect(i) = 10**logrhovect(i)
  enddo

  !! T is in K
  do j=1,jmax
     logtvect(j) = logtmin + logtstep * (j-1)
     tvect(j) = 10**logtvect(j)
  enddo
  
  do j=1,jmax !! Do slices of constant temperature
     write(filenum,'(i5.5)') j
     filename = "h5tdata/slice" // trim(filenum) // ".dat"
     open(unit=19,file=trim(filename),status='replace', &
          iostat=ios,action='write',form='formatted')       
     
     do i=1,imax
    


        Rho = rhovect(i)
        T = tvect(j)
        
        call ANEOS_get_PES(Rho, T, Pan, Ean, San)  !! This is more pure
        call ANEOS_get_free(Rho,T, Fvect)          !! This result involves numerical derivatives

        call h5_get_val(mytbl, Rho, T, h5Pvect, h5Evect, h5Svect, ierr)
        if(ierr .eq. 0) then           
!           write(*,*) " H5 output (ierr=0) (P,E,S) = ", h5Pvect(i_val), h5Evect(i_val), h5Svect(i_val)
        else
           h5Pvect(:) = 1d-100
           h5Evect(:) = 1d-100
           h5Svect(:) = 1d-100
           
!           write(*,*) "Calling aneos: ", Rho, T
!           write(*,*) " ANEOS output (P,E,S)          = ", Pan, Ean, San           
!           write(*,*) "h5 ierr = ", ierr
        endif
        write(19,*) Rho, T, Pan, Ean, San, h5Pvect(a_val), h5Evect(a_val), h5Svect(a_val)        
        
     enddo

     close(unit=19)

  enddo
  
  !! Write out the differences to a file
  
  
  
  
  call free_h5tbl(mytbl)
  call ANEOS_Shutdown
  deallocate(mytbl)
  deallocate(h5Pvect)
  deallocate(h5Evect)
  deallocate(h5Svect)
  
contains
  subroutine donothing(temp)
    double precision, intent(inout) :: temp
  end subroutine donothing
  
  
end program simple
   
