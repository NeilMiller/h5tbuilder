program simple
!
!--SIMPLE PROGRAM TO SEE HOW ANEOS IS INITIALIZED
  !
  implicit none
        
  integer, parameter :: maxmat=21
  integer, parameter :: imax=128
  integer, parameter :: jmax=128
  integer :: imat,nntype(maxmat),klst,kinp,kpai
  double precision :: tti,ttti,rrhoi,ppi,uui,ssi,ccvi
  double precision :: ddpdti,ddpdri,ffkrosi,ccsi,ffve,ffva
  
  integer :: i, j, outfid, outfid2
  double precision :: logtmin, logtmax, logrhomin, logrhomax, &
       logtstep, logrhostep
  double precision :: logrhovect(imax), rhovect(imax), lnrhovect(imax), VolVect(imax)
  double precision :: logtvect(jmax), tvect(jmax), lntvect(jmax)
  double precision :: logPmat(imax,jmax),logEmat(imax,jmax),logSmat(imax,jmax), &
       logPcon(imax,jmax),logEcon(imax,jmax),logScon(imax,jmax), &
       free_energy(imax,jmax), dfdrho_mat(imax,jmax), dfdt_mat(imax, jmax), &
       d2fdrho2_mat(imax,jmax), d2fdt2_mat(imax,jmax), d2fdrhodt_mat(imax,jmax), &
       d3fdrhodt2_mat(imax,jmax), d3fdrho2dt_mat(imax,jmax), d4fdrho2dt2_mat(imax,jmax), &
       d3fdrho2dt_con(imax,jmax), d3fdrhodt2_con(imax,jmax), d4fdrho2dt2_con(imax,jmax), &
       d2fdrho2_con(imax,jmax), d2fdt2_con(imax,jmax), d2fdrhodt_con(imax,jmax), &
       output_mat(imax,jmax), d2fdrho2_nmat(imax,jmax), d2fdt2_nmat(imax,jmax), d2fdrhodt_nmat(imax,jmax), &
       d2fdrho2_diff(imax,jmax), d2fdt2_diff(imax,jmax), convlog_mat(imax, jmax), &
       free_energy2(imax,jmax), Pmat(imax,jmax), Emat(imax,jmax), Smat(imax,jmax)
  
  !! take 9 points at every point to calculate numerical derivatives      
  double precision :: free_cluster(3,3), dfdx_cl(3,3),dfdy_cl(3,3),&
       d2fdx2_cl(3,3), d2fdy2_cl(3,3), d2fdxdy_cl(3,3)
  
  
  external aneos,aneos2
  COMMON /FILEOS/ klst, kinp
  double precision, parameter :: evtemp = 11604.8d0
  
  character (len=256) :: filename, prefix
  double precision :: yD0, xD0, sigDX, sigDY, epsilon, mult, rad
  
  double precision :: delta = 1d-4
  double precision :: delta_h = 1d-4
  double precision :: dX, dY, tmpvar
  double precision :: Xvect(3), Yvect(3)
  
  integer :: idx, idy
  double precision :: dfdx_num, dfdy_num, d2fdx2_num, d2fdy2_num, d2fdxdy_num, d3fdxdy2_num, d3fdx2dy_num, d4fdx2dy2_num
  double precision :: dfdx_an, dfdy_an, d2fdx2_an, d2fdy2_an, d2fdxdy_an
  
  integer :: ios, nmat
  
  nntype(:) = 0
  logtmin = 2.5
  logtmax = 6.
  logrhomin = -3.
  logrhomax = 1.
  logrhostep = (logrhomax - logrhomin) / (imax - 1)
  logtstep = (logtmax - logtmin) / (jmax - 1)
  
  do i=1,imax
     logrhovect(i) = logrhomin + logrhostep * (i-1)
     rhovect(i) = 10**logrhovect(i)
     lnrhovect(i) = log(rhovect(i))
     VolVect(i) = (1. / rhovect(i))
  enddo
  
  do j=1,jmax
     logtvect(j) = logtmin + logtstep * (j-1)
     tvect(j) = 10**logtvect(j)
     lntvect(j) = log(tvect(j))
  enddo
      
!--INITIALIZATION PARAMETERS
      
  outfid = 20
  outfid = 23
  kinp=21
  klst=22
      
! Material code
! 1 = water
! 2 = ice
! 3 = Serpentine
! 4 = DUNITE with Molecules
! 5 = Iron
!      imat=2
  do imat=1,5
         
     if(imat.eq.1) then
        open(outfid,file='water.data') 
        open(kinp,file='water.input')
        open(klst, file='water.output')
        open(outfid2,file='waterfe.data')
        !            xD0 = -3
        !            sigDX = 2.6
        !            yD0 = 2.0
!            sigDY = 1.0
!            epsilon = 0.5
     elseif(imat.eq.2) then
        open(outfid,file='ice.data') 
        open(kinp,file='ice.input')
        open(klst, file='ice.output')
        open(outfid2,file='icefe.data')
     elseif(imat.eq.3) then
        open(outfid,file='serpentine.data') 
        open(kinp,file='serpentine.input')
        open(klst, file='serpentine.output')
        open(outfid2,file='serpentinefe.data')
     elseif(imat.eq.4) then
        open(outfid,file='dunite.data') 
        open(kinp,file='aneosdunite.input')
        open(klst, file='dunite.output')
        open(outfid2,file='dunitefe.data')
     elseif(imat.eq.5) then
        open(outfid,file='iron.data') 
        open(kinp,file='aneosiron.input')
        open(klst, file='iron.output')            
        open(outfid2,file='ironfe.data')
     endif
         
! Open file and prepare to write
         
!
!--SET TO imat > 1  IF MORE MATERIAL TYPES ARE REQUIRED
!
     nmat=1
     nntype(1)=-imat
     
     write(*,*) imat, nmat, nntype
!
!--INITIALIZATION SUBROUTINE
     !
     call aneos2(1,nmat,0,nntype)
     write(*,*) "ANEOS initialized"
     
     do i=1,imax
        rrhoi = rhovect(i)
        do j=1,jmax            
           kpai = 2
           
           ttti = tvect(j)
           tti = ttti / evtemp
           
           !               write(*,*) log10(rhovect(i)), log10(tvect(j))

!--MAIN SUBROUTINE TO CALL
               !               

           dX = rrhoi * delta
           dY = tti * delta

!               tmpvar = rrhoi + dX
!               call donothing(tmpvar)
!               dX = tmpvar - rrhoi

!               tmpvar = tti + dY
!               call donothing(tmpvar)
!               dY = tmpvar - tti

           Xvect(1) = rrhoi - dX
           Xvect(2) = rrhoi
           Xvect(3) = rrhoi + dX
           Yvect(1) = tti - dY
           Yvect(2) = tti
           Yvect(3) = tti + dY
               
           do idx=1,3
              do idy=1,3
                 call aneos(Yvect(idY), Xvect(idX), ppi, uui, ssi, ccvi, ddpdti, &
                      ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
                 
                 free_cluster(idx,idy) = uui - Yvect(idY) * ssi
                 dfdx_cl(idx,idy)= ppi / Xvect(idX)**2d0
                 dfdy_cl(idx,idy) = -ssi
                 d2fdx2_cl(idx,idy) = (1. / Xvect(idX)**2d0) * (ddpdri - 2d0 * ppi / Xvect(idx))
                 d2fdy2_cl(idx,idy) = -ccvi / Yvect(idY)
                 d2fdxdy_cl(idx,idy) = (1. / Xvect(idX)**2d0) * ddpdti
                 !                     write(*,*) Yvect(idY), Xvect(idX), ppi, uui, ssi
              enddo
           enddo
           
           dfdx_num = (free_cluster(3,2) - free_cluster(1,2)) / (2.*dX)
           dfdy_num = (free_cluster(2,3) - free_cluster(2,1)) / (2.*dY)
           d2fdx2_num = (free_cluster(3,2) - 2*free_cluster(2,2) + free_cluster(1,2)) / (dX * dX)
           d2fdy2_num = (free_cluster(2,3) - 2*free_cluster(2,2) + free_cluster(2,1)) / (dY * dY)
           d2fdxdy_num = ((free_cluster(3,3) - free_cluster(3,1) - free_cluster(1,3) + free_cluster(1,1)) / (4. * dX * dY))

           dX = rrhoi * delta_h
           dY = tti * delta_h
           Xvect(1) = rrhoi - dX
           Xvect(2) = rrhoi
           Xvect(3) = rrhoi + dX
           Yvect(1) = tti - dY
           Yvect(2) = tti
           Yvect(3) = tti + dY
               
           do idx=1,3
              do idy=1,3
                 call aneos(Yvect(idY), Xvect(idX), ppi, uui, ssi, ccvi, ddpdti, &
                      ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
                 
                 free_cluster(idx,idy) = uui - Yvect(idY) * ssi
                 dfdx_cl(idx,idy)= ppi / Xvect(idX)**2d0
                 dfdy_cl(idx,idy) = -ssi
                 d2fdx2_cl(idx,idy) = (1. / Xvect(idX)**2) * (ddpdri - 2d0 * ppi / Xvect(idx))
                 d2fdy2_cl(idx,idy) = -ccvi / Yvect(idY)
                 d2fdxdy_cl(idx,idy) = (1. / Xvect(idX)**2d0) * ddpdti
                 !                     write(*,*) Yvect(idY), Xvect(idX), ppi, uui, ssi
              enddo
           enddo
               
           d3fdxdy2_num = (d2fdxdy_cl(2,3) - d2fdxdy_cl(2,1)) / (2.*dY)
           d3fdx2dy_num = (d2fdxdy_cl(3,2) - d2fdxdy_cl(1,2)) / (2.*dX)
           d4fdx2dy2_num = (d2fdxdy_cl(3,3) - d2fdxdy_cl(3,1) - d2fdxdy_cl(1,3) + d2fdxdy_cl(1,1)) / (4.*dX*dY)
               
               !! There is probably too much error associated with these.
               !! Ideally we will modify the ANEOS code directly to output the 3rd and 4th derivatives, but for now lets just take
               !!  the numerical derivatives.
!               d3fdxdy2_num = ((free_cluster(3,3) - 2.*free_cluster(3,2) + free_cluster(3,1)) &
!                    - (free_cluster(1,3) - 2. * free_cluster(1,2) + free_cluster(1,1))) / (dY*dY*2.*dX)
!               d3fdx2dy_num = ((free_cluster(3,3) - 2. * free_cluster(2,3) + free_cluster(1,3)) &
!                    - (free_cluster(3,1) - 2. * free_cluster(2,1) + free_cluster(1,1))) / (dX*dX*2*dY)
!               d4fdx2dy2_num = ((free_cluster(3,3) - 2.*free_cluster(3,2) + free_cluster(3,1)) &
!                    - 2 * (free_cluster(2,3) - 2. * free_cluster(2,2) + free_cluster(2,1)) &
!                    + (free_cluster(1,3) - 2. * free_cluster(1,2) + free_cluster(1,1))) / (dX*dX*dY*dY)

           call aneos (tti,rrhoi,ppi,uui,ssi,ccvi,ddpdti, &
                ddpdri,ffkrosi,ccsi,kpai,imat,ffve,ffva)
           
           dfdx_an = ppi / rrhoi**2d0
           dfdy_an = -ssi
           d2fdx2_an = (1/rrhoi**2d0) * (ddpdri - 2d0 * ppi / rrhoi)
           d2fdy2_an = -ccvi / tti
           d2fdxdy_an = (1/rrhoi**2d0) * ddpdti
           
               !! Generally use the analytic ones
           dfdrho_mat(i,j) = dfdx_an
           dfdt_mat(i,j) = dfdy_an
           d2fdrho2_mat(i,j) = d2fdx2_an
           d2fdt2_mat(i,j) = d2fdy2_an
           d2fdrhodt_mat(i,j) = d2fdxdy_an
           
               !! Use the numeric ones possibly?
!               dfdrho_mat(i,j) = dfdx_num
!               dfdt_mat(i,j) = dfdy_num
!               d2fdrho2_mat(i,j) = d2fdx2_num
!               d2fdt2_mat(i,j) = d2fdy2_num
!               d2fdrhodt_mat(i,j) = d2fdxdy_num

	       !! For plotting purposes
           d2fdrho2_nmat(i,j) = d2fdx2_num
           d2fdt2_nmat(i,j) = d2fdy2_num
           d2fdrhodt_nmat(i,j) = d2fdxdy_num
           
           d3fdrhodt2_mat(i,j) = d3fdxdy2_num
           d3fdrho2dt_mat(i,j) = d3fdx2dy_num
           d4fdrho2dt2_mat(i,j) = d4fdx2dy2_num
           
           d2fdrho2_mat(i,j) = d2fdx2_num
           d2fdrhodt_mat(i,j) = d2fdxdy_num
           d2fdt2_mat(i,j) = d2fdy2_num
           
           logPmat(i,j) = log10(ppi)
           logEmat(i,j) = log10(uui)
           logSmat(i,j) = log10(ssi / evtemp) 
               
           Pmat(i,j) = ppi
           Emat(i,j) = uui
           Smat(i,j) = ssi / evtemp
           
           if(isnan(logPmat(i,j))) then
              logPmat(i,j) = -100
              write(*,*) rrhoi, ttti
           endif
           if(isnan(logSmat(i,j))) then 
              logSmat(i,j) = 10
           endif
           if(isnan(logEmat(i,j))) then
              logEmat(i,j) = -100
           endif
               
           !! Double check the units
           free_energy(i,j) = uui - tti * ssi
               
               !! All of these derivatives are in (g/cc, eV) space.  For (dn/dyn) we divide by evtemp**n
           write(outfid2, *) lnrhovect(i), lntvect(j), free_energy(i,j), dfdrho_mat(i,j), dfdt_mat(i,j) / evtemp, &
                d2fdrho2_mat(i,j), d2fdt2_mat(i,j) / evtemp**2, d2fdrhodt_mat(i,j) / evtemp, &
                d3fdrho2dt_mat(i,j) / evtemp, d3fdrhodt2_mat(i,j) / evtemp**2., d4fdrho2dt2_mat(i,j) / evtemp**2.
               
        enddo
     enddo
     
     write(outfid2,*) 
     
     do i=1,imax
        do j=1,jmax
!               write(outfid, *) logrhovect(i), logtvect(j), &
!                    logPcon(i,j), logEcon(i,j), logScon(i,j)

           write(outfid, *) lnrhovect(i), lntvect(j), &
                logPmat(i,j), logEmat(i,j), logSmat(i,j)
           !               write(outfid2, *) lnrhovect(i), lntvect(j), free_energy(i,j)
        enddo
     enddo
     
     close(outfid)
     close(outfid2)
     close(kinp)
     close(klst) 

         

     if(imat .eq. 1) then 
            !! output some diagnostic files that we can plot
        prefix = "plot_data/"
        write(*,*) "putting plots in " // prefix	    

        filename = trim(prefix) // "logRhovect.data"
        write(*,*) filename
        open(unit=19,file=trim(filename),status='replace', &
             iostat=ios,action='write',form='formatted')
        write(19,'(e14.6)') logRhovect
        close(unit=19)
        
        filename = trim(prefix) // "logTvect.data"
        open(unit=19,file=trim(filename),status='replace', &
             iostat=ios,action='write',form='formatted')
        write(19,'(e14.6)') logtvect
        close(unit=19)
        
        filename = trim(prefix) // "fr.data"
        open(unit=19,file=trim(filename),status='replace', &
             iostat=ios,action='write',form='formatted')
        
        free_energy2 = free_energy - minval(free_energy) + 1.
        write(*,*) minval(free_energy)
        !            call ColorMat(free_energy2, output_mat)
!            write(19,'(e14.6)') output_mat
!            write(19,'(e14.6)') free_energy2 / 1e16
        write(19,'(e14.6)') log10(free_energy2)		
        close(unit=19)
        
        filename = trim(prefix) // "dfdrho.data"
        call write_sgnlogabsmatrix(dfdrho_mat, filename)
        
        filename = trim(prefix) // "dfdt.data"
        call write_sgnlogabsmatrix(dfdt_mat, filename)
        
        filename = trim(prefix) // "d2fdrho2.data"
        call write_sgnlogabsmatrix(d2fdrho2_mat, filename)
        
        filename = trim(prefix) // "d2fdrho2_num.data"
        write(*,*) filename
        call write_sgnlogabsmatrix(d2fdrho2_nmat, filename)
!        call write_sgnlogabsmatrix(d2fdrho2_nmat, filename)
        
        do i=1,imax
           do j=1,jmax
              d2fdrho2_diff(i,j) = log10(abs(d2fdrho2_con(i,j) / d2fdrho2_nmat(i,j)))
              if(isnan(d2fdrho2_diff(i,j))) then
                 d2fdrho2_diff(i,j) = -100
              endif
              d2fdt2_diff(i,j) = log10(abs(d2fdt2_con(i,j) / d2fdt2_nmat(i,j)))
              if(isnan(d2fdt2_diff(i,j))) then
                 d2fdt2_diff(i,j) = -100
              endif
           enddo
        enddo
        
        filename = trim(prefix) // "d2fdrho2_diff.data"
        open(unit=19,file=trim(filename),status='replace', &
             iostat=ios,action='write',form='formatted')
        write(19,'(e14.6)') d2fdrho2_diff
        close(unit=19)

        filename = trim(prefix) // "d2fdt2.data"
        call write_sgnlogabsmatrix(d2fdt2_mat, filename)
        
        filename = trim(prefix) // "d2fdt2_num.data"
        call write_sgnlogabsmatrix(d2fdt2_nmat, filename)

        filename = trim(prefix) // "d2fdt2_diff.data"
        open(unit=19,file=trim(filename),status='replace', &
             iostat=ios,action='write',form='formatted')
        write(19,'(e14.6)') d2fdt2_diff
        close(unit=19)
        
        filename = trim(prefix) //"d2fdrhodt.data"
        call write_sgnlogabsmatrix(d2fdrhodt_mat, filename)
        
        filename = trim(prefix) // "d3fdrhodt2.data"
        call write_sgnlogabsmatrix(d3fdrhodt2_mat, filename)
        
        filename = trim(prefix) // "d3fdrho2dt.data"
        call write_sgnlogabsmatrix(d3fdrho2dt_mat, filename)
        
        filename = trim(prefix) // "d4fdrho2dt2.data"
        call write_sgnlogabsmatrix(d4fdrho2dt2_mat, filename)
        
        
        !! This is boilerplate and needs to be improved
        i = 65
        write(*,*) i, logrhovect(i)
        filename = "line65.data"
        call plot_Yvect(logtvect, free_energy, i, filename)
            
        filename = "line80.data"
        i = 80
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, free_energy, i, filename)
        
        filename = "line90.data"
        i = 90
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, free_energy, i, filename)
        
        filename = "line95.data"
        i = 95
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, free_energy, i, filename)
        
        filename = "line100.data"
        i = 100
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, free_energy, i, filename)
        
        filename = "line105.data"
        i = 105
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, free_energy, i, filename)
        
        filename = "line110.data"
        i = 110
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, free_energy, i, filename)
        
        
        !! logP(T) | Rho_i
        write(*,*) "Rho index, logRho"
        filename = "logP_rho20.data"
        i = 20
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, logPmat, i, filename)
        
        filename = "logP_rho50.data"
        i = 50
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, logPmat, i, filename)
        
        filename = "logP_rho80.data"
        i = 80
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, logPmat, i, filename)
        
        filename = "logP_rho90.data"
        i = 90
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, logPmat, i, filename)
            
        filename = "logP_rho95.data"
        i = 95
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, logPmat, i, filename)
        
        filename = "logP_rho100.data"
        i = 100
        write(*,*) i, logrhovect(i)
        call plot_Yvect(logtvect, logPmat, i, filename)
        
        !! logS(logT) | Rho_i
        filename = "logS_rho20.data"
        i = 20
        call plot_Yvect(logtvect, logSmat, i, filename)
        
        filename = "logS_rho50.data"
        i = 50
        call plot_Yvect(logtvect, logSmat, i, filename)
        
        filename = "logS_rho80.data"
        i = 80
        call plot_Yvect(logtvect, logSmat, i, filename)
        
        filename = "logS_rho90.data"
        i = 90
        call plot_Yvect(logtvect, logSmat, i, filename)
        
        filename = "logS_rho95.data"
        i = 95
        call plot_Yvect(logtvect, logSmat, i, filename)
        
        filename = "logS_rho100.data"
        i = 100
        call plot_Yvect(logtvect, logSmat, i, filename)
        
        
        write(*,*) "T index, logT"
        filename = "logP_t20.data"
        j= 20
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "logP_t40.data"
        j= 40
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "logP_t60.data"
        j= 60
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "logP_t80.data"
        j= 80
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "logP_t90.data"
        j= 90
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)

        filename = "logP_t100.data"
        j= 100
        write(*,*)  j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        
        write(*,*) "T index, logT"
        filename = "v.logP_t20.data"
        j= 20
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "v.logP_t40.data"
        j= 40
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "v.logP_t60.data"
        j= 60
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "v.logP_t80.data"
        j= 80
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "v.logP_t90.data"
        j= 90
        write(*,*) j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        filename = "v.logP_t100.data"
        j= 100
        write(*,*)  j, logTvect(j)
        call plot_Xvect(logrhovect, logPmat, j, filename)
        
        
        !! logS(logRho) | T_j
        filename = "logS_t20.data"
        j= 20
        call plot_Xvect(logrhovect, logSmat, j, filename)
        
        filename = "logS_t40.data"
        j= 40
        call plot_Xvect(logrhovect, logSmat, j, filename)
        
        filename = "logS_t60.data"
        j= 60
        call plot_Xvect(logrhovect, logSmat, j, filename)

        filename = "logS_t80.data"
        j= 80
        call plot_Xvect(logrhovect, logSmat, j, filename)
        
        filename = "logS_t100.data"
        j= 100
        call plot_Xvect(logrhovect, logSmat, j, filename)
     endif
         
  enddo
  
contains
  subroutine write_matrix(matrix, filename)
    double precision, intent(in), dimension(:,:) :: matrix
    character (len=256) :: filename
    integer :: unit, ios
    open(unit=19, file=trim(filename), status='replace', &
         iostat=ios, action='write', form='formatted')
    write(19,'(e14.6)') matrix
    close(unit=19)
  end subroutine write_matrix

  !! Sign * (log10(abs(matrix)))
  subroutine write_sgnlogabsmatrix(matrix, filename)
    double precision, intent(in), dimension(:,:) :: matrix
    character (len=256) :: filename

    open(unit=19, file=trim(filename), status='replace', &
         iostat=ios, action='write', form='formatted')
    call ColorMat(matrix, output_mat)
    write(19,'(e14.6)') output_mat
    close(unit=19)

  end subroutine write_sgnlogabsmatrix
  
  subroutine plot_Yvect(Yvect, matrix, i, filename)
    double precision, intent(in), dimension(:) :: Yvect
    double precision, intent(in), dimension(:,:) :: matrix
    integer, intent(in) :: i
    character (len=256) :: filename
    open(unit=19, file=trim(filename), status='replace', &
         iostat=ios,action='write',form='formatted')
    do j=1,jmax
       write(19,*) Yvect(j), matrix(i,j)
    enddo
    close(unit=19)
  end subroutine plot_Yvect
  
  subroutine plot_Xvect(Xvect, matrix, j, filename)
    double precision, intent(in), dimension(:) :: Xvect
    double precision, intent(in), dimension(:,:) :: matrix
    integer, intent(in) :: j
    character (len=256) :: filename
    open(unit=19, file=trim(filename), status='replace', &
         iostat=ios,action='write',form='formatted')
    do i=1,imax
       write(19,*) Xvect(i), matrix(i,j)
    enddo
    close(unit=19)
  end subroutine plot_Xvect
  
  
  subroutine donothing(temp)
    double precision, intent(inout) :: temp
  end subroutine donothing
  
  double precision function sgnlogabs(val)
    double precision,intent(in) :: val
    
    if(abs(val) .lt. 1d0) then
       sgnlogabs = 0d0
    else
       if(val .gt. 0) then
          sgnlogabs = log10(val) 
       else
          sgnlogabs = -log10(-val)
       end if
    endif
  end function sgnlogabs
  
  double precision function invsgnlogabs(val)
    double precision, intent(in) :: val
    
    if(abs(val) .lt. 1d0) then
       invsgnlogabs = 0.001d0
    else
       if(val .gt. 0) then
          invsgnlogabs = 10d0**val
       else
          invsgnlogabs = -10d0**(-val)
       end if
    end if
  end function invsgnlogabs
  
  subroutine ColorMat(matrix_in, matrix_out)
    double precision, intent(in), dimension(:,:) :: matrix_in
    double precision, intent(out), dimension(:,:) :: matrix_out
    integer :: i, j
    
    do i=1,imax
       do j=1,jmax
          matrix_out(i,j) = sgnlogabs(matrix_in(i,j))
       enddo
    enddo
  end subroutine ColorMat
  
  subroutine invColorMat(matrix_in, matrix_out)
    double precision, intent(in), dimension(:,:) :: matrix_in
    double precision, intent(out), dimension(:,:) :: matrix_out
    integer :: i, j
    
    do i=1,imax
       do j=1,jmax
          
          matrix_out(i,j) = invsgnlogabs(matrix_in(i,j))
       enddo
    enddo
  end subroutine invColorMat
end program simple

!      include 'aneosequ.f'
