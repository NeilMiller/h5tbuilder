real(8) function y1(x,nd)

  implicit none
  
  real(8) :: x
  integer :: nd
  
  select case(nd)
  case(0)
    y1 = 2.d0*x**3 - x
  case(1)
    y1 = 6.d0*x**2 - 1.d0
  case(2) 
    y1 = 12.d0*x
  case default
    y1 = 0.d0
  end select
  
  return
  
end function  

real(8) function y2(x,nd1,nd2)

  implicit none
  
  real(8) :: x(2)
  integer :: nd1,nd2
  
  select case(nd1)
  case(0)
    select case(nd2)
    case(0)
      y2 = x(1)**2*x(2)**3 + x(1) + 2.d0
    case(1)
      y2 = 3.d0*x(1)**2*x(2)**2
    case(2) 
      y2 = 6.d0*x(1)**2*x(2)
    case default
      y2 = 0.d0
    end select
  case(1)
    select case(nd2)
    case(0)
      y2 = 2.d0*x(1)*x(2)**3 + 1.d0
    case(1)
      y2 = 6.d0*x(1)*x(2)**2
    case(2)
      y2 = 12.d0*x(1)*x(2)
    case default
      y2 = 0.d0
    end select
  case(2) 
    select case(nd2)
    case(0)
      y2 = 2.d0*x(2)**3
    case(1)
      y2 = 6.d0*x(2)**2
    case(2) 
      y2 = 12.d0*x(2)
    case default
      y2 = 0.d0
    end select
  case default
    y2 = 0.d0
  end select
  
  return
  
end function  

real(8) function y3(x,nd1,nd2,nd3)

  implicit none
  
  real(8) :: x(3)
  integer :: nd1,nd2,nd3
  
  select case(nd1)
  case(0)
    select case(nd2)
    case(0)
      y3 = x(1)**2*x(2)**3 + x(1) + 2.d0
    case(1)
      y3 = 3.d0*x(1)**2*x(2)**2
    case(2) 
      y3 = 6.d0*x(1)**2*x(2)
    case default
      y3 = 0.d0
    end select
  case(1)
    select case(nd2)
    case(0)
      y3 = 2.d0*x(1)*x(2)**3 + 1.d0
    case(1)
      y3 = 6.d0*x(1)*x(2)**2
    case(2)
      y3 = 12.d0*x(1)*x(2)
    case default
      y3 = 0.d0
    end select
  case(2) 
    select case(nd2)
    case(0)
      y3 = 2.d0*x(2)**3
    case(1)
      y3 = 6.d0*x(2)**2
    case(2) 
      y3 = 12.d0*x(2)
    case default
      y3 = 0.d0
    end select
  case default
    y3 = 0.d0
  end select
  
  select case (nd3)
  case(0)
    y3 = y3*x(3)**2.
  case(1)
    y3 = 2.d0*y3*x(3)
  case(2)
    y3 = 2.d0*y3
  case default
    y3 = 0.d0
  end select
   
  
  return
  
end function

real(8) function y4(x,nd1,nd2,nd3,nd4)

  implicit none

  real(8) :: x(4)
  integer :: nd1,nd2,nd3,nd4
  real(8), external :: y3
  

  y4 = y3(x(1:3),nd1,nd2,nd3)

  select case(nd4)
    case(0); y4 = y4*x(4)**2
    case(1); y4 = 2.d0*y4*x(4)
    case(2); y4 = 2.d0*y4
    case default; y4 = 0.d0
  end select

  return

end function

program test

  use flat_interpolator_mod
  
  implicit none
  
  real(8), external :: y1,y2,y3,y4
  
  real(8) :: F1(3,2)
  real(8) :: F2(0:8,0:3)
  real(8) :: F3(2,2,2,3,3,3),    y3out(3,3,3)
  real(8) :: F3f(0:26,0:7)
  real(8) :: F4(0:80,0:15) 
  real(8) :: val(4),xx(4),del(4),deriv(4)
  
  real(8),target :: xlo(4),xhi(4),error(4),yy,f
  real(8) :: xgr(4,2)
  integer :: i1,i2,i3,i4,j1,j2,j3,j4,k1,k2,k3,k4,i,j,k,l,m,n,ierr
  integer :: idx1,idx2 
  integer :: np = 50
  
  type(flat_interpolator) :: interp
  
  
  ! Do the 1-D Test
  xlo(1) = 2.d0
  xhi(1) = 4.d0
  del(1) = xhi(1)-xlo(1)
  
  do j=1,3
    F1(j,1) = y1(xlo(1),j-1)
    F1(j,2) = y1(xhi(1),j-1)
  enddo
  
  error  = 0.
  do j1=1,np
    val(1) = xlo(1) + (xhi(1)-xlo(1))*REAL(j1-1)/(np-1.d0)
    xx(1)  = (val(1)-xlo(1))/del(1)
    do i1=1,3
      ierr = interp%set_order(iorder=i1)
      do k1=0,i1-1
        f = interp%f1d(xx,del,F1,(/k1/),3)
        error(i1) = error(i1) + ABS(f - y1(val(1),k1))
      enddo  
    enddo    
  enddo
  
  error(1)  = error(1)/(np)
  if (error(1)<1.e-10) then
    write(6,'(a,es12.3)')'1D Linear Interpolation test passed. Error: ',error(1)
  else  
    write(6,'(a,es12.3)')'1D Linear Interpolation test failed. Error: ',error(1)
  endif 
  
  error(2) = error(2)/(2.d0*np)
  if (error(2)<1.e-10) then
    write(6,'(a,es12.3)')'1D Cubic Interpolation test passed.   Error: ',error(2)
  else  
    write(6,'(a,es12.3)')'1D Cubic Interpolation test failed.   Error: ',error(2)
  endif
  
  error(3)  = error(3)/(3.d0*np)
  if (error(3)<1.e-10) then
    write(6,'(a,es12.3)')'1D Quintic Interpolation test passed. Error: ',error(3)
  else  
    write(6,'(a,es12.3)')'1D Quintic Interpolation test failed. Error: ',error(3)
  endif 
  
  ! Do the 2-D Test
  xlo(1) = 2.d0
  xhi(1) = 4.d0
  xlo(2) = 1.d0
  xhi(2) = 4.d0
  del(1:2) = xhi(1:2)-xlo(1:2)
  
  ! Do the 2-D Test
  xlo(1) = 2.d0
  xhi(1) = 4.d0
  xlo(2) = 1.d0
  xhi(2) = 4.d0
  del(1:2) = xhi(1:2)-xlo(1:2)
  xgr(1,1) = 2.d0
  xgr(1,2) = 4.d0
  xgr(2,1) = 1.d0
  xgr(2,2) = 4.d0
  del(1:2) = xgr(1:2,2) - xgr(1:2,1)
  
  do k=0,2
  do l=0,2
    do i=0,1
    do j=0,1
!    F2(1,1,k,l) = y2((/xlo(1),xlo(2)/),k-1,l-1)
!    F2(1,2,k,l) = y2((/xlo(1),xhi(2)/),k-1,l-1)
!    F2(2,1,k,l) = y2((/xhi(1),xlo(2)/),k-1,l-1)
!    F2(2,2,k,l) = y2((/xhi(1),xhi(2)/),k-1,l-1) 
      idx2 = 2*j + i 
      idx1 = 3*l + k 
      F2(idx1,idx2) = y2((/xgr(1,i+1),xgr(2,j+1)/),k,l) 
    enddo
    enddo
  enddo
  enddo
  
  error  = 0.
  do j1=1,np
  do j2=1,np
    val(1) = xlo(1) + (xhi(1)-xlo(1))*(j1-1)/(np-1.d0)
    val(2) = xlo(2) + (xhi(2)-xlo(2))*(j2-1)/(np-1.d0)
    
    xx(1) = (val(1)-xlo(1))/del(1)
    xx(2) = (val(2)-xlo(2))/del(2)
    
    do i1=1,3
      ierr = interp%set_order(iorder=i1)
      do k1=0,i1-1
      do k2=0,i1-1
        f = interp%f2d(xx,del,F2,(/k1,k2/),3)
        error(i1) = error(i1) + ABS(f - y2((/val(1),val(2)/),k1,k2))
      enddo  
      enddo
    enddo 
  enddo
  enddo
  
  error(1) = error(1)/(np*np)
  if (error(2)<1.e-10) then
    write(6,'(a,es12.3)')'2D Linear Interpolation test passed.   Error: ',error(1)
  else  
    write(6,'(a,es12.3)')'2D Linear Interpolation test failed.   Error: ',error(1)
  endif  
  
  error(2) = error(2)/(4.d0*np*np)
  if (error(2)<1.e-10) then
    write(6,'(a,es12.3)')'2D Cubic Interpolation test passed.   Error: ',error(2)
  else  
    write(6,'(a,es12.3)')'2D Cubic Interpolation test failed.   Error: ',error(2)
  endif  
  
  error(3) = error(3)/(9.d0*np*np)
  if (error(3)<1.e-10) then
    write(6,'(a,es12.3)')'2D Quintic Interpolation test passed. Error: ',error(3)
  else  
    write(6,'(a,es12.3)')'2D Quintic Interpolation test failed. Error: ',error(3)
  endif  
  
  ! Do the 3-D Test
  xlo(1) = 2.d0
  xhi(1) = 4.d0
  xlo(2) = 1.d0
  xhi(2) = 4.d0
  xlo(3) = 1.d0
  xhi(3) = 2.d0
  del(1:3) = xhi(1:3)-xlo(1:3)
  
  do k=1,3
  do l=1,3
  do m=1,3
    F3(1,1,1,k,l,m) = y3((/xlo(1),xlo(2),xlo(3)/),k-1,l-1,m-1)
    F3(2,1,1,k,l,m) = y3((/xhi(1),xlo(2),xlo(3)/),k-1,l-1,m-1)
    F3(1,2,1,k,l,m) = y3((/xlo(1),xhi(2),xlo(3)/),k-1,l-1,m-1)
    F3(1,1,2,k,l,m) = y3((/xlo(1),xlo(2),xhi(3)/),k-1,l-1,m-1)
    F3(2,2,1,k,l,m) = y3((/xhi(1),xhi(2),xlo(3)/),k-1,l-1,m-1)
    F3(2,1,2,k,l,m) = y3((/xhi(1),xlo(2),xhi(3)/),k-1,l-1,m-1)
    F3(1,2,2,k,l,m) = y3((/xlo(1),xhi(2),xhi(3)/),k-1,l-1,m-1)
    F3(2,2,2,k,l,m) = y3((/xhi(1),xhi(2),xhi(3)/),k-1,l-1,m-1)
  enddo
  enddo
  enddo
 
  do k =0,2
  do l =0,2
  do m =0,2
  do j1=0,1
  do j2=0,1
  do j3=0,1
    idx1 = 9*m  + 3*l  + k
    idx2 = 4*j3 + 2*j2 + j1
    F3f(idx1,idx2) = F3(j1+1,j2+1,j3+1,k+1,l+1,m+1)
  enddo
  enddo
  enddo
  enddo
  enddo
  enddo
         
  error  = 0.
  do j1=1,np
  do j2=1,np
  do j3=1,np
    val(1) = xlo(1) + (xhi(1)-xlo(1))*(j1-1)/(np-1.d0)
    val(2) = xlo(2) + (xhi(2)-xlo(2))*(j2-1)/(np-1.d0)
    val(3) = xlo(3) + (xhi(3)-xlo(3))*(j3-1)/(np-1.d0)
    
    xx(1) = (val(1)-xlo(1))/del(1)
    xx(2) = (val(2)-xlo(2))/del(2)
    xx(3) = (val(3)-xlo(3))/del(3)
    
    do i1=2,3
      ierr = interp%set_order(iorder=i1)
      do k1=0,i1-1
      do k2=0,i1-1
      do k3=0,i1-1
        f = interp%f3d(xx,del,F3f,(/k1,k2,k3/),3)
        error(i1) = error(i1) + ABS(f - y3(val(1:3),k1,k2,k3))
      enddo  
      enddo
      enddo
    enddo 
  enddo
  enddo
  enddo
  
  error(2) = error(2)/(4.d0*np*np)
  if (error(2)<1.e-10) then
    write(6,'(a,es12.3)')'3D Cubic Interpolation test passed.   Error: ',error(2)
  else  
    write(6,'(a,es12.3)')'3D Cubic Interpolation test failed.   Error: ',error(2)
  endif  
  
  error(3) = error(3)/(9.d0*np*np)
  if (error(3)<1.e-10) then
    write(6,'(a,es12.3)')'3D Quintic Interpolation test passed. Error: ',error(3)
  else  
    write(6,'(a,es12.3)')'3D Quintic Interpolation test failed. Error: ',error(3)
  endif  

  xgr(1,1) = 2.d0
  xgr(1,2) = 4.d0
  xgr(2,1) = 1.d0
  xgr(2,2) = 4.d0
  xgr(3,1) = 2.d0
  xgr(3,2) = 4.d0
  xgr(4,1) = 1.d0
  xgr(4,2) = 3.d0
  del(1:4) = xgr(1:4,2) - xgr(1:4,1)

  do i4=0,1
  do i3=0,1
  do i2=0,1
  do i1=0,1 
    idx2 = 8*i4 + 4*i3 + 2*i2 + i1
  do j4=0,2
  do j3=0,2
  do j2=0,2
  do j1=0,2 
    idx1 = 27*j4 + 9*j3 + 3*j2 + j1
    F4(idx1,idx2) = y4((/xgr(1,i1+1),xgr(2,i2+1),xgr(3,i3+1),xgr(4,i4+1)/),j1,j2,j3,j4)
  enddo
  enddo
  enddo
  enddo
  enddo   
  enddo
  enddo
  enddo   
  
  error  = 0.
  do j1=1,np
  do j2=1,np
  do j3=1,np
  do j4=1,np
    val(1) = xgr(1,1) + (xgr(1,2)-xgr(1,1))*(j1-1)/(np-1.d0)
    val(2) = xgr(2,1) + (xgr(2,2)-xgr(2,1))*(j2-1)/(np-1.d0)
    val(3) = xgr(3,1) + (xgr(3,2)-xgr(3,1))*(j3-1)/(np-1.d0)
    val(4) = xgr(4,1) + (xgr(4,2)-xgr(4,1))*(j4-1)/(np-1.d0)
    
    xx(1) = (val(1)-xgr(1,1))/del(1)
    xx(2) = (val(2)-xgr(2,1))/del(2)
    xx(3) = (val(3)-xgr(3,1))/del(3)
    xx(4) = (val(4)-xgr(4,1))/del(4)
    
    do i1=2,3
      ierr = interp%set_order(iorder=i1)
      do k1=0,i1-1
      do k2=0,i1-1
      do k3=0,i1-1
      do k4=0,i1-1
        f = interp%f4d(xx,del,F4,(/k1,k2,k3,k4/),3)
        error(i1) = error(i1) + ABS(f - y4(val(1:4),k1,k2,k3,k4))
      enddo  
      enddo
      enddo
      enddo
    enddo 
  enddo
  enddo
  enddo
  enddo
  
  error(2) = error(2)/(8.d0*np*np*np)
  if (error(2)<1.e-10) then
    write(6,'(a,es12.3)')'4D Cubic Interpolation test passed.   Error: ',error(2)
  else  
    write(6,'(a,es12.3)')'4D Cubic Interpolation test failed.   Error: ',error(2)
  endif  
  
  error(3) = error(3)/(27.d0*np*np*np)
  if (error(3)<1.e-10) then
    write(6,'(a,es12.3)')'4D Quintic Interpolation test passed. Error: ',error(3)
  else  
    write(6,'(a,es12.3)')'4D Quintic Interpolation test failed. Error: ',error(3)
  endif  

end program
