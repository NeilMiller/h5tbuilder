# plot_images.rb

load "/Users/neil/research/mesa/utils/image_plot.rb"
    
class MY_data < Image_data

  attr_accessor :logPan
  attr_accessor :logEan
  attr_accessor :logSan

  attr_accessor :logP
  attr_accessor :logE
  attr_accessor :logS

  attr_accessor :logPdiff
  attr_accessor :logEdiff
  attr_accessor :logSdiff
  
  def initialize(data_dir)
        
      read_image_Xs(data_dir, 'h5t_logRhovect.data', 'log10(\rho)')
      read_image_Ys(data_dir, 'h5t_logTvect.data', 'log10(T)')
      
      @logPan = read_image_data(data_dir, 'an_logP')
      @logEan = read_image_data(data_dir, 'an_logE')
      @logSan = read_image_data(data_dir, 'an_logS')
    
      @logP = read_image_data(data_dir, 'h5t_logP')
      @logE = read_image_data(data_dir, 'h5t_logE')
      @logS = read_image_data(data_dir, 'h5t_logS')
    
      @logPdiff = read_image_data(data_dir, 'h5t_logPdiff')
      @logEdiff = read_image_data(data_dir, 'h5t_logEdiff')
      @logSdiff = read_image_data(data_dir, 'h5t_logSdiff')
      
    end
  
end # class MY_data


class MY_plot

  include Math
  include Tioga
  include FigureConstants
  include Image_plot
  
  def initialize(data_dir)
    
    @data_dir = data_dir
    @figure_maker = FigureMaker.default
    t.def_eval_function { |str| eval(str) }
    t.save_dir = 'aneos_water'
    
    

    t.def_figure('logPan') { logPan }
    t.def_figure('logEan') { logEan }
    t.def_figure('logSan') { logSan }
    
    
    t.def_figure('logP') { logP }
    t.def_figure('logE') { logE }
    t.def_figure('logS') { logS }
    
    t.def_figure('logPdiff') { logPdiff }
    t.def_figure('logEdiff') { logEdiff }
    t.def_figure('logSdiff') { logSdiff }
    

    @image_data = MY_data.new(data_dir)
    @label_scale = 0.75
    @no_clipping = false #true        
    
      ### Load the neptune profile into the class
    @margin = 0.05
    
    @LowMassProfile1 = Dvector.read("planet_data/RhoTset01.dat")
    @LowMasslog10RhoProfile1 = @LowMassProfile1[0]
    @LowMasslog10TProfile1 = @LowMassProfile1[1]
    
    @LowMassProfile2 = Dvector.read("planet_data/RhoTset02.dat")
    @LowMasslog10RhoProfile2 = @LowMassProfile2[0]
    @LowMasslog10TProfile2 = @LowMassProfile2[1]
    
    @LowMassProfile3 = Dvector.read("planet_data/RhoTset03.dat")
    @LowMasslog10RhoProfile3 = @LowMassProfile3[0]
    @LowMasslog10TProfile3 = @LowMassProfile3[1]
    
    @HD80606Profile = Dvector.read("planet_data/RhoTset04.dat")
    @HD80606log10RhoProfile = @HD80606Profile[0]
    @HD80606log10TProfile = @HD80606Profile[1]
    
#      @NepPressureProfile = @NepProfile[2]
#      @Neplog10Rho = @NepRhoProfile.log10
#      @Neplog10T = @NepTProfile.log10
#      @NeplnP = @NepPressureProfile.log
      
    t.def_enter_page_function { enter_page } 
    
  end
  
  def enter_page
    t.yaxis_numeric_label_angle = -90
    t.page_setup(11*72/2,8.5*72/2)
    t.set_frame_sides(0.15,0.85,0.85,0.15) # left, right, top, bottom in page coords  
  end
    
  def clip_image
    t.fill_color = Black
    t.fill_frame
    return
  end
    
  def t
    @figure_maker
  end
  
  def d
    @image_data
  end
  
  def do_decorations(title)
  end
    
  # plot routines
  
  def logPan
    image_plot('d' => d, 'zs' => d.logPan, 'title' => 'logPan ', 'interpolate' => false)
#               'z_lower' => 4, 'z_upper' => 18, 'interpolate' => false)
  end

  def logEan
    image_plot('d' => d, 'zs' => d.logEan, 'title' => 'logEan ', 'interpolate' => false,
               'z_lower' => 8.5, 'z_upper' => 18, 'interpolate' => false)
  end

  def logSan
    image_plot('d' => d, 'zs' => d.logSan, 'title' => 'logSan ', 'interpolate' => false,
               'z_lower' => 6.5, 'z_upper' => 9, 'interpolate' => false)
  end  
  
  
  def logP
    image_plot('d' => d, 'zs' => d.logP, 'title' => 'logP ', 'interpolate' => false)
#               'z_lower' => 4, 'z_upper' => 18, 'interpolate' => false)
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do 
      @bounds = [-5,2,5.5,2.5]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        t.line_width=1.0
        t.show_polyline(@LowMasslog10RhoProfile1, @LowMasslog10TProfile1, Blue)
        t.show_polyline(@LowMasslog10RhoProfile2, @LowMasslog10TProfile2, Red)
        t.show_polyline(@LowMasslog10RhoProfile3, @LowMasslog10TProfile3, Green)
        t.show_polyline(@HD80606log10RhoProfile, @HD80606log10TProfile, Black)
      }
    end
  end
  
  def logE
    image_plot('d' => d, 'zs' => d.logE, 'title' => 'logE ', 'interpolate' => false,
               'z_lower' => 8.5, 'z_upper' => 18, 'interpolate' => false)
  end
  
  def logS
    image_plot('d' => d, 'zs' => d.logS, 'title' => 'logS ', 'interpolate' => false,
               'z_lower' => 6.5, 'z_upper' => 9, 'interpolate' => false)
  end  
  
  def logPdiff
    image_plot('d' => d, 'zs' => d.logPdiff, 'title' => 'Delta logP ', 'interpolate' => false,
               'z_lower' => -0.2, 'z_upper' => 0.2)
    
  end
  
  def logEdiff
    image_plot('d' => d, 'zs' => d.logEdiff, 'title' => 'Delta logE ',  'interpolate' => false,
               'z_lower' => -0.2, 'z_upper' => 0.2)
    
  end
  
  def logSdiff
    image_plot('d' => d, 'zs' => d.logSdiff, 'title' => 'Delta logS ', 'interpolate' => false,
               'z_lower' => -0.2, 'z_upper' => 0.2)
    
  end  
  
  def plot_boundaries(xs,ys,margin,xmin=nil,xmax=nil,ymin=nil,ymax=nil)
    xmin = xs.min if xmin == nil
    xmax = xs.max if xmax == nil
    ymin = ys.min if ymin == nil
    ymax = ys.max if ymax == nil
    
    width = (xmax == xmin) ? 1 : xmax - xmin
    height = (ymax == ymin) ? 1 : ymax - ymin
    
    left_boundary = xmin - margin * width
    right_boundary = xmax + margin * width
    
    top_boundary = ymax + margin * height
    bottom_boundary = ymin - margin * height
    
    return [ left_boundary, right_boundary, top_boundary, bottom_boundary ]
  end
  
  def pressure
    image_plot('d' => d, 'zs' => d.pressure, 'title' => 'log10(Pressure)',
               'z_lower' => 0, 'z_upper' => 14, 'interpolate' => false)
    # We would like to overplot some profile here.
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do
      @bounds = [-4,2.5,2,10]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        #        clip_press_image
        t.line_width=2.5
        t.show_polyline(@LowMasslog10RhoProfile, @LowMasslog10TProfile, Blue)
        #      t.show_marker('Xs' => @Neplog10Rho, 'Ys' => @Neplog10T,
#                    'marker' => Asterisk,
#                    'scale' => 1.2, 
#                    'color' => Blue)
        print @image_right_margin
      }
    end 
  end  
end


MY_plot.new('h5tdata')
