!! h5table_register.  Module for storing a list of tables
!! By making this into a seperate module, this has become a private type
module h5table_register
  use h5table_params
  use h5table_type
  implicit none
  !! These should not ever be accessed by routines other than those below
  !! These handles are setup for the purpose of mapping integers <-> pointers
  type h5_entry
     type (h5tbl), pointer :: tbl => null()
     integer :: tbl_handle
  end type h5_entry

  integer, private, parameter :: H5REGSIZE = 64
  type (h5_entry), private :: h5list(H5REGSIZE)

  private h5_entry
  public get_tbl, set_handle, free_handle

contains
  !! subroutines to deal with the handles
  !!  The purpose of these handles is such that when we perform a rootfind, we have
  !!  an integer that is associated with the table.
  !!  Keep in mind that potentially two processes may be calling these routines
  !!  at the same time so output should be safely written out to memory where only
  !!  one process could access.
  !!  ----------------------------------------
  ! This routine gets the table from a handle
  subroutine get_tbl(tbl_handle, tbl, ierr)
    implicit none
    integer, intent(in) :: tbl_handle
    type (h5tbl), pointer, intent(inout) :: tbl
    integer, intent(out) :: ierr
    
    if((tbl_handle .le. H5REGSIZE).and.(tbl_handle .gt.0)) then 
       if(h5list(tbl_handle)%tbl_handle .eq. tbl_handle) then
          ierr = 0
          tbl => h5list(tbl_handle)%tbl
       else
          ierr = H5TBLERR_BADIND
       endif
    else
       ierr = H5TBLERR_OUTBOUND
    endif
  end subroutine get_tbl
  
  !! Associate an integer with the table pointer
  subroutine set_handle(tbl, tbl_handle, ierr)
    implicit none
    type (h5tbl), intent(in), pointer :: tbl
    integer, intent(out) :: tbl_handle, ierr
    integer :: index
    
    ierr = 0
    index = 1
    do while (h5list(index)%tbl_handle .eq. index)
       index = index + 1
    end do
    
    if((index .le. H5REGSIZE).and.(h5list(index)%tbl_handle .ne. index)) then
       tbl_handle = index
       h5list(index)%tbl_handle = index
       h5list(index)%tbl => tbl
    else
       ierr = H5TBLERR_REGFULL      
    endif
  end subroutine set_handle
  
  subroutine free_handle(tbl_handle, ierr)
    implicit none
    integer, intent(in) :: tbl_handle
    integer, intent(out) :: ierr
    
    ierr = 0
    if(h5list(tbl_handle)%tbl_handle.eq.tbl_handle) then
       h5list(tbl_handle)%tbl_handle = 0
       nullify(h5list(tbl_handle)%tbl)
    else 
       ierr = H5TBLERR_BADASSOC
    endif
  end subroutine free_handle
  
end module h5table_register
