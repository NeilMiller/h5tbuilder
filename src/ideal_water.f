module ideal_water
  

  integer, parameter :: i_P = 1
  integer, parameter :: i_E = 2
  integer, parameter :: i_S = 3

  integer, parameter :: i_val = 1
  integer, parameter :: i_drho = 2
  integer, parameter :: i_dt = 3
  integer, parameter :: i_dRho2 = 4
  integer, parameter :: i_dt2 = 5
  integer, parameter :: i_dRhodt = 6

  type idealparams
     double precision :: mu = 18.
     double precision, pointer, dimension(:) :: Pvect, Svect, Evect
     double precision, pointer, dimension(:,:) :: Zi_dv
     double precision :: minX = 1e-5
     double precision :: maxX = 1e3
     double precision :: minY = 1e1
     double precision :: maxY = 1e8
  end type idealparams
  
  !! The register allows us to acces the equation of state tables
  !! The format for this must be uniform such that we can use these later
  type eos_register
     type (idealparams), pointer :: tbl
     integer :: tbl_handle
     ! logical :: locked  !! I don't think locking is necessary right now
  end type eos_register
  
  integer, parameter :: REGSIZE = 20
  type (eos_register) :: ideal_register(REGSIZE)
  
contains
  
  subroutine setup_tbl(tbl, ierr)
    type (idealparams), pointer :: tbl
    integer, intent(out) :: ierr

    ierr = 0
    allocate(tbl%Pvect(6), tbl%Svect(6), tbl%Evect(6))
  end subroutine setup_tbl

  subroutine free_tbl(tbl, ierr)
    type (idealparams), pointer :: tbl
    integer, intent(out) :: ierr
    
    ierr = 0
    deallocate(tbl%Pvect)
    deallocate(tbl%Svect)
    deallocate(tbl%Evect)
    
  end subroutine free_tbl

  !! This routine gets the table from a handle
  subroutine get_tbl(tbl_handle, tbl, ierr)
    integer, intent(in) :: tbl_handle
    type (idealparams), pointer, intent(inout) :: tbl
    integer, intent(out) :: ierr
    
    ierr = -1
    if((tbl_handle .le. REGSIZE).and.(tbl_handle .gt.0)) then 
       if(ideal_register(tbl_handle)%tbl_handle .eq. tbl_handle) then
          ierr = 0
          tbl => ideal_register(tbl_handle)%tbl
       endif
    endif
  end subroutine get_tbl
  
  !! Associate an integer with the table pointer
  subroutine set_handle(tbl, tbl_handle, ierr)
    type (idealparams), intent(in), pointer :: tbl
    integer, intent(out) :: tbl_handle, ierr
    integer :: index
    
    ierr = 0
    index = 1
    do while (ideal_register(index)%tbl_handle .eq. index)
       index = index + 1
    end do
    
    if((index .le. REGSIZE).and.(ideal_register(index)%tbl_handle .ne. index)) then
       tbl_handle = index
       ideal_register(index)%tbl_handle = index
       ideal_register(index)%tbl => tbl
    else
       ierr = -3         
    endif
  end subroutine set_handle
  
  !! The handle must not current be locked
  subroutine free_handle(tbl_handle, ierr)
    integer, intent(in) :: tbl_handle
    integer, intent(out) :: ierr
    
    ierr = 0
    if(ideal_register(tbl_handle)%tbl_handle.eq.tbl_handle) then
       ideal_register(tbl_handle)%tbl_handle = 0
       nullify(ideal_register(tbl_handle)%tbl)
    else 
       ierr = -6
    endif
  end subroutine free_handle

  !! The ideal gas law equation of state implemented here at time of writing does not
  !! include Free energy internal to molecules.  
  !! I found that it generally does not closely quantitatively match the values for
  !!  ANEOS , however they are similar in order of magnitude and general shape.  
  
  subroutine get_IG_EOS(lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
    double precision, intent(in) :: lnRho, lnT
    double precision, intent(out) :: f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
    double precision :: N_A, mu, alpha, k, h, m, Rho, T
    
    Rho = exp(lnRho)
    T = exp(lnT)
    
    h = 6.6e-27 
    k = 1.38e-16 !! erg K^-1
    mu = 18. !! Water
    N_A = 6.e23 !! Number of particles per mole
    m = 18. / N_A !! g / particle
    alpha = (h / (2 * 3.14 * m * k)**0.5)**3.
    
    !! Get the equation of state here
    
    f_val = -(N_A / mu) * k * T * ( - log(rho) - log(N_A / mu) - log(alpha) + 1.5 * log(T) + 1.)
    df_drho =  (N_A / mu) * k * T / rho
    df_dt = -(N_A / mu) * k * ( - log(rho) - log(N_A / mu) - log(alpha) + 1.5 * log(T) + 2.5)
    d2f_drho2 = -(N_A / mu) * k * T / rho**2
    d2f_dt2 =  -(N_A / mu) * 1.5 * k  / T
    d2f_drhodt =  (N_A / mu) * k / rho
    d3f_drho3 = (N_A / mu) * 2. * k * T / rho**3
    d3f_drho2dt = -(N_A / mu) * k / rho**2
    d3f_drhodt2 = 0.
    d3f_dt3 = (N_A / mu) * 1.5 * k / T**2.
    d4f_drho2dt2 = 0.
    
  end subroutine get_IG_EOS
  
  subroutine convDF_thermo(rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
       Pvect, Evect, Svect)
!    use h5table, only: i_val, i_drho, i_dt, i_drho2, i_dt2, i_drhodt
    
    double precision, intent(in) :: rho, t, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3
    double precision, dimension(:), pointer, intent(out) :: Pvect, Evect, Svect
    double precision :: RhoSQ
    
    RhoSQ = rho*rho
    Pvect(i_val) = RhoSQ * df_drho
    Pvect(i_drho) = RhoSQ * d2f_drho2 + 2 * rho * df_drho
    Pvect(i_dt) = RhoSQ * d2f_drhodt
    Pvect(i_drho2) = RhoSQ * d3f_drho3 + 4. * rho * d2f_drho2 + 2. * df_drho 
    Pvect(i_dt2) = RhoSQ * d3f_drhodt2
    Pvect(i_drhodt) = RhoSQ * d3f_drho2dt + 2. * rho * d2f_drhodt
    
    Svect(i_val) = -df_dt
    Svect(i_drho) = -d2f_dt2
    Svect(i_dt) = -d2f_drhodt
    Svect(i_drho2) = -d3f_drho2dt
    Svect(i_dt2) = - d3f_dt3
    Svect(i_drhodt) = - d3f_drhodt2
    
    Evect(i_val) = f_val - T * df_dt
    Evect(i_drho) = df_drho - T * d2f_drhodt
    Evect(i_dt) = -T * d2f_dt2
    Evect(i_drho2) = d2f_drho2 - T*d3f_drho2dt
    Evect(i_dt2) = -d2f_dt2 - T * d3f_dt3
    Evect(i_drhodt) = - T * d3f_drhodt2
  end subroutine convDF_thermo
  
  subroutine get_val(params, Rho, T, Pvect, Evect, Svect, ierr)
    type (idealparams), pointer, intent(in) :: params
    double precision, intent(in) :: Rho, T
    double precision, intent(out), pointer, dimension(:) :: Pvect, Evect, Svect
    integer, intent(out) :: ierr

    double precision :: lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
    
    lnRho = log(Rho)
    lnT   = log(T)
    
    call get_IG_EOS(lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)

    call convDF_thermo(Rho, T, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
       Pvect, Evect, Svect)    
  end subroutine get_val

  !! Write routines to perform lookup routine.
  !! This should be general enough that we can easily swap in the code later into the h5 table
  
  !! Put lookup routines here
  
  !! This is exactly the format that the generalized 2D table uses
  subroutine get_2Dtable_val(params, Rho, T, Zi_dv, ierr)
    type (idealparams), pointer, intent(in) :: params
    double precision, intent(in) :: Rho, T
    double precision, intent(out), pointer, dimension(:,:) :: Zi_dv
    integer, intent(out) :: ierr

    double precision, pointer, dimension(:) :: Pvect, Evect, Svect

    double precision :: lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
         d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2
    
    lnRho = log(Rho)
    lnT   = log(T)
    
    Pvect => params%Pvect
    Evect => params%Evect
    Svect => params%Svect
    
    call get_IG_EOS(lnRho, lnT, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, d4f_drho2dt2)
    
    call convDF_thermo(Rho, T, f_val, df_drho, df_dt, d2f_drho2, d2f_dt2, d2f_drhodt, &
       d3f_drho3, d3f_drho2dt, d3f_drhodt2, d3f_dt3, &
       Pvect, Evect, Svect) 
    Zi_dv(:,i_P) = Pvect
    Zi_dv(:,i_E) = Evect
    Zi_dv(:,i_S) = Svect
    
  end subroutine get_2Dtable_val
  
  
  double precision function DeltaZ_X(Yval,dZdX,lrpar,rpar,lipar,ipar,ierr)
    integer, intent(in) :: lrpar, lipar
    double precision, intent(in) :: Yval
    double precision, intent(out) :: dZdX
    double precision, intent(inout), target :: rpar(lrpar)
    integer, intent (inout), target :: ipar(lipar)
    integer, intent (out) :: ierr
    double precision :: Xval, Zval0
    integer :: tbl_handle, Zind
    type (idealparams), pointer :: tbl
    double precision, pointer, dimension(:,:) :: Zi_dv      
    
    ierr = 0
    Xval = rpar(1)  !! X is fixed
    Zval0 = rpar(2)  !! The target
    tbl_handle = ipar(1) !! This gets the table handle
    Zind = ipar(2) !! which Z surface to index
    
    call get_tbl(tbl_handle, tbl, ierr)
!      call lock_handle(tbl_handle, ierr)
    Zi_dv => tbl%Zi_dv
    call get_2Dtable_val(tbl, Xval, Yval, Zi_dv, ierr)
    DeltaZ_X = Zi_dv(Zind,1) - Zval0
    dZdX = Zi_dv(Zind,2)
    !      call unlock_handle(tbl_handle, ierr)
    
  end function DeltaZ_X

  subroutine get_2Dtab_invXZ(tbl, Xval, Zval, Zind, Yval, ierr)
!    use num_lib, only: safe_root_with_initial_guess
      
    type (idealparams), pointer, intent(in) :: tbl
    double precision, intent(in) :: Xval, Zval
    integer, intent(in) :: Zind
    double precision, intent(out) :: Yval
    integer, intent(out) :: ierr
    integer :: tbl_handle
    double precision, parameter :: eps = 1d-5
    double precision, pointer, dimension(:,:) :: Zi_dv

    integer, parameter :: lrpar = 2, lipar = 2
    double precision, target :: rpar(lrpar)
    integer, target :: ipar(lipar)
    double precision :: Z1, Z2, Y1, Y2, Y_guess
    integer :: itermax
    
    call set_handle(tbl, tbl_handle, ierr)
      
    rpar(1) = Xval
    rpar(2) = Zval
    
    ipar(1) = tbl_handle
    ipar(2) = Zind
    
    Y1 = tbl%minY
    Y2 = tbl%maxY
    
    Zi_dv => tbl%Zi_dv
      
!!! USE these calls to get Z1, and Z2
    call get_2Dtable_val(tbl, Xval, Y1, Zi_dv, ierr)
    Z1 = Zi_dv(Zind,1) - Zval
    call get_2Dtable_val(tbl, Xval, Y2, Zi_dv, ierr)
    Z2 = Zi_dv(Zind,1) - Zval

!      call unlock_handle(tbl_handle, ierr)  !! Important to unlock before calling safe_root..
      
    Y_guess = Y2 + (Y2 - Y1) / (Z2 - Z1) * ( - Z2)
      
    itermax = 200
      
!    Yval = safe_root_with_initial_guess(DeltaZ_X, Y_guess, Y1, Y2, Z1, Z2, itermax, eps, eps, &
!         lrpar, rpar, lipar, ipar, ierr)
      
    call free_handle(tbl_handle, ierr)

  end subroutine get_2Dtab_invXZ
    
    
  double precision function DeltaZ_Y(Xval,dZdY,lrpar,rpar,lipar,ipar,ierr)
    integer, intent(in) :: lrpar, lipar
    double precision, intent(in) :: Xval
    double precision, intent(out) :: dZdY
    double precision, intent(inout), target :: rpar(lrpar)
    integer, intent (inout), target :: ipar(lipar)
    integer, intent (out) :: ierr
    double precision :: Yval,Zval0
    integer :: tbl_handle, Zind
    type (idealparams), pointer :: tbl
    double precision, pointer, dimension(:,:) :: Zi_dv      

    ierr = 0
    Yval = rpar(1)  !! X is fixed
    Zval0 = rpar(2)  !! The target
    tbl_handle = ipar(1) !! This gets the table handle
    Zind = ipar(2) !! which Z surface to index
    
    call get_tbl(tbl_handle, tbl, ierr)

    Zi_dv => tbl%Zi_dv

    call get_2Dtable_val(tbl, Xval, Yval, Zi_dv, ierr)
    DeltaZ_Y = Zi_dv(Zind,1) - Zval0
    dZdY = Zi_dv(Zind,3)
      
  end function DeltaZ_Y

  subroutine get_2Dtab_invZY(tbl, Yval, Zval, Zind, Xval, ierr)
!    use num_lib, only: safe_root_with_initial_guess
      
    type (idealparams), pointer, intent(in) :: tbl
    double precision, intent(in) :: Yval, Zval
    integer, intent(in) :: Zind
    double precision, intent(out) :: Xval
    integer, intent(out) :: ierr
    integer :: tbl_handle
    double precision, parameter :: eps = 1d-4
    double precision, pointer, dimension(:,:) :: Zi_dv
    
    integer, parameter :: lrpar = 2, lipar = 2
    double precision, target :: rpar(lrpar)
    integer, target :: ipar(lipar)
    double precision :: Z1, Z2, X1, X2, X_guess
    integer :: itermax

    call set_handle(tbl, tbl_handle, ierr)
      
    rpar(1) = Yval
    rpar(2) = Zval
      
    ipar(1) = tbl_handle
    ipar(2) = Zind
      
    X1 = tbl%minX
    X2 = tbl%maxX
      
      !!! USE these calls to get Z1, and Z2
    Zi_dv => tbl%Zi_dv
!      allocate(Zi_dv(tbl%N_i,6))
    call get_2Dtable_val(tbl, X1, Yval, Zi_dv, ierr)
    Z1 = Zi_dv(Zind,1) - Zval
    call get_2Dtable_val(tbl, X2, Yval, Zi_dv, ierr)
    Z2 = Zi_dv(Zind,1) - Zval
!      deallocate(Zi_dv)
!      call unlock_handle(tbl_handle, ierr)  !! Important to unlock before calling safe_root..
      
    X_guess = X2 + (X2 - X1) / (Z2 - Z1) * ( - Z2)
    itermax = 200
      
 !   Xval = safe_root_with_initial_guess(DeltaZ_Y, X_guess, X1, X2, Z1, Z2, itermax, eps, eps, &
 !        lrpar, rpar, lipar, ipar, ierr)
    if(ierr .ne. 0) then
       write(*,*) "rootfind error: ", ierr
    endif
    call free_handle(tbl_handle, ierr)
      
  end subroutine get_2Dtab_invZY
    
  subroutine get_rho_PT(tbl, P, T, Rho, ierr)
    type (idealparams), pointer, intent(in) :: tbl
    double precision, intent(in) :: P, T
    double precision, intent(out) :: Rho
    integer, intent(out) :: ierr

    call get_2Dtab_invZY(tbl, T, P, i_P, Rho, ierr)
    
  end subroutine get_rho_PT
  
  subroutine get_rho_ET(tbl, E, T, Rho, ierr)
    type (idealparams), pointer, intent(in) :: tbl
    double precision, intent(in) :: E, T
    double precision, intent(out) :: Rho
    integer, intent(out) :: ierr

    call get_2Dtab_invZY(tbl, T, E, i_E, Rho, ierr)
  end subroutine get_rho_ET
  
  subroutine get_rho_ST(tbl, S, T, Rho, ierr)
    type (idealparams), pointer, intent(in) :: tbl
    double precision, intent(in) :: S, T
    double precision, intent(out) :: Rho
    integer, intent(out) :: ierr

    call get_2Dtab_invZY(tbl, T, S, i_S, Rho, ierr)
  end subroutine get_rho_ST
  
end module ideal_water
