module aneos_access
!  use h5table
  implicit none
  
  integer, parameter :: maxmat=21
  integer :: imat,nntype(maxmat),kpai
  
  double precision, parameter :: evtemp = 11604.8d0

  double precision, parameter :: delta1 = 1d-4
  double precision, parameter :: delta2 = 1d-2
  double precision, parameter :: delta34 = 1d-2
  double precision, parameter :: logdelta3 = 0.1
  logical, parameter :: lognder = .true.
  
  integer :: nmat
  logical :: ANEOS_init = .false.  
  
  integer, parameter :: num_free_derivs = 9
  integer, parameter :: a_val = 1
  integer, parameter :: a_dRho = 2
  integer, parameter :: a_dT = 3
  integer, parameter :: a_dRho2 = 4
  integer, parameter :: a_dT2 = 5
  integer, parameter :: a_dRhodT = 6
  integer, parameter :: a_dRho2dT = 7
  integer, parameter :: a_dRhodT2 = 8
  integer, parameter :: a_dRho2dT2 = 9
  
contains
  subroutine ANEOSWater_Init
    external aneos2
    COMMON /FILEOS/ klst, kinp
    integer :: klst, kinp
    
    if(ANEOS_init .eqv. .true.) then
       write(*,*) "Don't initialize twice"
       stop
    endif

    kinp=21
    klst=22
    open(kinp,file='INPUT/water.input')
    open(klst, file='OUTPUT/aneos.output')
    nmat = 1
    imat = 1
    nntype(1) = -1
    nntype(2) = -2
    nntype(3) = -3
    nntype(4) = -4
    nntype(5) = -5
    call aneos2(1,nmat,0,nntype)
    write(*,*) "ANEOS initialized"
    kpai = 2  !! For when we call aneos later
    ANEOS_init = .true.
  end subroutine ANEOSWater_Init
  
  subroutine ANEOSSerpentine_Init
    external aneos2
    COMMON /FILEOS/ klst, kinp
    integer :: klst, kinp
    
    if(ANEOS_init .eqv. .true.) then
       write(*,*) "Don't initialize twice"
       stop
    endif

    kinp=21
    klst=22
!    open(kinp,file='INPUT/aneos2.input')
    open(kinp, file='INPUT/serpentine2.input')
    open(klst, file='OUTPUT/aneos_serp.output')
    nmat = 5
    imat = 1
    nntype(1) = -3
!    nntype(2) = -2
!    nntype(3) = -3
!    nntype(4) = -4
!    nntype(5) = -5
    call aneos2(1,nmat,0,nntype)
    write(*,*) "ANEOS initialized"
    kpai = 2  !! For when we call aneos later
    ANEOS_init = .true.
  end subroutine ANEOSSerpentine_Init

  subroutine ANEOSIron_Init
    external aneos2
    COMMON /FILEOS/ klst, kinp
    integer :: klst, kinp
    
    if(ANEOS_init .eqv. .true.) then
       write(*,*) "Don't initialize twice"
       stop
    endif
    
    kinp=21
    klst=22
!    open(kinp,file='INPUT/aneos2.input')
    open(kinp,file='INPUT/iron2.input')
    open(klst, file='OUTPUT/aneos_iron.output')
    nmat = 5
    imat = 1
!    nntype(1) = -1
!    nntype(2) = -2
!    nntype(3) = -3
!    nntype(4) = -4
!    nntype(5) = -5
    nntype(1) = -5
    call aneos2(1,nmat,0,nntype)
    write(*,*) "ANEOS initialized"
    kpai = 2  !! For when we call aneos later
    ANEOS_init = .true.
  end subroutine ANEOSIron_Init
  
  
  !! Maybe modify this to also output other stuff for testing purposes.  The main thing though to test
  !!   is that the values of P, E, and S are correct
  subroutine ANEOS_get_PES(Rho, T, P, E, S)    
    
    external aneos
    
    double precision, intent(in) :: Rho, T !! cgs, K
    double precision, intent(out) :: P, E, S
    double precision :: Tev, Cv, dPdTev, dPdRho, ffkrosi, cs, ffve, ffva 
    
    if(ANEOS_init .eqv. .false.) then
       write(*,*) "You need to call ANEOSWater_Init"
       stop
    endif
    
    Tev = T / evtemp

    P = 0d0
    E = 0d0
    S = 0d0

!    write(*,*) "Calling aneos: "
!    write(*,*) "Tev = ", Tev
!    write(*,*) "Rho = ", Rho
!    write(*,*) "imat = ", imat
    

    call aneos(Tev, Rho, P, E, S, Cv, dPdTev, &
               dPdRho,ffkrosi, cs, kpai, imat, ffve, ffva)
    S = S / evtemp
!    write(*,*) "P = ", P
!    write(*,*) "S = ", S
!    write(*,*) "E = ", E

  end subroutine ANEOS_get_PES

  !! Only one output version
  subroutine ANEOS_get_free(Rho, T, Fvect)
    external aneos
    COMMON /FILEOS/ klst, kinp

    integer :: klst, kinp

    double precision, intent(in) :: Rho, T  !! Take inputs in [g/cc] and [K]
    double precision, intent(out) :: Fvect(num_free_derivs)
    
    double precision :: Tev, dRho, dTev, tmpvar, dlog10Rho, dlog10Tev, log10Rho, log10Tev
    double precision :: ppi,uui,ssi,ccvi
    double precision :: ddpdti,ddpdri,ffkrosi,ccsi,ffve,ffva
    double precision :: dfdx_num, dfdy_num, d2fdx2_num, d2fdy2_num, d2fdxdy_num, d3fdxdy2_num, d3fdx2dy_num, d4fdx2dy2_num
    double precision :: dfdlx_num, dfdly_num, d2fdlx2_num, d2fdly2_num, d2fdlxdly_num, &
         d3fdlxdly2_num, d3fdlx2dly_num, d4fdlx2dly2_num
    double precision :: tmp_dfdx_num, tmp_dfdy_num, tmp_d2fdx2_num, tmp_d2fdy2_num, tmp_d2fdxdy_num
    double precision :: dfdx_an, dfdy_an, d2fdx2_an, d2fdy2_an, d2fdxdy_an
    double precision :: free_energy, dfdrho, dfdt, d2fdrho2, d2fdt2, d2fdrhodt, d3fdrho2dt, d3fdrhodt2, d4fdrho2dt2

    double precision :: RhoVect(5), TevVect(5), log10RhoVect(5), log10TevVect(5)
    !! take 9 points at every point to calculate numerical derivatives      
    double precision :: free_cluster(5,5), dfdx_cl(5,5),dfdy_cl(5,5),&
         d2fdx2_cl(5,5), d2fdy2_cl(5,5), d2fdxdy_cl(5,5)
    double precision :: dfdx_ln(5), dfdy_ln(5), d2fdx2_ln(5), d2fdy2_ln(5)
    
    integer :: idx, idy, i
    
    logical, parameter :: use_an = .false.
    logical, parameter :: use_Ean = .false.
    
    integer :: ierr



    if(ANEOS_init .eqv. .false.) then
       write(*,*) "You need to call ANEOSWater_Init"
       stop
    endif
    
    kpai = 2
    Tev = T / evtemp

! Here is the code for log taking derivatives over log spacing
!  I found, however that it just didn't improve the situation
!    if(log_nder) then
!       log10Rho = log10(Rho)
!       log10Tev = log10(Tev)

!       dlog10Rho = logDelta
!       dlog10Tev = logDelta
!       dRho = dlog10Rho
!       dTev = dlog10Tev

!       log10RhoVect(1) = log10Rho - 2d0 * dlog10Rho
!       log10RhoVect(2) = log10Rho - 1d0 * dlog10Rho
!       log10RhoVect(3) = log10Rho
!       log10RhoVect(4) = log10Rho + 1d0 * dlog10Rho
!       log10RhoVect(5) = log10Rho + 2d0 * dlog10Rho

!       RhoVect = 1d1**log10RhoVect

!       log10TevVect(1) = log10Tev - 2d0 * dlog10Tev
!       log10TevVect(2) = log10Tev - 1d0 * dlog10Tev
!       log10TevVect(3) = log10Tev 
!       log10TevVect(4) = log10Tev + 1d0 * dlog10Tev
!       log10TevVect(5) = log10Tev + 2d0 * dlog10Tev
!
!       TevVect = 1d1**log10TevVect
!    else

    !! Calculate first derivatives
    call calculate_derivatives1(Rho, Tev, delta1, dfdx_num, dfdy_num, ierr)

    !! 2nd Derivatives
    !!  Stencil uses a different delta
    call calculate_derivatives2(Rho, Tev, delta2, d2fdx2_num, d2fdy2_num, d2fdxdy_num, ierr)

    !! Possibly for the highest order derivatives, we can use the logderivatives
    !! Compute the 3rd derivatives

    call calculate_derivatives34(Rho, Tev, delta34, d3fdx2dy_num, d3fdxdy2_num, d4fdx2dy2_num, ierr)

    if(lognder) then
       log10Rho = log10(Rho)
       log10Tev = log10(Tev)
       
       dlog10Rho = logDelta3
       dlog10Tev = logDelta3
       
       log10RhoVect(1) = log10Rho - 2d0 * dlog10Rho
       log10RhoVect(2) = log10Rho - 1d0 * dlog10Rho
       log10RhoVect(3) = log10Rho
       log10RhoVect(4) = log10Rho + 1d0 * dlog10Rho
       log10RhoVect(5) = log10Rho + 2d0 * dlog10Rho
       
       RhoVect = 1d1**log10RhoVect
    
       log10TevVect(1) = log10Tev - 2d0 * dlog10Tev
       log10TevVect(2) = log10Tev - 1d0 * dlog10Tev
       log10TevVect(3) = log10Tev 
       log10TevVect(4) = log10Tev + 1d0 * dlog10Tev
       log10TevVect(5) = log10Tev + 2d0 * dlog10Tev
       
       TevVect = 1d1**log10TevVect

!    dRho = Rho * delta3
!    dTev = Tev * delta3
       
!    RhoVect(1) = Rho - 2.d0 * dRho
!    RhoVect(2) = Rho - dRho
!    RhoVect(3) = Rho 
!    RhoVect(4) = Rho + dRho
!    RhoVect(5) = Rho + 2.d0 * dRho
    
!    TevVect(1) = Tev - 2.d0 * dTev
!    TevVect(2) = Tev - dTev
!    TevVect(3) = Tev
!    TevVect(4) = Tev + dTev
!    TevVect(5) = Tev + 2.d0 * dTev

       do idx=1,5
          do idy=1,5
             call aneos(TevVect(idy), RhoVect(idx), ppi, uui, ssi, ccvi, ddpdti, &
                  ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
             
             free_cluster(idx,idy) = uui - TevVect(idY) * ssi
          enddo
       enddo

       
       
       do i=1,5
          dfdx_ln(i) = (-free_cluster(5,i) + 8.d0* free_cluster(4,i) &
               - 8d0*free_cluster(2,i) + free_cluster(1,i)) / (12.d0*dlog10Rho)
          dfdy_ln(i) = (-free_cluster(i,5) + 8.d0 * free_cluster(i,4) &
               - 8d0*free_cluster(i,2) + free_cluster(i,1)) / (12.d0*dlog10Tev)
          d2fdx2_ln(i) = (-3d0 * free_cluster(5,i) + 8d0 * free_cluster(4,i) &
               - 10d0 * free_cluster(3,i) +  8d0*free_cluster(2,i) &
               - 3d0*free_cluster(1,i)) / (6d0 * dlog10Rho * dlog10Rho)
          d2fdy2_ln(i) = (-3d0 * free_cluster(i,5) + 8d0 * free_cluster(i,4) &
               - 10d0 * free_cluster(i,3) +  8d0*free_cluster(i,2) &
               - 3d0*free_cluster(i,1)) / (6d0 * dlog10Tev * dlog10Tev)
       enddo
       
       dfdlx_num = dfdx_ln(3)
       dfdly_num = dfdy_ln(3)
       d2fdlx2_num = d2fdx2_ln(3)
       d2fdly2_num = d2fdy2_ln(3)
       
       d3fdlxdly2_num = (-d2fdy2_ln(5) + 8d0 * d2fdy2_ln(4) &
            - 8d0 * d2fdy2_ln(2) + d2fdy2_ln(1)) / (12d0 * dlog10Rho)
       d3fdlx2dly_num = (-d2fdx2_ln(5) + 8d0 * d2fdx2_ln(4) &
            - 8d0 * d2fdx2_ln(2) + d2fdx2_ln(1)) / (12d0 * dlog10Tev)
       d4fdlx2dly2_num = (-3d0 * d2fdx2_ln(5) + 8d0 * d2fdx2_ln(4) &
            - 10d0 * d2fdx2_ln(3) + 8d0 * d2fdx2_ln(2) &
            - 3d0 * d2fdx2_ln(1)) / (6d0 * dlog10Tev * dlog10Tev)
       
       tmp_dfdx_num = dfdlx_num / Rho
       tmp_dfdy_num = dfdly_num / Tev
       tmp_d2fdx2_num = (d2fdlx2_num - tmp_dfdx_num * Rho) / Rho**2d0
       tmp_d2fdy2_num = (d2fdly2_num - tmp_dfdy_num * Tev) / Tev**2d0
       tmp_d2fdxdy_num = d2fdlxdly_num / (Rho * Tev)
       
       d3fdx2dy_num = (d3fdlx2dly_num - Rho * Tev * d2fdxdy_num) / (Rho**2d0 * Tev) 
       d3fdxdy2_num = (d3fdlxdly2_num - Rho * Tev * d2fdxdy_num) / (Rho* Tev**2d0) 
       
       d4fdx2dy2_num = (d4fdlx2dly2_num - Rho * Tev * d2fdxdy_num &
            - Rho**2d0 * Tev * d3fdx2dy_num &
            - Rho * Tev**2d0 * d3fdxdy2_num) &
            / (Rho**2d0 * Tev**2d0)
    end if
!    d3fdxdy2_num = (-d2fdy2_ln(5) + 8d0 * d2fdy2_ln(4) - 8d0 * d2fdy2_ln(2) + d2fdy2_ln(1)) / (12d0 * dRho)
!    d3fdx2dy_num = (-d2fdx2_ln(5) + 8d0 * d2fdx2_ln(4) - 8d0 * d2fdx2_ln(2) + d2fdx2_ln(1)) / (12d0 * dTev)
!    d4fdx2dy2_num = (-3d0 * d2fdx2_ln(5) + 8d0 * d2fdx2_ln(4) - 10d0 * d2fdx2_ln(3) + 8d0 * d2fdx2_ln(2) &
!         - 3d0 * d2fdx2_ln(1)) / (6d0 * dTev * dTev)


!    do idx=1,5
!       do idy=1,5
!          call aneos(TevVect(idy), RhoVect(idx), ppi, uui, ssi, ccvi, ddpdti, &
!               ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
!          
!          free_cluster(idx,idy) = uui - TevVect(idY) * ssi
!          dfdx_cl(idx,idy)= ppi / RhoVect(idX)**2d0
!          dfdy_cl(idx,idy) = -ssi
!          d2fdx2_cl(idx,idy) = (1. / RhoVect(idX)**2d0) * (ddpdri - 2d0 * ppi / RhoVect(idx))
!          d2fdy2_cl(idx,idy) = -ccvi / TevVect(idY)
!          d2fdxdy_cl(idx,idy) = (1. / RhoVect(idX)**2d0) * ddpdti
!       enddo
!    enddo
    
    !!! IMPORTANT NOTE
    !! if the log_nder flag is set, then dRho => dlogRho 
    !!   and dTev => dlogTev
!    do i=1,5
!       dfdx_ln(i) = (-free_cluster(5,i) + 8.d0* free_cluster(4,i) - 8d0*free_cluster(2,i) + free_cluster(1,i)) / (12.d0*dRho)
!       dfdy_ln(i) = (-free_cluster(i,5) + 8.d0 * free_cluster(i,4) - 8d0*free_cluster(i,2) + free_cluster(i,1)) / (12.d0*dTev)
!       d2fdx2_ln(i) = (-3d0 * free_cluster(5,i) + 8d0 * free_cluster(4,i) - 10d0 * free_cluster(3,i) +  8d0*free_cluster(2,i) &
!            - 3d0*free_cluster(1,i)) / (6d0 * dRho * dRho)
!       d2fdy2_ln(i) = (-3d0 * free_cluster(i,5) + 8d0 * free_cluster(i,4) - 10d0 * free_cluster(i,3) +  8d0*free_cluster(i,2) &
!            - 3d0*free_cluster(i,1)) / (6d0 * dTev * dTev)
!    enddo
    
!    dfdx_num = dfdx_ln(3)
!    dfdy_num = dfdy_ln(3)
!    d2fdx2_num = d2fdx2_ln(3)
!    d2fdy2_num = d2fdy2_ln(3)

!    d2fdxdy_num = (-dfdx_ln(5) + 8d0 * dfdx_ln(4) - 8d0 * dfdx_ln(2) + dfdx_ln(1)) / (12d0 * dTev)    

    !! Get higher order derivatives    
!    d3fdxdy2_num = (-d2fdy2_ln(5) + 8d0 * d2fdy2_ln(4) - 8d0 * d2fdy2_ln(2) + d2fdy2_ln(1)) / (12d0 * dRho)
!    d3fdx2dy_num = (-d2fdx2_ln(5) + 8d0 * d2fdx2_ln(4) - 8d0 * d2fdx2_ln(2) + d2fdx2_ln(1)) / (12d0 * dTev)
!    d4fdx2dy2_num = (-3d0 * d2fdx2_ln(5) + 8d0 * d2fdx2_ln(4) - 10d0 * d2fdx2_ln(3) + 8d0 * d2fdx2_ln(2) &
!         - 3d0 * d2fdx2_ln(1)) / (6d0 * dTev * dTev)
    

!    if(log_nder) then
!       dfdlx_num = dfdx_num
!       dfdly_num = dfdy_num
!       d2fdlx2_num = d2fdx2_num
!       d2fdly2_num = d2fdy2_num
!       d2fdlxdly_num = d2fdxdy_num
!       d3fdlx2dly_num = d3fdx2dy_num
!       d3fdlxdly2_num = d3fdxdy2_num
!       d4fdlx2dly2_num = d4fdx2dy2_num


!       dfdx_num = dfdlx_num / Rho
!       dfdy_num = dfdly_num / Tev
!       d2fdx2_num = (d2fdlx2_num - dfdx_num * Rho) / Rho**2d0
!       d2fdy2_num = (d2fdly2_num - dfdy_num * Tev) / Tev**2d0
!       d2fdxdy_num = d2fdlxdly_num / (Rho * Tev)
!       d3fdx2dy_num = (d3fdlx2dly_num - Rho * Tev * d2fdxdy_num) / (Rho**2d0 * Tev) 
!       d3fdxdy2_num = (d3fdlxdly2_num - Rho * Tev * d2fdxdy_num) / (Rho* Tev**2d0)

!       d4fdx2dy2_num = (d4fdlx2dly2_num - Rho * Tev * d2fdxdy_num &
!            - Rho**2d0 * Tev * d3fdx2dy_num &
!            - Rho * Tev**2d0 * d3fdxdy2_num) &
!            / (Rho**2d0 * Tev**2d0)
!    endif

!    dfdx_an = dfdx_cl(3,3)    
!    dfdy_an = dfdy_cl(3,3)
!    d2fdx2_an = d2fdx2_cl(3,3)
!    d2fdy2_an = d2fdy2_cl(3,3)
!    d2fdy2_an = (-dfdy_cl(3,5) + 8.d0 * dfdy_cl(3,4) - 8d0*dfdy_cl(3,2) + dfdy_cl(3,1)) / (12.d0*dTev)
!    d2fdxdy_an = d2fdxdy_cl(3,3)    
    call aneos(Tev, Rho, ppi, uui, ssi, ccvi, ddpdti, &
         ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
    free_energy = uui - Tev * ssi

!    if(use_an) then
!       dfdrho = dfdx_an
!       dfdt = dfdy_an
!       d2fdrho2 = d2fdx2_an
!       d2fdt2 = d2fdy2_an
!       d2fdrhodt = d2fdxdy_an
!    else
    dfdrho = dfdx_num
    dfdt = dfdy_num
    d2fdrho2 = d2fdx2_num
    d2fdt2 = d2fdy2_num
    d2fdrhodt = d2fdxdy_num
!    endif

    d3fdrhodt2 = d3fdxdy2_num
    d3fdrho2dt = d3fdx2dy_num
    d4fdrho2dt2 = d4fdx2dy2_num
    
    !! Consolidate all of these into the final vector
    Fvect(a_val) = free_energy
    Fvect(a_dRho) = dfdrho
    Fvect(a_dT) = dfdt / evtemp
    Fvect(a_dRho2) = d2fdrho2
    Fvect(a_dT2) = d2fdt2 /evtemp**2d0
    Fvect(a_dRhodT) = d2fdrhodt / evtemp
    Fvect(a_dRho2dT) = d3fdrho2dt / evtemp
    Fvect(a_dRhodT2) = d3fdrhodt2 / evtemp**2d0
    Fvect(a_dRho2dT2) = d4fdrho2dt2 / evtemp**2d0

  contains
    
    !! Calculate the first order derivatives
    subroutine calculate_derivatives1(Rho, Tev, delta1, dfdrho_num, dfdt_num, ierr)
      use h5table_params


      double precision, intent(in) :: Rho, Tev, delta1
      double precision, intent(out) :: dfdrho_num, dfdt_num
      integer, intent(out) :: ierr

      double precision :: RhoVect(5), TevVect(5)
      double precision :: dRho, dTev
      integer :: idx, idy
      double precision :: free_cluster(5,5)
      integer :: kpai_sten(5,5)
      
      ierr = 0

      dRho = Rho * delta1
      dTev = Tev * delta1
       
      RhoVect(1) = Rho - 2.d0 * dRho
      RhoVect(2) = Rho - dRho
      RhoVect(3) = Rho 
      RhoVect(4) = Rho + dRho
      RhoVect(5) = Rho + 2.d0 * dRho
    
      TevVect(1) = Tev - 2.d0 * dTev
      TevVect(2) = Tev - dTev
      TevVect(3) = Tev
      TevVect(4) = Tev + dTev
      TevVect(5) = Tev + 2.d0 * dTev

      call aneos(TevVect(3), RhoVect(3), ppi, uui, ssi, ccvi, ddpdti, &
                    ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
               
      free_cluster(3,3) = uui - TevVect(3) * ssi
      kpai_sten(3,3) = kpai

      idy = 3
      do idx=1,5
         if(idx .ne. 3) then
            call aneos(TevVect(idy), RhoVect(idx), ppi, uui, ssi, ccvi, ddpdti, &
                 ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
               
            free_cluster(idx,idy) = uui - TevVect(idy) * ssi
            kpai_sten(idx,idy) = kpai
            
            if(kpai_sten(idx,idy) .ne. kpai_sten(3,3)) then
               ierr = H5TBLERR_NDERMIX
            endif
         endif
      enddo

      idx = 3
      do idy=1,5
         if(idy .ne. 3) then
            call aneos(TevVect(idy), RhoVect(idx), ppi, uui, ssi, ccvi, ddpdti, &
                 ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
            
            free_cluster(idx,idy) = uui - TevVect(idy) * ssi
            kpai_sten(idx,idy) = kpai
            
            if(kpai_sten(idx,idy) .ne. kpai_sten(3,3)) then
               ierr = H5TBLERR_NDERMIX
            endif
         endif
      enddo
      
      dfdrho_num = (-free_cluster(5,3) + 8.d0* free_cluster(4,3) - 8d0*free_cluster(2,3) + free_cluster(1,3)) / (12.d0*dRho)
      dfdt_num = (-free_cluster(3,5) + 8.d0 * free_cluster(3,4) - 8d0*free_cluster(3,2) + free_cluster(3,1)) / (12.d0*dTev)
      
    end subroutine calculate_derivatives1

    subroutine calculate_derivatives2(Rho, Tev, delta2, d2fdRho2_num, d2fdT2_num, d2fdRhodT_num, ierr)
      use h5table_params

      double precision, intent(in) :: Rho, Tev, delta2
      double precision, intent(out) :: d2fdRho2_num, d2fdT2_num, d2fdRhodT_num
      integer, intent(out) :: ierr

      double precision :: RhoVect(5), TevVect(5)
      double precision :: dRho, dTev
      integer :: idx, idy
      double precision :: free_cluster(5,5)
      double precision :: dfdRho_ln(5), dfdT_ln(5), d2fdRho2_ln(5), d2fdT2_ln(5)
      integer :: kpai_sten(5,5)

      ierr = 0

      dRho = Rho * delta2
      dTev = Tev * delta2
       
      RhoVect(1) = Rho - 2.d0 * dRho
      RhoVect(2) = Rho - dRho
      RhoVect(3) = Rho 
      RhoVect(4) = Rho + dRho
      RhoVect(5) = Rho + 2.d0 * dRho
      
      TevVect(1) = Tev - 2.d0 * dTev
      TevVect(2) = Tev - dTev
      TevVect(3) = Tev
      TevVect(4) = Tev + dTev
      TevVect(5) = Tev + 2.d0 * dTev
      
      do idx=1,5
         do idy=1,5
            call aneos(TevVect(idy), RhoVect(idx), ppi, uui, ssi, ccvi, ddpdti, &
                 ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
            free_cluster(idx,idy) = uui - TevVect(idY) * ssi
            kpai_sten(idx,idy) = kpai
            
         enddo
      enddo
      
      do idx=1,5
         do idy=1,5
            if(kpai_sten(idx,idy) .ne. kpai_sten(3,3)) then
               ierr = H5TBLERR_NDERMIX
            endif
         enddo
      enddo

      do i=1,5
         dfdRho_ln(i) = (-free_cluster(5,i) + 8.d0* free_cluster(4,i) - 8d0*free_cluster(2,i) + free_cluster(1,i)) / (12.d0*dRho)
         dfdT_ln(i) = (-free_cluster(i,5) + 8.d0 * free_cluster(i,4) - 8d0*free_cluster(i,2) + free_cluster(i,1)) / (12.d0*dTev)
         d2fdRho2_ln(i) = (-3d0 * free_cluster(5,i) + 8d0 * free_cluster(4,i) - 10d0 * free_cluster(3,i) +  8d0*free_cluster(2,i) &
              - 3d0*free_cluster(1,i)) / (6d0 * dRho * dRho)
         d2fdT2_ln(i) = (-3d0 * free_cluster(i,5) + 8d0 * free_cluster(i,4) - 10d0 * free_cluster(i,3) +  8d0*free_cluster(i,2) &
              - 3d0*free_cluster(i,1)) / (6d0 * dTev * dTev)
      enddo
      d2fdRho2_num = d2fdRho2_ln(3)
      d2fdT2_num = d2fdT2_ln(3)
      d2fdRhodT_num = (-dfdRho_ln(5) + 8d0 * dfdRho_ln(4) - 8d0 * dfdRho_ln(2) + dfdRho_ln(1)) / (12d0 * dTev)    
      
    end subroutine calculate_derivatives2

    subroutine calculate_derivatives34(Rho, Tev, delta34, d3fdRho2dT_num, d3fdRhodT2_num, d4fdRho2dT2_num, ierr)
      use h5table_params

      double precision, intent(in) :: Rho, Tev, delta34
      double precision, intent(out) :: d3fdRho2dT_num, d3fdRhodT2_num, d4fdRho2dT2_num
      integer, intent(out) :: ierr

      double precision :: RhoVect(5), TevVect(5)
      double precision :: dRho, dTev
      integer :: idx, idy
      double precision :: free_cluster(5,5)
      double precision :: dfdRho_ln(5), dfdT_ln(5), d2fdRho2_ln(5), d2fdT2_ln(5)
      integer :: kpai_sten(5,5)

      ierr = 0

      dRho = Rho * delta34
      dTev = Tev * delta34
       
      RhoVect(1) = Rho - 2.d0 * dRho
      RhoVect(2) = Rho - dRho
      RhoVect(3) = Rho 
      RhoVect(4) = Rho + dRho
      RhoVect(5) = Rho + 2.d0 * dRho
      
      TevVect(1) = Tev - 2.d0 * dTev
      TevVect(2) = Tev - dTev
      TevVect(3) = Tev
      TevVect(4) = Tev + dTev
      TevVect(5) = Tev + 2.d0 * dTev
      
      do idx=1,5
         do idy=1,5
            call aneos(TevVect(idy), RhoVect(idx), ppi, uui, ssi, ccvi, ddpdti, &
                 ddpdri,ffkrosi, ccsi, kpai, imat, ffve, ffva)
            free_cluster(idx,idy) = uui - TevVect(idY) * ssi
            kpai_sten(idx,idy) = kpai
            
         enddo
      enddo
      
      do idx=1,5
         do idy=1,5
            if(kpai_sten(idx,idy) .ne. kpai_sten(3,3)) then
               ierr = H5TBLERR_NDERMIX
            endif
         enddo
      enddo

      do i=1,5
         dfdRho_ln(i) = (-free_cluster(5,i) + 8.d0* free_cluster(4,i) - 8d0*free_cluster(2,i) + free_cluster(1,i)) / (12.d0*dRho)
         dfdT_ln(i) = (-free_cluster(i,5) + 8.d0 * free_cluster(i,4) - 8d0*free_cluster(i,2) + free_cluster(i,1)) / (12.d0*dTev)
         d2fdRho2_ln(i) = (-3d0 * free_cluster(5,i) + 8d0 * free_cluster(4,i) - 10d0 * free_cluster(3,i) +  8d0*free_cluster(2,i) &
              - 3d0*free_cluster(1,i)) / (6d0 * dRho * dRho)
         d2fdT2_ln(i) = (-3d0 * free_cluster(i,5) + 8d0 * free_cluster(i,4) - 10d0 * free_cluster(i,3) +  8d0*free_cluster(i,2) &
              - 3d0*free_cluster(i,1)) / (6d0 * dTev * dTev)
      enddo

      d3fdRho2dT_num = (-d2fdRho2_ln(5) + 8d0 * d2fdRho2_ln(4) - 8d0 * d2fdRho2_ln(2) + d2fdRho2_ln(1)) / (12.d0 * dTev)
      d3fdRhodT2_num = (-d2fdT2_ln(5) + 8d0 * d2fdT2_ln(4) - 8d0 * d2fdT2_ln(2) + d2fdT2_ln(1)) / (12d0 * dRho)
      d4fdRho2dT2_num = (-3d0 * d2fdRho2_ln(5) + 8d0 * d2fdRho2_ln(4) - 10d0 * d2fdRho2_ln(3) &
           + 8d0 * d2fdRho2_ln(2) - 3d0 * d2fdRho2_ln(1)) / (6d0 * dTev * dTev)
      
    end subroutine calculate_derivatives34

  end subroutine ANEOS_get_free

  subroutine ANEOS_Shutdown
    integer :: klst, kinp

    COMMON /FILEOS/ klst, kinp

    if(ANEOS_init .eqv. .false.) then
       write(*,*) "You need to call ANEOSWater_Init"
       stop
    endif
    close(kinp)
    close(klst) 
  end subroutine ANEOS_Shutdown
end module aneos_access

