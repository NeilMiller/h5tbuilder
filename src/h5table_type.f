!! This module defines the h5 table type

module h5table_type
  !! After this table is loaded, the data should NOT be modified
  !! No temporary variables within this structure.  Those should probably
  !! be located in the mixeos_accessor structure
  type h5tbl
     character (len=256) :: name, datafilename
     integer :: imax, jmax     ! sizes of the arrays     
     real(8) :: minlnRho, maxlnRho, minlnT, maxlnT, dlnRho, dlnT
     
     !! let the dRho_v(i) = Rho_v(i+1) - Rho_v(i)
     !! Use the lnRho_v(i) as a hash
     !! Interpolation however occurs using the biquintic method which is linear
     real(8), dimension(:), pointer :: Rho_v, T_v, lnRho_v, lnT_v, &
          dRho_v, dT_v, dRho2_v, dT2_v, dRho3_v, dT3_v, &
          dRho_i, dRho2_i, dRho3_i, dT_i, dT2_i, dT3_i
     
     !!  (9, imax, jmax) 
     real(8), dimension(:,:,:), pointer :: Fij
     
     !! minlogRho = minlogRhoAlpha * logT + minlogRhoBeta
     !! maxlogRho = minlogRhoAlpha * logT + minlogRhoBeta
     real(8) :: minlogRhoAlpha = 0., minlogRhoBeta = -5., maxlogRhoAlpha = 0., maxlogRhoBeta = 5.
     
     logical :: IsSetup = .false. !! Has the table's members been allocated?
  end type h5tbl
end module h5table_type
