module mixeos_def
  implicit none

  integer, parameter :: i_P = 1
  integer, parameter :: i_E = 2
  integer, parameter :: i_S = 3

  integer, parameter :: i_val = 1

  integer, parameter :: i_dRho = 2
  integer, parameter :: i_dlnRho = 2
  integer, parameter :: i_dlnP = 2
  integer, parameter :: i_dX = 2
  integer, parameter :: i_dZ = 2
  
  integer, parameter :: i_dT = 3
  integer, parameter :: i_dlnT = 3
  integer, parameter :: i_dY = 3

  integer, parameter :: i_dRho2 = 4
  integer, parameter :: i_dlnRho2 = 4
  integer, parameter :: i_dlnP2 = 4
  integer, parameter :: i_dX2 = 4
  integer, parameter :: i_dZ2 = 4

  integer, parameter :: i_dT2 = 5
  integer, parameter :: i_dlnT2 = 5
  integer, parameter :: i_dY2 = 5

  integer, parameter :: i_dRhodT = 6
  integer, parameter :: i_dlnRhodlnT = 6
  integer, parameter :: i_dlnPdlnT = 6
  integer, parameter :: i_dXdY = 6
  integer, parameter :: i_dZdY = 6

  integer, parameter :: i_dRho3 = 7
  integer, parameter :: i_dRho2dT = 8
  integer, parameter :: i_dRhodT2 = 9
  integer, parameter :: i_dT3 = 10
  
  !! size of derivative vectors : for allocation
  integer, parameter :: num_derivs = 6
  integer, parameter :: num_derivs3 = 10
  
  integer, parameter :: NUM_METALS = 5
  double precision, parameter :: minRho = 1e-10
  double precision, parameter :: maxRho = 1e5
  double precision, parameter :: minT = 1e1
  double precision, parameter :: maxT = 1e8
  double precision, parameter :: minlog10Rho = log10(minRho)
  double precision, parameter :: maxlog10Rho = log10(maxRho)
  double precision, parameter :: minlog10T = log10(minT)
  double precision, parameter :: maxlog10T = log10(maxT)

  integer, parameter :: MIXEOSERR_MEM = -32  


  !! Mixeos access.  Often when we call a function, we put the temporary variables here.
  !!  such that they don't have to be constantly allocated and deallocated.
  !!  Since you may want to access the eos with multiple processes, each of those processes
  !!  should hold a unique access structure.  All of the access structures are stored in
  !!  a public variable array below.
  type mixeos_access
     
     !! Make these static if possible so that we don't have to worry about memory allocation
!     double precision, target :: Pvect(num_derives, NUM_METALS), &
!                                 Svect(num_derives, NUM_METALS), &
!                                 Evect(num_derives, NUM_METALS), &
!                                 lnPvect(num_derives, NUM_METALS), &
!                                 lnSvect(num_derives, NUM_METALS), &
!                                 lnEvect(num_derives, NUM_METALS), &
!                                 lnRhovect_PT(num_derivs, NUM_METALS+1), & 
!                                 lnSvect_PT(num_derivs, NUM_METALS+1), &
!                                 lnEvect_PT(num_derivs, NUM_METALS+1)
     integer :: handle = 0
     integer :: eos_handle = 0
  end type mixeos_access
  
  !! Adjust this if you have more than 8 processes
  !! Make sure that the eos module is willing to also give as many handles as we are looking for here
  integer, parameter :: MIXREGSIZE = 8  
  type (mixeos_access), pointer, dimension(:) :: mix_register
  integer, parameter :: ERROR_OUTOFBOUND = -16
  integer, parameter :: ERROR_ACCESS = -17
  integer, parameter :: ERROR_ARGS = -18
  
  
  
  double precision, parameter :: mixeos_epsilon = 1d-8
  integer, parameter :: mixeos_getRhoT_maxiter = 100
  double precision, parameter :: lnPmin = 15.
  double precision, parameter :: lnPmax = 35.

end module mixeos_def

