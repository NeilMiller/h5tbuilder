! ***********************************************************************
!
!   Copyright (C) 2010  Bill Paxton, Neil Miller
!
!   This file is part of MESA.
!
!   MESA is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Library Public License as published
!   by the Free Software Foundation; either version 2 of the License, or
!   (at your option) any later version.
!
!   MESA is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU Library General Public License for more details.
!
!   You should have received a copy of the GNU Library General Public License
!   along with this software; if not, write to the Free Software
!   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
!
! ***********************************************************************

!! h5table module
!!  This is the helmholtz free energy tabular / interpolation routines
!!   for a specific substance such as water.  The EOS should be generated
!!   from other software such as ANEOS and put into a format that can
!!   be read here.  
!!
!!  provides: 
!!    type: h5tbl
!!    subroutine: setup_h5tbl(tbl, Xsize, Ysize, filename, name, ierr)
!!       - allocates memory and reads table from filename
!!    subroutine: free_h5tbl(tbl)
!!       - deallocate memory that was allocated in setup_h5tbl
!!    (Note that setup_h5tbl expects tbl to be allocated and free_h5tbl doesn't
!!     deallocate the tbl pointer itself.  This should be performed higher up)
!!    subroutine: h5_get_val(tbl, Rho, T, Pvect, Evect, Svect, ierr)
!!       - get the value using the biquintic interpolation code
!!    subroutine: h5_get_Rho(tbl, P, T, Rho, Pvect, Evect, Svect, ierr)
!!       - (P,T) -> Rho.  Required for additive volume
!!
!!  Posible improvements:
!!    Binary data file
!!
!!  Notes. 
!!   There are a few supporting modules
!!   h5table_params - define parameters only used in h5table
!!   h5table_type   - define the table type used in h5table
!!   h5table_register - effectively I made these into private routines 
!!        by putting into a seperate module
module h5table
  use mixeos_def  !! Parameters, global to mixeos 
  use h5table_type   !! Define the h5tbl type
  implicit none

  private DeltalnP_T  
  contains

    !! setup_h5tbl performs two functions
    !!  (1) allocate members of tbl pointer
    !!  (2) load data from filename
    subroutine setup_h5tbl(tbl, Xsize, Ysize, &
         filename, name, ierr)
      use h5table_params !! Parameters, only for h5table
      implicit none

      type (h5tbl), pointer, intent(in) :: tbl  !! This should already be allocated
      character (len=256), intent(in) :: filename, name
      integer, intent(in) :: Xsize, Ysize
      integer, intent(out) :: ierr ! 0 means OK

      write(*,*) "Setting up table ", name
      
      ierr = 0

      if(.not. associated(tbl)) then
         write(*,*) "You need to allocated the memory for tbl before passing to setup_h5tbl"
         stop
      endif

      tbl%imax = Xsize
      tbl%jmax = Ysize
      tbl%datafilename = filename
      tbl%name = name
      
      call allocate_h5tbl_members(tbl, Xsize, Ysize, ierr)
      call read_h5tbl(tbl, Xsize, Ysize, filename, ierr)
      call check_h5tbl_spacing(tbl, Xsize, Ysize, ierr)
      
      !! IF we made it down to here, I guess we probably haven't had any problems
      tbl%IsSetup = .true.

    end subroutine setup_h5tbl
    
    !! I wrote this routine mainly so that I could setup the table in a program without
    !! needing to write and read from a file.
    subroutine setup_h5tbl_mem(tbl, Xsize, Ysize, Xvect, Yvect, Fij, name, ierr)
      use h5table_params
      implicit none
      
      type (h5tbl), pointer, intent(in) :: tbl  !! This should already be allocated
      integer, intent(in) :: Xsize, Ysize
      real(8), pointer, dimension(:) :: Xvect, Yvect
      real(8), pointer, dimension(:,:,:), intent(in) :: Fij
      character (len=256) :: name
      integer, intent(out) :: ierr

      tbl%imax = Xsize
      tbl%jmax = Ysize
      tbl%datafilename = ""
      tbl%name = name
      
      call allocate_h5tbl_members(tbl, Xsize, Ysize, ierr)
      tbl%Rho_v = Xvect
      tbl%T_v = Yvect
      tbl%lnRho_v = log(Xvect)
      tbl%lnT_v = log(Yvect)
      tbl%Fij = Fij !! Assume that the memory has been formatted correctly
      call check_h5tbl_spacing(tbl, Xsize, Ysize, ierr)

      tbl%IsSetup = .true.
    end subroutine setup_h5tbl_mem

    
    subroutine allocate_h5tbl_members(tbl, Xsize, Ysize, ierr)
      type (h5tbl), pointer, intent(in) :: tbl
      integer, intent(in) :: Xsize, Ysize
      integer, intent(out) :: ierr
      
      write(*,*) "Allocating table internal memory"
      call alloc_1d_array(tbl%Rho_v, Xsize)
      call alloc_1d_array(tbl%lnRho_v, Xsize)
      call alloc_1d_array(tbl%dRho_v, Xsize)
      call alloc_1d_array(tbl%dRho2_v, Xsize)
      call alloc_1d_array(tbl%dRho3_v, Xsize)
      call alloc_1d_array(tbl%dRho_i, Xsize)
      call alloc_1d_array(tbl%dRho2_i, Xsize)
      call alloc_1d_array(tbl%dRho3_i, Xsize)
      call alloc_1d_array(tbl%T_v, Ysize)      
      call alloc_1d_array(tbl%lnT_v, Ysize)
      call alloc_1d_array(tbl%dT_v, Ysize)
      call alloc_1d_array(tbl%dT2_v, Ysize)
      call alloc_1d_array(tbl%dT3_v, Ysize)
      call alloc_1d_array(tbl%dT_i, Ysize)
      call alloc_1d_array(tbl%dT2_i, Ysize)
      call alloc_1d_array(tbl%dT3_i, Ysize)
      allocate(tbl%Fij(9, Xsize, Ysize), stat=ierr)
      if(ierr /= 0) then
         write(*,*) 'failure to allocate memory for storing table'
      end if
    contains      
      subroutine alloc_1d_array(ptr,sz)
        real(8), dimension(:), pointer :: ptr
        integer, intent(in) :: sz        
        allocate(ptr(sz),stat=ierr)
        if (ierr /= 0) then 
           write(*,*) 'failure in attempt to allocate Aneos_Table storage'
        endif
      end subroutine alloc_1d_array

    end subroutine allocate_h5tbl_members

    !! This is the block of code that reads the table from the file
    subroutine read_h5tbl(tbl, Xsize, Ysize, filename, ierr)
      use h5table_params
      implicit none

      
      type (h5tbl), pointer, intent(in) :: tbl
      integer, intent(in) :: Xsize, Ysize
      character (len=256), intent(in) :: filename
      integer, intent(out) :: ierr
      
      integer :: i, j, fid, ios
      double precision :: RhoVal, Tval, lnRhoVal, lnTval, FvectIn(9)
      
      ios = 0
      ierr = 0
      fid = 20
      
      write(*,*) "Opening file - ", trim(filename)
      open(unit=fid, file=trim(filename), action='read', status ='old', form="unformatted", iostat=ios)
      
      do i=1,Xsize
         do j=1,Ysize
            read(fid) lnRhoVal, lnTval, FvectIn
            tbl%Fij(1:9,i,j) = FvectIn
            
            RhoVal = exp(lnRhoVal)
            Tval = exp(lnTVal)
            if(isnan(RhoVal) .or. &
                 isnan(Tval) .or. &
                 isnan(tbl%Fij(TBL_VAL,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DX,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DY,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DX2,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DY2,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DXDY,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DX2DY,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DXDY2,i,j)) .or. &
                 isnan(tbl%Fij(TBL_DX2DY2,i,j))) then
               write(*,*) "NaN found - this is unnacceptable"  
               write(*,*) "Make sure that the table has NO NaN values"
               write(*,*) "  in it before you pass it to the h5table"
               stop
            endif
            
            tbl%Rho_v(i) = RhoVal
            tbl%T_v(j) = Tval
            tbl%lnRho_v(i) = lnRhoVal
            tbl%lnT_v(j) = lnTval
         enddo
      enddo
      
      close(unit=fid)

    end subroutine read_h5tbl

    subroutine check_h5tbl_spacing(tbl, Xsize, Ysize, ierr)
      type (h5tbl), pointer, intent(in) :: tbl
      integer, intent(in) :: Xsize, Ysize
      integer, intent(out) :: ierr

      real(8), parameter :: epsilon = 1e-5
      integer :: i, j

      ierr = 0

      tbl%minlnRho = minval(tbl%lnRho_v)
      tbl%maxlnRho = maxval(tbl%lnRho_v)
      tbl%minlnT = minval(tbl%lnT_v)
      tbl%maxlnT = maxval(tbl%lnT_v)

      write(*,*) "lnRho range: ", tbl%minlnRho, tbl%maxlnRho
      write(*,*) "lnT range: ", tbl%minlnT, tbl%maxlnT
      tbl%dlnRho = (tbl%maxlnRho - tbl%minlnRho) / (Xsize - 1)
      tbl%dlnT = (tbl%maxlnT - tbl%minlnT) / (Ysize - 1)

      !!! Set up the difference vectors and their inverses
      do i=1,Xsize-1
         tbl%dRho_v(i) = tbl%Rho_v(i+1) - tbl%Rho_v(i)
         tbl%dRho2_v(i) = tbl%dRho_v(i) * tbl%dRho_v(i)
         tbl%dRho3_v(i) = tbl%dRho_v(i) * tbl%dRho2_v(i)
         tbl%dRho_i(i) = 1d0 / tbl%dRho_v(i)
         tbl%dRho2_i(i) = tbl%dRho_i(i) * tbl%dRho_i(i)
         tbl%dRho3_i(i) = tbl%dRho_i(i) * tbl%dRho2_i(i)
      enddo      
      do j=1,Ysize-1
         tbl%dT_v(j) = tbl%T_v(j+1) - tbl%T_v(j)
         tbl%dT2_v(j) = tbl%dT_v(j) * tbl%dT_v(j)
         tbl%dT3_v(j) = tbl%dT_v(j) * tbl%dT2_v(j)
         tbl%dT_i(j) = 1d0 / tbl%dT_v(j)
         tbl%dT2_i(j) = tbl%dT_i(j) * tbl%dT_i(j)
         tbl%dT3_i(j) = tbl%dT_i(j) * tbl%dT2_i(j)
      enddo

      !! Check spacing of Rho_v to make sure that it is logrithmically
      !! spaced with seperation dlnRho.  Note a common reason these might be mispaced
      !! would be if the user provides an inccorect Xsize or Ysize
      do i=2,Xsize  
         if(abs(log(tbl%Rho_v(i) / tbl%Rho_v(i-1)) - tbl%dlnRho) .gt. epsilon) then
            write(*,*) "There is something wrong with the grid or the dimensions of the table"
            write(*,*) "i : ", i
            write(*,*) "Rho_v(i) : ", tbl%Rho_v(i)
            write(*,*) "Rho_v(i-1) : ", tbl%Rho_v(i-1)
            write(*,*) "dlnRho : ", tbl%dlnRho
            stop
         endif
      enddo

      !! Check spacing of T_v that it is logrithmically spaced with seperation dlnT
      do j=2,Ysize
         if(abs(log(tbl%T_v(j) / tbl%T_v(j-1)) - tbl%dlnT) .gt. epsilon) then 
            write(*,*) " There is something wrong with the grid or the dimensions of the table"
            write(*,*) "j : ", j
            write(*,*) "T_v(j) : ", tbl%T_v(j)
            write(*,*) "T_v(j-1) : ", tbl%T_v(j-1)
            write(*,*) "dlnT : ", tbl%dlnT
            stop
         endif
      enddo

    end subroutine check_h5tbl_spacing

    subroutine slice_h5table(tbl, minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta)
      type (h5tbl), pointer, intent(in) :: tbl
      real(8), intent(in) :: minlogRhoAlpha, minlogRhoBeta, maxlogRhoAlpha, maxlogRhoBeta
      
      tbl%minlogRhoAlpha = minlogRhoAlpha
      tbl%minlogRhoBeta = minlogRhoBeta
      tbl%maxlogRhoAlpha = maxlogRhoAlpha
      tbl%maxlogRhoBeta = maxlogRhoBeta
      
    end subroutine slice_h5table

    subroutine free_h5tbl(tbl)
      use h5table_params !! Parameters, only for h5table
      implicit none
      type (h5tbl), pointer, intent(in) :: tbl
      
      call free_1D(tbl%Rho_v)
      call free_1D(tbl%lnRho_v)
      call free_1D(tbl%dRho_v)
      call free_1D(tbl%dRho2_v)
      call free_1D(tbl%dRho3_v)
      call free_1D(tbl%dRho_i)
      call free_1D(tbl%dRho2_i)
      call free_1D(tbl%dRho3_i)
      
      call free_1D(tbl%T_v)
      call free_1D(tbl%lnT_v)
      call free_1D(tbl%dT_v)
      call free_1D(tbl%dT2_v)
      call free_1D(tbl%dT3_v)
      call free_1D(tbl%dT_i)      
      call free_1D(tbl%dT2_i)
      call free_1D(tbl%dT3_i)
      if(associated(tbl%Fij)) then
         deallocate(tbl%Fij)
         nullify(tbl%Fij)
      endif
    contains
      subroutine free_1D(vector)
        real(8), pointer, dimension(:) :: vector
        if(associated(vector)) then 
           deallocate(vector)
           nullify(vector)
        endif
      end subroutine free_1D
    end subroutine free_h5tbl

    subroutine h5_get_val(tbl, Rho, T, Pvect, Evect, Svect, ierr)
      use mixeos_def
      use h5table_params
      implicit none
      
      type (h5tbl), pointer, intent(in) :: tbl
      real(8), intent(in) :: Rho, T
      real(8), intent(out), pointer, dimension(:) :: Pvect, Evect, Svect
      integer, intent(out) :: ierr

      real(8), dimension(num_derivs3) :: Fvect
      double precision :: RhoSQ

      call h5_get_free(tbl, Rho, T, Fvect, ierr)

      RhoSQ = Rho*Rho
      
      Pvect(i_val) = RhoSQ * Fvect(i_dRho)
      Pvect(i_dRho) = RhoSQ * Fvect(i_dRho2) + 2d0 * Rho * Fvect(i_dRho)
      Pvect(i_dT) = RhoSQ * Fvect(i_dRhodT)
      Pvect(i_dRho2) = RhoSQ*Fvect(i_dRho3) + 4d0*Rho*Fvect(i_dRho2) + 2d0*Fvect(i_dRho)
      Pvect(i_dT2) = RhoSQ * Fvect(i_dRhodT2)
      Pvect(i_dRhodT) = RhoSQ * Fvect(i_dRho2dT) + 2d0 * Rho * Fvect(i_dRhodT)
      
      Svect(i_val)    = -Fvect(i_dT)
      Svect(i_dRho)   = -Fvect(i_dRhodT)
      Svect(i_dT)     = -Fvect(i_dT2)
      Svect(i_dRho2)  = -Fvect(i_dRho2dT)
      Svect(i_dT2)    = -Fvect(i_dT3)
      Svect(i_dRhodT) = -Fvect(i_dRhodT2)
      
      Evect(i_val)    = Fvect(i_val) - T * Fvect(i_dT)
      Evect(i_dRho)   = Fvect(i_dRho) - T * Fvect(i_dRhodT)
      Evect(i_dT)     = -T * Fvect(i_dT2)
      Evect(i_dRho2)  = Fvect(i_dRho2) - T * Fvect(i_dRho2dT)
      Evect(i_dT2)    = -Fvect(i_dT2) - T * Fvect(i_dT3)
      Evect(i_dRhodT) = - T * Fvect(i_dRhodT2)
         
    end subroutine h5_get_val
    
    !! get_val : takes the (Rho, T) cordinates
    !!   (1) lookup the (Rho_index, T_index) using the logrithmic hash
    !!   (2) Use the Timmes biquintic iterpolation code - inline here
    !! TODO: Check that his code actually works
    subroutine h5_get_free(tbl, Rho, T, Fvect, ierr)
      use mixeos_def
      use h5table_params !! Parameters, only for h5table
      implicit none
      
      type (h5tbl), pointer, intent(in) :: tbl
      real(8), intent(in) :: Rho, T
      real(8), intent(out), dimension(num_derivs3) :: Fvect
      integer, intent(out) :: ierr
      
      integer :: dI, tI
      real(8) :: lnRho, lnT, RhoSQ, log10Rho, log10T
      
      !! Define Variables for helm Holtz interpolation routine
      real(8) :: fi(36) 
!      real(8) :: z
      real(8) :: xt, xd, mxt, mxd!, &
!           w0t, w1t, w2t, w0mt, w1mt, w2mt, &
!           w0d, w1d, w2d, w0md, w1md, w2md
           
      real(8) :: si0t, si1t, si2t, si0mt, si1mt, si2mt, &
           si0d, si1d, si2d, si0md, si1md, si2md, &
           dsi0t, dsi1t, dsi2t, dsi0mt, dsi1mt, dsi2mt, &
           dsi0d, dsi1d, dsi2d, dsi0md, dsi1md, dsi2md, &
           ddsi0t, ddsi1t, ddsi2t, ddsi0mt, ddsi1mt, ddsi2mt, &
           ddsi0d, ddsi1d, ddsi2d, ddsi0md, ddsi1md, ddsi2md, &
           dddsi0t, dddsi1t, dddsi2t, dddsi0mt, dddsi1mt, dddsi2mt, &
           dddsi0d, dddsi1d, dddsi2d, dddsi0md, dddsi1md, dddsi2md

      real(8) :: free, df_d, df_t, df_dd, df_tt, df_dt, &
           df_ddd, df_ddt, df_dtt, df_ttt

      !! Define the types for the statement functions below
!      real(8) :: psi0, dpsi0, ddpsi0, dddpsi0,  &
!           psi1, dpsi1, ddpsi1, dddpsi1, &
!           psi2, dpsi2, ddpsi2, dddpsi2!, h5 

      !! The following statement functions are used in helm_interp.dek      
      !..quintic hermite polynomial statement functions
      !..psi0 and its derivatives
!      psi0(z)    = z**3 * ( z * (-6.0d0*z + 15.0d0) - 10.0d0) + 1.0d0
!      dpsi0(z)   = z**2 * ( z * (-30.0d0*z + 60.0d0) - 30.0d0)
!      ddpsi0(z)  = z* ( z*( -120.0d0*z + 180.0d0) - 60.0d0)
!      dddpsi0(z) = z*( -360.0d0*z + 360.0d0) - 60.0d0


      !..psi1 and its derivatives
!      psi1(z)    = z* (z**2 * ( z * (-3.0d0*z + 8.0d0) - 6.0d0) + 1.0d0)
!      dpsi1(z)   = z*z * ( z * (-15.0d0*z + 32.0d0) - 18.0d0) +1.0d0
!      ddpsi1(z)  = z * (z * (-60.0d0*z + 96.0d0) -36.0d0)
!      dddpsi1(z) = z * (-180.0d0*z + 192.0d0) - 36.0d0


      !..psi2  and its derivatives
!      psi2(z)    = 0.5d0*z*z*( z* ( z * (-z + 3.0d0) - 3.0d0) + 1.0d0)
!      dpsi2(z)   = 0.5d0*z*( z*(z*(-5.0d0*z + 12.0d0) - 9.0d0) + 2.0d0)
!      ddpsi2(z)  = 0.5d0*(z*( z * (-20.0d0*z + 36.0d0) - 18.0d0) +2.0d0)
!      dddpsi2(z) = 0.5d0*(z * (-60.0d0*z + 72.0d0) - 18.0d0)



      !! BE CAREFUL - I think this was running over the line limit, still compiling and producing trash
      !..biquintic hermite polynomial statement function
!      h5(w0t, w1t, w2t, w0mt, w1mt, w2mt, w0d, w1d, w2d, w0md, w1md, w2md)= fi(1)  *w0d*w0t   + fi(2)  *w0md*w0t + fi(3)  *w0d*w0mt  + fi(4)  *w0md*w0mt + fi(5)  *w0d*w1t   + fi(6)  *w0md*w1t  + fi(7)  *w0d*w1mt  + fi(8)  *w0md*w1mt + fi(9)  *w0d*w2t   + fi(10) *w0md*w2t  + fi(11) *w0d*w2mt  + fi(12) *w0md*w2mt + fi(13) *w1d*w0t   + fi(14) *w1md*w0t + fi(15) *w1d*w0mt  + fi(16) *w1md*w0mt + fi(17) *w2d*w0t   + fi(18) *w2md*w0t + fi(19) *w2d*w0mt  + fi(20) *w2md*w0mt + fi(21) *w1d*w1t   + fi(22) *w1md*w1t + fi(23) *w1d*w1mt  + fi(24) *w1md*w1mt + fi(25) *w2d*w1t   + fi(26) *w2md*w1t + fi(27) *w2d*w1mt  + fi(28) *w2md*w1mt + fi(29) *w1d*w2t   + fi(30) *w1md*w2t + fi(31) *w1d*w2mt  + fi(32) *w1md*w2mt + fi(33) *w2d*w2t   + fi(34) *w2md*w2t + fi(35) *w2d*w2mt  + fi(36) *w2md*w2mt 


      !! First try to make sure the function is being used appropriatedly
      if(.not.associated(tbl)) then
         write(*,*) "The table pointer is not associated!!!"
         stop
      endif

      if(.not.tbl%IsSetup) then
         write(*,*), "You need to call the procedure setup_h5tbl"
         write(*,*), " - this will allocate the member function memory"
         stop
      endif

      lnRho = log(Rho)
      log10Rho = log10(Rho)
      lnT = log(T)
      log10T = log10(T)
      RhoSQ = Rho*Rho

      ierr = 0
      if((lnRho.ge.tbl%maxlnRho) .or. &
           (log10Rho .gt. (tbl%maxlogRhoAlpha*log10T + tbl%maxlogRhoBeta))) then
         ierr = H5TBLERR_MAXRHO         
      elseif(lnRho.le.tbl%minlnRho .or. &
           (log10Rho .lt. (tbl%minlogRhoAlpha*log10T + tbl%minlogRhoBeta))) then
         ierr = H5TBLERR_MINRHO
      elseif(lnT.ge.tbl%maxlnT) then
         ierr = H5TBLERR_MAXTEMPERATURE
      elseif(lnT.le.tbl%minlnT) then
         ierr = H5TBLERR_MINTEMPERATURE
      endif
      if(ierr /= 0) then
         if(h5dbg) then
            write(*,*) "Data table out of bounds ", tbl%name
            write(*,*) "lnRho range: ", tbl%minlnRho, tbl%maxlnRho
            write(*,*) "lnT range: ", tbl%minlnT, tbl%maxlnT
            write(*,*) "lnRho value: ", lnRho
            write(*,*) "lnT value: ", lnT
         endif
         Fvect(:) = 0d0
         return
      endif

      
      dI = get_Rho_index(lnRho)
      tI = get_T_index(lnT)
      if(h5dbg) then
         write(*,*) "Rho range: ", tbl%Rho_v(dI), tbl%Rho_v(dI+1)
         write(*,*) "T range: ", tbl%T_v(tI), tbl%T_v(tI+1)
         write(*,*) "dRho_v: ", tbl%dRho_v(dI)
         write(*,*) "dRho2_v: ", tbl%dRho2_v(dI)
         write(*,*) "dRho_i: ", tbl%dRho_i(dI)
         write(*,*) "dRho2_i: ", tbl%dRho2_i(dI)
         write(*,*) "dRho3_i: ", tbl%dRho3_i(dI)
      endif
         
      include 'helm_interp.dek'
      !! Use Timmes Code HERE
    contains
      
      !! Hash functions.  The grid must actually be logrithmically spaced
      !!  we also check that this is in fact the case when the grid is
      !!  begin loaded.  
      integer function get_Rho_index(lnRho)
        implicit none
        real(8) :: lnRho
        get_Rho_index = floor((lnRho - tbl%minlnRho) / tbl%dlnRho) + 1 !! Start at index 1
      end function get_Rho_index
      
      integer function get_T_index(lnT)
        implicit none
        real(8) :: lnT              
        get_T_index = floor((lnT - tbl%minlnT) / tbl%dlnT) + 1 !! start at index 1
      end function get_T_index

      real(8) function psi0(z)
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2,z3,z4,z5        
        z2 = z*z
        z3 = z2*z
        z4 = z3*z
        z5 = z4*z
        psi0 = -6d0*z5 + 15d0 * z4 - 10d0 * z3 + 1d0
      end function psi0

      real(8) function dpsi0(z)  
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2,z3,z4    
        z2 = z*z
        z3 = z2*z
        z4 = z3*z
        dpsi0 = -30.d0 * z4 + 60.d0 * z3 - 30.d0*z2
      end function dpsi0

      real(8) function ddpsi0(z) 
        implicit none
        real(8), intent(in) :: z 
        real(8) :: z2,z3     
        z2 = z*z
        z3 = z2*z
        ddpsi0 = -120.0d0*z3 + 180.d0 * z2 - 60.d0 * z
      end function ddpsi0

      real(8) function dddpsi0(z)
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2      
        z2 = z*z

        dddpsi0 = -360.d0 * z2 + 360.d0 * z - 60.0d0
      end function dddpsi0

      !..psi1 and its derivatives
      real(8) function psi1(z)
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2,z3,z4,z5        
        z2 = z*z
        z3 = z2*z
        z4 = z3*z
        z5 = z4*z

        psi1 = -3d0 * z5 + 8.d0*z4 - 6.d0 * z3 + z
      end function psi1
    
      real(8) function dpsi1(z) 
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2,z3,z4  
        z2 = z*z
        z3 = z2*z
        z4 = z3*z

        dpsi1 = -15.d0*z4 + 32.d0 * z3 - 18.d0 * z2 + 1.d0
      end function dpsi1
      
      real(8) function ddpsi1(z) 
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2,z3       
        z2 = z*z
        z3 = z2*z

        ddpsi1 = -60.d0*z3 + 96.d0*z2 - 36.d0*z
      end function ddpsi1

      real(8) function dddpsi1(z)
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2
        z2 = z*z

        dddpsi1 = -180.d0 * z2 + 192.d0*z - 36.d0
      end function dddpsi1
        
      !..psi2  and its derivatives
      real(8) function psi2(z)
        implicit none
        real(8), intent(in) :: z 
        real(8) :: z2,z3,z4,z5        
        z2 = z*z
        z3 = z2*z
        z4 = z3*z
        z5 = z4*z
        
        psi2 = 0.5d0*(-z5 + 3.d0*z4 - 3.d0*z3 + z2)
      end function psi2

      real(8) function dpsi2(z)
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2,z3,z4
        z2 = z*z
        z3 = z2*z
        z4 = z3*z
        
        dpsi2 = 0.5d0*(-5.d0*z4 + 12.d0 * z3 - 9.d0*z2 + 2.d0*z)
      end function dpsi2

      real(8) function ddpsi2(z)
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2,z3
        z2 = z*z
        z3 = z2*z

        ddpsi2 = 0.5d0*(-20.d0*z3 + 36.d0*z2 - 18.d0*z + 2.0d0)
      end function ddpsi2
      
      real(8) function dddpsi2(z) 
        implicit none
        real(8), intent(in) :: z
        real(8) :: z2
        z2 = z*z

        dddpsi2 = 0.5d0*(-60.d0*z2 + 72.d0*z - 18.d0)
      end function dddpsi2
      
      real(8) function h5(w0t, w1t, w2t, w0mt, w1mt, w2mt, &
                          w0d, w1d, w2d, w0md, w1md, w2md)
        implicit none
        real(8), intent(in) :: w0t, w1t, w2t, w0mt, w1mt, w2mt, &
                               w0d, w1d, w2d, w0md, w1md, w2md

        h5 = (fi(1)  *w0d*w0t   + fi(2)  *w0md*w0t &
             + fi(3)  *w0d*w0mt  + fi(4)  *w0md*w0mt) &
            + (fi(5)  *w0d*w1t   + fi(6)  *w0md*w1t  &
             + fi(7)  *w0d*w1mt  + fi(8)  *w0md*w1mt) &
            + (fi(9)  *w0d*w2t   + fi(10) *w0md*w2t  &
             + fi(11) *w0d*w2mt  + fi(12) *w0md*w2mt) &
            + (fi(13) *w1d*w0t   + fi(14) *w1md*w0t  &
            + fi(15) *w1d*w0mt  + fi(16) *w1md*w0mt) &
            +(fi(17) *w2d*w0t   + fi(18) *w2md*w0t &
            + fi(19) *w2d*w0mt  + fi(20) *w2md*w0mt) &
           + (fi(21) *w1d*w1t   + fi(22) *w1md*w1t &
            + fi(23) *w1d*w1mt  + fi(24) *w1md*w1mt) &
           + (fi(25) *w2d*w1t   + fi(26) *w2md*w1t &
            + fi(27) *w2d*w1mt  + fi(28) *w2md*w1mt) &
           + (fi(29) *w1d*w2t   + fi(30) *w1md*w2t &
            + fi(31) *w1d*w2mt  + fi(32) *w1md*w2mt) &
           + (fi(33) *w2d*w2t   + fi(34) *w2md*w2t &
            + fi(35) *w2d*w2mt  + fi(36) *w2md*w2mt)
      end function h5
    end subroutine h5_get_free
    
    !! h5table only requires (P,T) -> Rho
    !! You ABSOLUTELEY MUST CHECK IF IERR IS NOT EQUAL TO ZERO
    !! Pvect(TBL_VAL) may contain the maximum or minimum pressure as a hint to the to user if there is an error
    subroutine h5_get_Rho(tbl, P, T, Rho, Pvect, Evect, Svect, ierr)
      use num_lib
      use h5table_params   !! Parameters, only for h5table
      use h5table_register, only: set_handle, free_handle !! table register access procedures. 
      implicit none
      type (h5tbl), pointer, intent(in) :: tbl
      real(8), intent(in) :: P, T
      real(8), intent(out) :: Rho
      real(8), intent(out), pointer, dimension(:) :: Pvect, Evect, Svect
      real(8) :: P1, P2, Rho1, Rho2, RhoGuess, lnRho, &
           log10T, lnT, minlogRhoCut, maxlogRhoCut
      integer, intent(out) :: ierr

      integer :: tbl_handle, itermax
      integer, parameter :: lrpar = 20, lipar = 1
      real(8), target :: rpar(lrpar)
      integer :: ipar(lipar)
      real(8) :: eps
      
      call set_handle(tbl, tbl_handle, ierr)  !! Think about this more
      
      rpar(1) = T
      rpar(2) = log(P)
      ipar(1) = tbl_handle
      
      log10T = log10(T)
      lnT = log(T)

      Rho1 = exp(tbl%minlnRho) * 1.01
      Rho2 = exp(tbl%maxlnRho) * 0.99
!      write(*,*) Rho1, Rho2
      minlogRhoCut = tbl%minlogRhoAlpha * log10T + tbl%minlogRhoBeta
      maxlogRhoCut = tbl%maxlogRhoAlpha * log10T + tbl%maxlogRhoBeta
      if(log10(Rho1) .lt. minlogRhoCut) then
         Rho1 = 1d1**minlogRhoCut * 1.01
      endif
      if(log10(Rho2) .gt. maxlogRhoCut) then
         Rho2 = 1d1**maxlogRhoCut * 0.99
      endif

      if(lnT .lt. tbl%minlnT) then
         ierr = H5TBLERR_MINTEMPERATURE
         call free_handle(tbl_handle, ierr)
         return
      endif

      if(lnT .gt. tbl%maxlnT) then
         ierr = H5TBLERR_MAXTEMPERATURE
         call free_handle(tbl_handle, ierr)
         return
      endif
!      write(*,*) "PT Diagnostics" 
!      write(*,*) log10T
!      write(*,*) tbl%minlogRhoAlpha, tbl%minlogRhoBeta
!      write(*,*) tbl%maxlogRhoAlpha, tbl%maxlogRhoBeta
!      write(*,*) minlogRhoCut, maxlogRhoCut

!      write(*,*) "Rho range: ", Rho1, Rho2

      if(Rho1 .gt. Rho2) then
         ierr = H5TBLERR_NOPRESSURESOL
!         write(*,*) "No valid density range"
         call free_handle(tbl_handle, ierr)
         return
      endif
      
      call h5_get_val(tbl, Rho1, T, Pvect, Evect, Svect, ierr)
      P1 = Pvect(TBL_VAL)
      if(ierr .ne. 0) then
!         write(*,*) "PT lookup failed to get P1"
         call free_handle(tbl_handle, ierr)
         return
      endif
      call h5_get_val(tbl, Rho2, T, Pvect, Evect, Svect, ierr)
      P2 = Pvect(TBL_VAL)
      if(ierr .ne. 0) then
!         write(*,*) "PT lookup failed to get P2"
         call free_handle(tbl_handle, ierr)
         return
      end if
!      write(*,*) "h5tbl bounds"
!      write(*,*) "Rho1 = ", Rho1, " <=> log10(P1) = ", log10(P1)
!      write(*,*) "Rho2 = ", Rho2, " <=> log10(P2) = ", log10(P2)

      Rho = 0.
      Pvect(:) = 0.
      Evect(:) = 0.
      Svect(:) = 0.
      if(P2.lt.P) then
         call free_handle(tbl_handle, ierr)
         ierr = H5TBLERR_MAXPRESSURE
         Pvect(TBL_VAL) = P2  
!         write(*,*) "Pressure greater than max pressure ", P2, " < ", P
         return
      elseif(P.lt.P1) then
         call free_handle(tbl_handle, ierr)
         ierr = H5TBLERR_MINPRESSURE
         Pvect(TBL_VAL) = P1
!         write(*,*) "Pressure less than min pressure ", P1, " < ", P
         return
      else
         !! 2. Rootfind
         !!    a. Guess a density by assuming that lnP ~ alpha * lnRho + beta
         RhoGuess = exp(log(Rho1) + (log(P) - log(P1)) * (log(Rho1) - log(Rho2)) &
                                                       / (log(P1) - log(P2)))
         
!         write(*,*) "RhoGuess = ", RhoGuess
         eps = H5TBL_EPSILON
         itermax = H5TBL_ITERMAX
         !!    b. Execute
         lnRho = safe_root_with_initial_guess(DeltalnP_T, log(RhoGuess), &
              log(Rho1), log(Rho2), log(P1)-log(P), log(P2)-log(P), &
              itermax, eps, eps, lrpar, rpar, lipar, ipar, ierr)
         Rho = exp(lnRho)
         Pvect(1:6) = rpar(3:8)
         Evect(1:6) = rpar(9:14)
         Svect(1:6) = rpar(15:20)
         call free_handle(tbl_handle, ierr)

      endif
    end subroutine h5_get_Rho

    !! This should be in lnP or logP and logRho
    real(8) function DeltalnP_T(lnRho, dlnPdlnRho,lrpar,rpar,lipar,ipar,ierr)
      use h5table_params !! Private parameters, only for h5table
      use h5table_register
      implicit none
      real(8), intent(in) :: lnRho
      real(8), intent(out) :: dlnPdlnRho
      real(8), intent(inout), target :: rpar(lrpar)
      real(8), pointer, dimension(:) :: Pvect, Evect, Svect
      integer, intent(inout), target :: ipar(lipar)      
      integer, intent(in) :: lrpar, lipar
      integer, intent(out) :: ierr

      integer :: tbl_handle, i
      type (h5tbl), pointer :: tbl
      real(8) :: T, lnP0, lnP, Rho

      ierr = 0

      T = rpar(1)
      lnP0 = rpar(2)
      i = 2
      Pvect => rpar((i+1):(i+6)); i = i+6  
      Evect => rpar((i+1):(i+6)); i = i+6
      Svect => rpar((i+1):(i+6)); i = i+6

      tbl_handle = ipar(1)
      call get_tbl(tbl_handle, tbl, ierr)
      Rho = exp(lnRho)
      call h5_get_val(tbl, Rho, T, Pvect, Evect, Svect, ierr)
!      write(*,*) "Test: Rho = ", Rho, " <=> log10P = ", log10(Pvect(TBL_VAL))
      
      lnP = log(Pvect(TBL_VAL))
      DeltalnP_T = lnP - lnP0
      dlnPdlnRho = (Rho / Pvect(TBL_VAL)) * Pvect(TBL_DX)
    end function DeltalnP_T
    
  end module h5table
