# plot_images.rb

load "/Users/neil/research/mesa/utils/image_plot.rb"
    
class MY_data < Image_data

  attr_accessor :mP
  attr_accessor :mE
  attr_accessor :mS

  attr_accessor :dPdRho
  attr_accessor :dEdRho
  attr_accessor :dSdRho

  attr_accessor :dPdT
  attr_accessor :dEdT
  attr_accessor :dSdT

  attr_accessor :d2PdRho2
  attr_accessor :d2EdRho2
  attr_accessor :d2SdRho2

  attr_accessor :d2PdT2
  attr_accessor :d2EdT2
  attr_accessor :d2SdT2

  attr_accessor :d2PdRhodT
  attr_accessor :d2EdRhodT
  attr_accessor :d2SdRhodT
  
  def initialize(data_dir)
        
    read_image_Xs(data_dir, 'h5t_logRhovect.data', 'log10(\rho)')
    read_image_Ys(data_dir, 'h5t_logTvect.data', 'log10(T)')
    
    @mP = read_image_data(data_dir, 'P')
    @mE = read_image_data(data_dir, 'E')
    @mS = read_image_data(data_dir, 'S')

    @dPdRho = read_image_data(data_dir, 'dPdRho')
    @dEdRho = read_image_data(data_dir, 'dEdRho')
    @dSdRho = read_image_data(data_dir, 'dSdRho')

    @dPdT = read_image_data(data_dir, 'dPdT')
    @dEdT = read_image_data(data_dir, 'dEdT')
    @dSdT = read_image_data(data_dir, 'dSdT')

    @d2PdRho2 = read_image_data(data_dir, 'd2PdRho2')
    @d2EdRho2 = read_image_data(data_dir, 'd2EdRho2')
    @d2SdRho2 = read_image_data(data_dir, 'd2SdRho2')

    @d2PdT2 = read_image_data(data_dir, 'd2PdT2')
    @d2EdT2 = read_image_data(data_dir, 'd2EdT2')
    @d2SdT2 = read_image_data(data_dir, 'd2SdT2')

    @d2PdRhodT = read_image_data(data_dir, 'd2PdRhodT')
    @d2EdRhodT = read_image_data(data_dir, 'd2EdRhodT')
    @d2SdRhodT = read_image_data(data_dir, 'd2SdRhodT')
      
  end
  
end # class MY_data


class MY_plot

  include Math
  include Tioga
  include FigureConstants
  include Image_plot
  
  def initialize(data_dir)
    
    @data_dir = data_dir
    @figure_maker = FigureMaker.default
    t.def_eval_function { |str| eval(str) }
    t.save_dir = 'derivs_plots'
    
    t.def_figure('P') { plP }
    t.def_figure('E') { plE }
    t.def_figure('S') { plS }

    t.def_figure('dPdRho') { dPdRho }
    t.def_figure('dEdRho') { dEdRho }
    t.def_figure('dSdRho') { dSdRho }

    t.def_figure('dPdT') { dPdT }
    t.def_figure('dEdT') { dEdT }
    t.def_figure('dSdT') { dSdT }

    t.def_figure('d2PdRho2') { d2PdRho2 }
    t.def_figure('d2EdRho2') { d2EdRho2 }
    t.def_figure('d2SdRho2') { d2SdRho2 }
    
    t.def_figure('d2PdT2') { d2PdT2 }
    t.def_figure('d2EdT2') { d2EdT2 }
    t.def_figure('d2SdT2') { d2SdT2 }

    t.def_figure('d2PdRhodT') { d2PdRhodT }
    t.def_figure('d2EdRhodT') { d2EdRhodT }
    t.def_figure('d2SdRhodT') { d2SdRhodT }

    @image_data = MY_data.new(data_dir)
    @label_scale = 0.75
    @no_clipping = false #true        
    
      ### Load the neptune profile into the class
    @margin = 0.05
    


    @logRhoMin = -5
    @logRhoMax = 2
    @logTMin = 2.5
    @logTMax = 5.5

    @PlotProfiles = false

    @LowMassProfile1 = Dvector.read("planet_data/RhoTset01.dat")
    @LowMasslog10RhoProfile1 = @LowMassProfile1[0]
    @LowMasslog10TProfile1 = @LowMassProfile1[1]
    
    @LowMassProfile2 = Dvector.read("planet_data/RhoTset02.dat")
    @LowMasslog10RhoProfile2 = @LowMassProfile2[0]
    @LowMasslog10TProfile2 = @LowMassProfile2[1]
    
    @LowMassProfile3 = Dvector.read("planet_data/RhoTset03.dat")
    @LowMasslog10RhoProfile3 = @LowMassProfile3[0]
    @LowMasslog10TProfile3 = @LowMassProfile3[1]
    
    @HD80606Profile = Dvector.read("planet_data/RhoTset04.dat")
    @HD80606log10RhoProfile = @HD80606Profile[0]
    @HD80606log10TProfile = @HD80606Profile[1]
    
#      @NepPressureProfile = @NepProfile[2]
#      @Neplog10Rho = @NepRhoProfile.log10
#      @Neplog10T = @NepTProfile.log10
#      @NepP = @NepPressureProfile.log
      
    t.def_enter_page_function { enter_page } 
    
  end
  
  def enter_page
    t.yaxis_numeric_label_angle = -90
    t.page_setup(11*72/2,8.5*72/2)
    t.set_frame_sides(0.15,0.85,0.85,0.15) # left, right, top, bottom in page coords  
  end
    
  def clip_image
    t.fill_color = Black
    t.fill_frame
    return
  end
    
  def t
    @figure_maker
  end
  
  def d
    @image_data
  end
  
  def do_decorations(title)
  end
    
  # plot routines
  
## P

  def plotRhoTProfiles
    if(@PlotProfiles) then
      t.subplot('right_margin' => 0.05, 
                'left_margin' => @image_left_margin) do 
        @bounds = [@logRhoMin, @logRhoMax, @logTMax, @logTMin] 
        t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
        t.top_edge_visible = false
        t.right_edge_visible = false
        t.show_plot(@bounds) {
          t.line_width=1.0
          t.show_polyline(@LowMasslog10RhoProfile1, @LowMasslog10TProfile1, Blue)
          t.show_polyline(@LowMasslog10RhoProfile2, @LowMasslog10TProfile2, Red)
          t.show_polyline(@LowMasslog10RhoProfile3, @LowMasslog10TProfile3, Green)
          t.show_polyline(@HD80606log10RhoProfile, @HD80606log10TProfile, Black)
        }
      end
    end
  end

  def plP
    image_plot('d' => d, 'zs' => d.mP, 'title' => 'P ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def dPdRho
    image_plot('d' => d, 'zs' => d.dPdRho, 'title' => 'dPdRho ', 'interpolate' => false)
    plotRhoTProfiles()
  end
    
  def dPdT
    image_plot('d' => d, 'zs' => d.dPdT, 'title' => 'dPdT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2PdRho2
    image_plot('d' => d, 'zs' => d.d2PdRho2, 'title' => 'd2PdRho2 ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2PdT2
    image_plot('d' => d, 'zs' => d.d2PdT2, 'title' => 'd2PdT2', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2PdRhodT
    image_plot('d' => d, 'zs' => d.d2PdRhodT, 'title' => 'd2PdRhodT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  

## E
  def plE
    image_plot('d' => d, 'zs' => d.mE, 'title' => 'E ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def dEdRho
    image_plot('d' => d, 'zs' => d.dEdRho, 'title' => 'dEdRho ', 'interpolate' => false)
    plotRhoTProfiles()
  end
    
  def dEdT
    image_plot('d' => d, 'zs' => d.dEdT, 'title' => 'dEdT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2EdRho2
    image_plot('d' => d, 'zs' => d.d2EdRho2, 'title' => 'd2EdRho2 ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2EdT2
    image_plot('d' => d, 'zs' => d.d2EdT2, 'title' => 'd2EdT2', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2EdRhodT
    image_plot('d' => d, 'zs' => d.d2EdRhodT, 'title' => 'd2EdRhodT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  

## S


  def plS
    image_plot('d' => d, 'zs' => d.mS, 'title' => 'S ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def dSdRho
    image_plot('d' => d, 'zs' => d.dSdRho, 'title' => 'dSdRho ', 'interpolate' => false)
    plotRhoTProfiles()
  end
    
  def dSdT
    image_plot('d' => d, 'zs' => d.dSdT, 'title' => 'dSdT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2SdRho2
    image_plot('d' => d, 'zs' => d.d2SdRho2, 'title' => 'd2SdRho2 ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2SdT2
    image_plot('d' => d, 'zs' => d.d2SdT2, 'title' => 'd2SdT2', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2SdRhodT
    image_plot('d' => d, 'zs' => d.d2SdRhodT, 'title' => 'd2SdRhodT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  

## 


  def plot_boundaries(xs,ys,margin,xmin=nil,xmax=nil,ymin=nil,ymax=nil)
    xmin = xs.min if xmin == nil
    xmax = xs.max if xmax == nil
    ymin = ys.min if ymin == nil
    ymax = ys.max if ymax == nil
    
    width = (xmax == xmin) ? 1 : xmax - xmin
    height = (ymax == ymin) ? 1 : ymax - ymin
    
    left_boundary = xmin - margin * width
    right_boundary = xmax + margin * width
    
    top_boundary = ymax + margin * height
    bottom_boundary = ymin - margin * height
    
    return [ left_boundary, right_boundary, top_boundary, bottom_boundary ]
  end
  
  def pressure
    image_plot('d' => d, 'zs' => d.pressure, 'title' => 'log10(Pressure)',
               'z_lower' => 0, 'z_upper' => 14, 'interpolate' => false)
    # We would like to overplot some profile here.
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do
      @bounds = [-4,2.5,2,10]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        #        clip_press_image
        t.line_width=2.5
        t.show_polyline(@LowMasslog10RhoProfile, @LowMasslog10TProfile, Blue)
        #      t.show_marker('Xs' => @Neplog10Rho, 'Ys' => @Neplog10T,
#                    'marker' => Asterisk,
#                    'scale' => 1.2, 
#                    'color' => Blue)
        print @image_right_margin
      }
    end 
  end  
end


MY_plot.new('h5tdata/derivs/')
