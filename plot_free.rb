# plot_images.rb

load "/Users/neil/research/mesa/utils/image_plot.rb"
    
class MY_data < Image_data

  attr_accessor :fr
  attr_accessor :dfdrho
  attr_accessor :dfdt
  attr_accessor :d2fdrho2
  #    attr_accessor :d2fdrho2num
#    attr_accessor :d2fdrho2diff
  attr_accessor :d2fdt2
#    attr_accessor :d2fdt2num
#    attr_accessor :d2fdt2diff
  attr_accessor :d2fdrhodt
  attr_accessor :d3fdrhodt2
  attr_accessor :d3fdrho2dt
  attr_accessor :d4fdrho2dt2
  
  
  
  def initialize(data_dir)
    
    read_image_Xs(data_dir, 'fr_logRhovect.data', 'log10(\rho)')
    read_image_Ys(data_dir, 'fr_logTvect.data', 'log10(T)')
    
    @fr = read_image_data(data_dir, 'fr')
    @dfdrho = read_image_data(data_dir, 'dfdrho')
    @dfdt = read_image_data(data_dir, 'dfdt')
    @d2fdrho2 = read_image_data(data_dir, 'd2fdrho2')
    @d2fdt2 = read_image_data(data_dir, 'd2fdt2')
    @d2fdrhodt = read_image_data(data_dir, 'd2fdrhodt')
    
    #      @d2fdrho2num = read_image_data(data_dir, 'd2fdrho2_num')
 #     @d2fdrho2diff = read_image_data(data_dir, 'd2fdrho2_diff')
#      @d2fdt2num = read_image_data(data_dir, 'd2fdt2_num')
#      @d2fdt2diff = read_image_data(data_dir, 'd2fdt2_diff')

    @d3fdrhodt2 = read_image_data(data_dir, 'd3fdrhodt2')
    @d3fdrho2dt = read_image_data(data_dir, 'd3fdrho2dt')
    @d4fdrho2dt2 = read_image_data(data_dir, 'd4fdrho2dt2')
  end
  
end # class MY_data


class MY_plot

  include Math
  include Tioga
  include FigureConstants
  include Image_plot
  
  def initialize(data_dir)
      
    @data_dir = data_dir
    @figure_maker = FigureMaker.default
    t.def_eval_function { |str| eval(str) }
    t.save_dir = 'aneos_water'
    
    t.def_figure('fr') { fr }
    t.def_figure('dfdrho') { dfdrho }
    t.def_figure('dfdt') { dfdt }
    t.def_figure('d2fdrho2') { d2fdrho2 }
    #      t.def_figure('d2fdrho2num') { d2fdrho2num }
#      t.def_figure('d2fdrho2diff') { d2fdrho2diff }
      
    t.def_figure('d2fdt2') { d2fdt2 }
#      t.def_figure('d2fdt2num' ) { d2fdt2num }
#      t.def_figure('d2fdt2diff' ) { d2fdt2diff }

    t.def_figure('d2fdrhodt') { d2fdrhodt }
    t.def_figure('d3fdrhodt2') { d3fdrhodt2 }
    t.def_figure('d3fdrho2dt') { d3fdrho2dt }
    t.def_figure('d4fdrho2dt2') { d4fdrho2dt2 }
    
    
    @image_data = MY_data.new(data_dir)
    @label_scale = 0.75
    @no_clipping = false #true        
    
      ### Load the neptune profile into the class
    @margin = 0.05

#      @NepProfile = Dvector.read("hotnep.dat")
#      @NepRhoProfile = @NepProfile[0]
#      @NepTProfile = @NepProfile[1]
#      @NepPressureProfile = @NepProfile[2]
#      @Neplog10Rho = @NepRhoProfile.log10
#      @Neplog10T = @NepTProfile.log10
#      @NeplnP = @NepPressureProfile.log
      
    t.def_enter_page_function { enter_page } 
      
  end
    
  def enter_page
    t.yaxis_numeric_label_angle = -90
    t.page_setup(11*72/2,8.5*72/2)
    t.set_frame_sides(0.15,0.85,0.85,0.15) # left, right, top, bottom in page coords  
  end
  
  def clip_image
    t.fill_color = Black
    t.fill_frame
    return
  end
  
  def t
    @figure_maker
  end
  
  def d
    @image_data
  end
  
  def do_decorations(title)
  end
  
  # plot routines
  
  def fr
    image_plot('d' => d, 'zs' => d.fr, 'title' => 'free energy', 'interpolate' => false)#,
    #                 'z_lower' => 14, 'z_upper' => 15, 'interpolate' => false)
#                 'z_lower' => 1.42, 'z_upper' => 1.435, 'interpolate' => false)
  end
  
  
  def dfdrho
    image_plot('d' => d, 'zs' => d.dfdrho, 'title' => 'dfdrho', 'interpolate' => false)#,
#                 'z_lower' => 0, 'z_upper' => 25, 'interpolate' => false)
  end

  def dfdt
    image_plot('d' => d, 'zs' => d.dfdt, 'title' => 'dfdt', 'interpolate' => false)#,
#                 'z_lower' => -14, 'z_upper' => 0, 'interpolate' => false)
  end  
  
  def d2fdrho2
    image_plot('d' => d, 'zs' => d.d2fdrho2, 'title' => 'd2fdrho2', 'interpolate' => false)#,
#                 'z_lower' => -35, 'z_upper' => 35, 'interpolate' => false)
  end

  def d2fdrho2num
    image_plot('d' => d, 'zs' => d.d2fdrho2num, 'title' => 'd2fdrho2 numeric')#,
#                 'z_lower' => -35, 'z_upper' => 35, 'interpolate' => false)
  end

  def d2fdrho2diff
    image_plot('d' => d, 'zs' => d.d2fdrho2diff, 'title' => 'd2fdrho2 Diff',
               'z_lower' => -1, 'z_upper' => 1, 'interpolate' => false)
  end
  
  def d2fdt2
    image_plot('d' => d, 'zs' => d.d2fdt2, 'title' => 'd2fdt2', 'interpolate'=> false)#,
#                 'z_lower' => -15, 'z_upper' => 15, 'interpolate' => false)
  end

  def d2fdt2num
    image_plot('d' => d, 'zs' => d.d2fdt2num, 'title' => 'd2fdt2 numeric',
               'z_lower' => -15, 'z_upper' => 15, 'interpolate' => false)
  end

  def d2fdt2diff
    image_plot('d' => d, 'zs' => d.d2fdt2diff, 'title' => 'd2fdt2 diff',
               'z_lower' => -1, 'z_upper' => 1, 'interpolate' => false)
  end

  def d2fdrhodt
    image_plot('d' => d, 'zs' => d.d2fdrhodt, 'title' => 'd2fdrhodt', 'interpolate' => false)#,
#                 'z_lower' => -21, 'z_upper' => 21, 'interpolate' => false)
  end
  
  def d3fdrhodt2
    image_plot('d' => d, 'zs' => d.d3fdrhodt2, 'title' => 'd3fdrhodt2', 'interpolate' => false)#,
#                 'z_lower' => -22, 'z_upper' => 22, 'interpolate' => false)
  end

  def d3fdrho2dt
    image_plot('d' => d, 'zs' => d.d3fdrho2dt, 'title' => 'd3fdrho2dt', 'interpolate' => false)#,
#                 'z_lower' => -28, 'z_upper' => 28, 'interpolate' => false)
  end


  def d4fdrho2dt2
    image_plot('d' => d, 'zs' => d.d4fdrho2dt2, 'title' => 'd4fdrho2dt2', 'interpolate' => false)#,
#                 'z_lower' => -31, 'z_upper' => 31, 'interpolate' => false)
  end
  
  def plot_boundaries(xs,ys,margin,xmin=nil,xmax=nil,ymin=nil,ymax=nil)
    xmin = xs.min if xmin == nil
    xmax = xs.max if xmax == nil
    ymin = ys.min if ymin == nil
    ymax = ys.max if ymax == nil
    
    width = (xmax == xmin) ? 1 : xmax - xmin
    height = (ymax == ymin) ? 1 : ymax - ymin
    
    left_boundary = xmin - margin * width
    right_boundary = xmax + margin * width
    
    top_boundary = ymax + margin * height
    bottom_boundary = ymin - margin * height
    
    return [ left_boundary, right_boundary, top_boundary, bottom_boundary ]
  end
  
  def pressure
    image_plot('d' => d, 'zs' => d.pressure, 'title' => 'log10(Pressure)',
               'z_lower' => 0, 'z_upper' => 14, 'interpolate' => false)
      # We would like to overplot some profile here.
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do
      @bounds = [-8,10,10,1]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        #        clip_press_image
        t.line_width=2.5
        t.show_polyline(@Neplog10Rho, @Neplog10T, Blue)
#      t.show_marker('Xs' => @Neplog10Rho, 'Ys' => @Neplog10T,
#                    'marker' => Asterisk,
#                    'scale' => 1.2, 
#                    'color' => Blue)
        print @image_right_margin
      }
    end 
  end  
end


MY_plot.new('h5tdata')
#MY_plot.new('h5tdata/iron')
#MY_plot.new('h5tdata/serp')
