# plot_images.rb

load "/Users/neil/research/mesa/utils/image_plot.rb"
    
class MY_data < Image_data

  attr_accessor :lnP
  attr_accessor :lnE
  attr_accessor :lnS

  attr_accessor :dlnPdlnRho
  attr_accessor :dlnEdlnRho
  attr_accessor :dlnSdlnRho

  attr_accessor :dlnPdlnT
  attr_accessor :dlnEdlnT
  attr_accessor :dlnSdlnT

  attr_accessor :d2lnPdlnRho2
  attr_accessor :d2lnEdlnRho2
  attr_accessor :d2lnSdlnRho2

  attr_accessor :d2lnPdlnT2
  attr_accessor :d2lnEdlnT2
  attr_accessor :d2lnSdlnT2

  attr_accessor :d2lnPdlnRhodlnT
  attr_accessor :d2lnEdlnRhodlnT
  attr_accessor :d2lnSdlnRhodlnT
  
  def initialize(data_dir)
        
    read_image_Xs(data_dir, 'h5t_logRhovect.data', 'log10(\rho)')
    read_image_Ys(data_dir, 'h5t_logTvect.data', 'log10(T)')
    
    @lnP = read_image_data(data_dir, 'lnP')
    @lnE = read_image_data(data_dir, 'lnE')
    @lnS = read_image_data(data_dir, 'lnS')

    @dlnPdlnRho = read_image_data(data_dir, 'dlnPdlnRho')
    @dlnEdlnRho = read_image_data(data_dir, 'dlnEdlnRho')
    @dlnSdlnRho = read_image_data(data_dir, 'dlnSdlnRho')

    @dlnPdlnT = read_image_data(data_dir, 'dlnPdlnT')
    @dlnEdlnT = read_image_data(data_dir, 'dlnEdlnT')
    @dlnSdlnT = read_image_data(data_dir, 'dlnSdlnT')

    @d2lnPdlnRho2 = read_image_data(data_dir, 'd2lnPdlnRho2')
    @d2lnEdlnRho2 = read_image_data(data_dir, 'd2lnEdlnRho2')
    @d2lnSdlnRho2 = read_image_data(data_dir, 'd2lnSdlnRho2')

    @d2lnPdlnT2 = read_image_data(data_dir, 'd2lnPdlnT2')
    @d2lnEdlnT2 = read_image_data(data_dir, 'd2lnEdlnT2')
    @d2lnSdlnT2 = read_image_data(data_dir, 'd2lnSdlnT2')

    @d2lnPdlnRhodlnT = read_image_data(data_dir, 'd2lnPdlnRhodlnT')
    @d2lnEdlnRhodlnT = read_image_data(data_dir, 'd2lnEdlnRhodlnT')
    @d2lnSdlnRhodlnT = read_image_data(data_dir, 'd2lnSdlnRhodlnT')
      
  end
  
end # class MY_data


class MY_plot

  include Math
  include Tioga
  include FigureConstants
  include Image_plot
  
  def initialize(data_dir)
    
    @data_dir = data_dir
    @figure_maker = FigureMaker.default
    t.def_eval_function { |str| eval(str) }
    t.save_dir = 'lnderivs_plots'
    
    t.def_figure('lnP') { lnP }
    t.def_figure('lnE') { lnE }
    t.def_figure('lnS') { lnS }

    t.def_figure('dlnPdlnRho') { dlnPdlnRho }
    t.def_figure('dlnEdlnRho') { dlnEdlnRho }
    t.def_figure('dlnSdlnRho') { dlnSdlnRho }

    t.def_figure('dlnPdlnT') { dlnPdlnT }
    t.def_figure('dlnEdlnT') { dlnEdlnT }
    t.def_figure('dlnSdlnT') { dlnSdlnT }

    t.def_figure('d2lnPdlnRho2') { d2lnPdlnRho2 }
    t.def_figure('d2lnEdlnRho2') { d2lnEdlnRho2 }
    t.def_figure('d2lnSdlnRho2') { d2lnSdlnRho2 }
    
    t.def_figure('d2lnPdlnT2') { d2lnPdlnT2 }
    t.def_figure('d2lnEdlnT2') { d2lnEdlnT2 }
    t.def_figure('d2lnSdlnT2') { d2lnSdlnT2 }

    t.def_figure('d2lnPdlnRhodlnT') { d2lnPdlnRhodlnT }
    t.def_figure('d2lnEdlnRhodlnT') { d2lnEdlnRhodlnT }
    t.def_figure('d2lnSdlnRhodlnT') { d2lnSdlnRhodlnT }

    @image_data = MY_data.new(data_dir)
    @label_scale = 0.75
    @no_clipping = false #true        
    
      ### Load the neptune profile into the class
    @margin = 0.05
    


    @logRhoMin = -5
    @logRhoMax = 2
    @logTMin = 2.5
    @logTMax = 5.5

    @PlotProfiles = false

    @LowMassProfile1 = Dvector.read("planet_data/RhoTset01.dat")
    @LowMasslog10RhoProfile1 = @LowMassProfile1[0]
    @LowMasslog10TProfile1 = @LowMassProfile1[1]
    
    @LowMassProfile2 = Dvector.read("planet_data/RhoTset02.dat")
    @LowMasslog10RhoProfile2 = @LowMassProfile2[0]
    @LowMasslog10TProfile2 = @LowMassProfile2[1]
    
    @LowMassProfile3 = Dvector.read("planet_data/RhoTset03.dat")
    @LowMasslog10RhoProfile3 = @LowMassProfile3[0]
    @LowMasslog10TProfile3 = @LowMassProfile3[1]
    
    @HD80606Profile = Dvector.read("planet_data/RhoTset04.dat")
    @HD80606log10RhoProfile = @HD80606Profile[0]
    @HD80606log10TProfile = @HD80606Profile[1]
    
#      @NepPressureProfile = @NepProfile[2]
#      @Neplog10Rho = @NepRhoProfile.log10
#      @Neplog10T = @NepTProfile.log10
#      @NeplnP = @NepPressureProfile.log
      
    t.def_enter_page_function { enter_page } 
    
  end
  
  def enter_page
    t.yaxis_numeric_label_angle = -90
    t.page_setup(11*72/2,8.5*72/2)
    t.set_frame_sides(0.15,0.85,0.85,0.15) # left, right, top, bottom in page coords  
  end
    
  def clip_image
    t.fill_color = Black
    t.fill_frame
    return
  end
    
  def t
    @figure_maker
  end
  
  def d
    @image_data
  end
  
  def do_decorations(title)
  end
    
  # plot routines
  
## lnP

  def plotRhoTProfiles
    if(@PlotProfiles) then
      t.subplot('right_margin' => 0.05, 
                'left_margin' => @image_left_margin) do 
        @bounds = [@logRhoMin, @logRhoMax, @logTMax, @logTMin] 
        t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
        t.top_edge_visible = false
        t.right_edge_visible = false
        t.show_plot(@bounds) {
          t.line_width=1.0
          t.show_polyline(@LowMasslog10RhoProfile1, @LowMasslog10TProfile1, Blue)
          t.show_polyline(@LowMasslog10RhoProfile2, @LowMasslog10TProfile2, Red)
          t.show_polyline(@LowMasslog10RhoProfile3, @LowMasslog10TProfile3, Green)
          t.show_polyline(@HD80606log10RhoProfile, @HD80606log10TProfile, Black)
        }
      end
    end
  end

  def lnP
    image_plot('d' => d, 'zs' => d.lnP, 'title' => 'lnP ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def dlnPdlnRho
    image_plot('d' => d, 'zs' => d.dlnPdlnRho, 'title' => 'dlnPdlnRho ', 'interpolate' => false)
    plotRhoTProfiles()
  end
    
  def dlnPdlnT
    image_plot('d' => d, 'zs' => d.dlnPdlnT, 'title' => 'dlnPdlnT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnPdlnRho2
    image_plot('d' => d, 'zs' => d.d2lnPdlnRho2, 'title' => 'd2lnPdlnRho2 ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnPdlnT2
    image_plot('d' => d, 'zs' => d.d2lnPdlnT2, 'title' => 'd2lnPdlnT2', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnPdlnRhodlnT
    image_plot('d' => d, 'zs' => d.d2lnPdlnRhodlnT, 'title' => 'd2lnPdlnRhodlnT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  

## lnE
  def lnE
    image_plot('d' => d, 'zs' => d.lnE, 'title' => 'lnE ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def dlnEdlnRho
    image_plot('d' => d, 'zs' => d.dlnEdlnRho, 'title' => 'dlnEdlnRho ', 'interpolate' => false)
    plotRhoTProfiles()
  end
    
  def dlnEdlnT
    image_plot('d' => d, 'zs' => d.dlnEdlnT, 'title' => 'dlnEdlnT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnEdlnRho2
    image_plot('d' => d, 'zs' => d.d2lnEdlnRho2, 'title' => 'd2lnEdlnRho2 ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnEdlnT2
    image_plot('d' => d, 'zs' => d.d2lnEdlnT2, 'title' => 'd2lnEdlnT2', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnEdlnRhodlnT
    image_plot('d' => d, 'zs' => d.d2lnEdlnRhodlnT, 'title' => 'd2lnEdlnRhodlnT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  

## lnS


  def lnS
    image_plot('d' => d, 'zs' => d.lnS, 'title' => 'lnS ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def dlnSdlnRho
    image_plot('d' => d, 'zs' => d.dlnSdlnRho, 'title' => 'dlnSdlnRho ', 'interpolate' => false)
    plotRhoTProfiles()
  end
    
  def dlnSdlnT
    image_plot('d' => d, 'zs' => d.dlnSdlnT, 'title' => 'dlnSdlnT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnSdlnRho2
    image_plot('d' => d, 'zs' => d.d2lnSdlnRho2, 'title' => 'd2lnSdlnRho2 ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnSdlnT2
    image_plot('d' => d, 'zs' => d.d2lnSdlnT2, 'title' => 'd2lnSdlnT2', 'interpolate' => false)
    plotRhoTProfiles()
  end
  
  def d2lnSdlnRhodlnT
    image_plot('d' => d, 'zs' => d.d2lnSdlnRhodlnT, 'title' => 'd2lnSdlnRhodlnT ', 'interpolate' => false)
    plotRhoTProfiles()
  end
  

## 


  def plot_boundaries(xs,ys,margin,xmin=nil,xmax=nil,ymin=nil,ymax=nil)
    xmin = xs.min if xmin == nil
    xmax = xs.max if xmax == nil
    ymin = ys.min if ymin == nil
    ymax = ys.max if ymax == nil
    
    width = (xmax == xmin) ? 1 : xmax - xmin
    height = (ymax == ymin) ? 1 : ymax - ymin
    
    left_boundary = xmin - margin * width
    right_boundary = xmax + margin * width
    
    top_boundary = ymax + margin * height
    bottom_boundary = ymin - margin * height
    
    return [ left_boundary, right_boundary, top_boundary, bottom_boundary ]
  end
  
  def pressure
    image_plot('d' => d, 'zs' => d.pressure, 'title' => 'log10(Pressure)',
               'z_lower' => 0, 'z_upper' => 14, 'interpolate' => false)
    # We would like to overplot some profile here.
    t.subplot('right_margin' => 0.05, 
              'left_margin' => @image_left_margin) do
      @bounds = [-4,2.5,2,10]
      t.xaxis_type = t.yaxis_type = AXIS_HIDDEN
      t.top_edge_visible = false
      t.right_edge_visible = false
      t.show_plot(@bounds) {
        #        clip_press_image
        t.line_width=2.5
        t.show_polyline(@LowMasslog10RhoProfile, @LowMasslog10TProfile, Blue)
        #      t.show_marker('Xs' => @Neplog10Rho, 'Ys' => @Neplog10T,
#                    'marker' => Asterisk,
#                    'scale' => 1.2, 
#                    'color' => Blue)
        print @image_right_margin
      }
    end 
  end  
end


MY_plot.new('h5tdata/lnderivs/')
